<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 07.01.2016
 * Time: 15:03
 */
namespace CustomIS\AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class FilterCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('lexik_form_filter.query_builder_updater'))
        {
            return;
        }

        $taggedServices = $container->findTaggedServiceIds('customis.filter');

        foreach ($taggedServices as $id => $tags)
        {
            $container
                ->findDefinition($id)
                ->addMethodCall('setFilterBuilder', [
                    new Reference('lexik_form_filter.query_builder_updater')
                ])
            ;
        }
    }
}