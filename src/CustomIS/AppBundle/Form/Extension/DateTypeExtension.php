<?php

namespace CustomIS\AppBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DateTypeExtension
 *
 * @package CustomIS\AppBundle\Form\Extension
 */
class DateTypeExtension extends AbstractTypeExtension
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['showDatepickerOnFocus'] = $options['show_datepicker_on_focus'];
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addViewTransformer(new CallbackTransformer(
            function ($value) {
                return $value;
            },
            function ($value) {
                return str_replace(' ', '', trim($value));
            }
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('widget', 'single_text');
        $resolver->setDefault('format', 'dd.MM.yyyy');
        $resolver->setDefault('show_datepicker_on_focus', true);
        $resolver->setNormalizer('format', function (Options $options, $value) {
            return str_replace(' ', '', $value);
        });
    }

    /**
     * @return mixed
     */
    public function getExtendedType()
    {
        return DateType::class;
    }
}
