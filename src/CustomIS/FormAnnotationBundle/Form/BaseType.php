<?php

namespace CustomIS\FormAnnotationBundle\Form;

use CustomIS\FormAnnotationBundle\Form\EventListener\EntityFormSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class BaseType extends AbstractType
{
    /**
     * @var EntityFormSubscriber
     */
    private $subscriber;

    /**
     * BaseType constructor.
     * @param EntityFormSubscriber $subscriber
     */
    public function __construct(EntityFormSubscriber $subscriber)
    {
        $this->subscriber = $subscriber;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventSubscriber($this->subscriber);
    }
}
