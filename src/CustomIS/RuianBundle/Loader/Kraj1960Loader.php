<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Loader;

use CustomIS\RuianBundle\Entity\Kraj1960;
use CustomIS\RuianBundle\Geometry\GeometryEntityFactory;

/**
 * Class Kraj1960Loader
 *
 * @package CustomIS\RuianBundle\Loader
 */
class Kraj1960Loader extends AbstractRuianLoader
{
    /**
     * @param \SimpleXMLElement $element
     * @param string $xmlFile
     */
    public function load(\SimpleXMLElement $element, string $xmlFile)
    {
        $ns = $element->getNamespaces(true);
        $kri = $element->children($ns['kri']);

        if (($kraj = $this->getEntityManager()->getRepository(Kraj1960::class)->findOneByCode($kri->{'Kod'})) !== null) {
            /** @var Kraj1960 $kraj */
            $kraj->setName((string) $kri->{'Nazev'});
        } else {
            $kraj = new Kraj1960(
                (int) $kri->{'Kod'},
                $kri->{'Nazev'}
            );

            $this->getEntityManager()->persist($kraj);
        }

        $kraj->clearGeometry();
        foreach ($element->xpath('.//gml:Polygon[@gml:id]') as $polygon) {
            $geometry = GeometryEntityFactory::build($polygon, $this->getSymplifyBorderRation());
            $this->getEntityManager()->persist($geometry);
            $kraj->addGeometry($geometry);
        }
        $this->getEntityManager()->flush();
    }

    /**
     * @return array
     */
    public function getXmlNodeName(): array
    {
        return ['vf:Kraj'];
    }
}
