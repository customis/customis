<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 14.11.2015
 * Time: 11:03
 */

namespace CustomIS\FormAnnotationBundle\Annotation\Resolver;

use CustomIS\FormAnnotationBundle\Annotation\Order;
use Symfony\Component\Form\FormInterface;

class OrderResolver extends AbstractResolver
{
    public function supports($annotation)
    {
        return $annotation instanceof Order;
    }

    public function setData($value)
    {
        $this->getElementBuilder()->setOrder($value);
    }


}