<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Entity;

use CustomIS\RuianBundle\Geometry\GeometryTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Obec
 *
 * @package CustomIS\RuianBundle\Entity
 * @ORM\Entity(repositoryClass="ObecRepository")
 * @ORM\Table(schema="ruian")
 */
class Obec extends AbstractBaseRuianEntity
{
    /**
     * @var Okres
     * @ORM\ManyToOne(targetEntity="CustomIS\RuianBundle\Entity\Okres", inversedBy="obce")
     * @ORM\JoinColumn(nullable=false)
     */
    private $okres;

    /**
     * @var Pou
     * @ORM\ManyToOne(targetEntity="CustomIS\RuianBundle\Entity\Pou")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pou;

    /**
     * @var Momc[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="CustomIS\RuianBundle\Entity\Momc", mappedBy="obec")
     */
    private $momc;

    /**
     * @var Zsj[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="CustomIS\RuianBundle\Entity\Zsj", mappedBy="obec")
     */
    private $zsj;

    /**
     * Pou constructor.
     *
     * @param Okres  $okres
     * @param Pou    $pou
     * @param int    $code
     * @param string $name
     */
    public function __construct(Okres $okres, Pou $pou, $code, $name)
    {
        $this->okres = $okres;
        $this->pou = $pou;
        parent::__construct($code, $name);
    }

    /**
     * @return Okres
     */
    public function getOkres(): Okres
    {
        return $this->okres;
    }

    /**
     * @param Okres $okres
     */
    public function setOkres(Okres $okres)
    {
        $this->okres = $okres;
    }

    /**
     * @return Pou
     */
    public function getPou(): Pou
    {
        return $this->pou;
    }

    /**
     * @param Pou $pou
     */
    public function setPou(Pou $pou)
    {
        $this->pou = $pou;
    }

    /**
     * @return array
     */
    public function getZsj(): array
    {
        return array_map(function (Zsj $zsj) {
            return $zsj->getCode();
        }, iterator_to_array($this->zsj));
    }

    /**
     * @return Momc[]|ArrayCollection
     */
    public function getMomc()
    {
        return $this->momc;
    }
}
