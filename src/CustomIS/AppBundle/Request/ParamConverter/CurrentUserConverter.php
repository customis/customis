<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 18.05.2016
 * Time: 18:00
 */

namespace CustomIS\AppBundle\Request\ParamConverter;


use CustomIS\AppBundle\Request\CurrentUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class CurrentUserConverter implements ParamConverterInterface
{
    /**
     * @var CurrentUser
     */
    private $currentUser;


    /**
     * CurrentUserConverter constructor.
     * @param CurrentUser $currentUser
     */
    public function __construct(CurrentUser $currentUser)
    {
        $this->currentUser = $currentUser;
    }

    public function apply(Request $request, ParamConverter $configuration)
    {
        $request->attributes->set($configuration->getName(), $this->currentUser);
    }

    public function supports(ParamConverter $configuration)
    {
        return $configuration->getClass() === CurrentUser::class;
    }

}