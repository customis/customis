<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 11.01.2016
 * Time: 23:08
 */
namespace CustomIS\AppBundle\Chart;

use Sonata\IntlBundle\Templating\Helper\DateTimeHelper;

class Line
{
    /**
     * @var Column[]
     */
    private $columns = [];
    /**
     * @var DateTimeHelper
     */
    private $dateTimeHelper;

    /**
     * Line constructor.
     * @param DateTimeHelper $dateTimeHelper
     */
    public function __construct(DateTimeHelper $dateTimeHelper)
    {
        $this->dateTimeHelper = $dateTimeHelper;
    }


    /**
     * @param Column $column
     */
    public function addColumn(Column $column)
    {
        $this->columns[] = $column;
    }

    /**
     * @return Column[]
     */
    public function getColumns()
    {
        return $this->columns;
    }

    public function toC3ChartColumnsArray()
    {
        $return = [];
        foreach ($this->getColumns() as $column)
        {
            $return[] = $column->toC3ChartColumnArray();
        }

        return $return;
    }

    public function toC3ChartCategoiriesArray()
    {
        if (count($this->getColumns()) == 0)
        {
            return [];
        }

        $values = array_keys($this->getColumns()[0]->getValues());
        array_walk($values, function (&$value) {
            $value = $this->dateTimeHelper->format($value, 'MMM Y');
        });

        return $values;
    }
}