<?php

namespace CustomIS\AppBundle\EventListener;

use CustomIS\AppBundle\Entity\Uzivatel;
use CustomIS\AppBundle\Entity\UzivatelRepository;
use CustomIS\AppBundle\Entity\UzivatelRepositoryInterface;
use CustomIS\LdapBundle\Security\Core\User\LdapUser;
use Doctrine\ORM\EntityManager;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Zend\Ldap\Ldap;

/**
 * Class SuccessLoginListener
 *
 * @package CustomIS\AppBundle\EventListener
 */
class SuccessLdapLoginListener
{
    /**
     * @var UzivatelRepository
     */
    private $userRepository;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Ldap|null
     */
    private $ldap;

    /**
     * @var string
     */
    private $ldapNameExpression;

    /**
     * @var string
     */
    private $ldapEmailExpression;

    /**
     * SuccessLoginListener constructor.
     *
     * @param UzivatelRepositoryInterface $userRepository
     * @param EntityManager               $entityManager
     * @param Ldap|null                   $ldap
     * @param string                      $ldapNameExpression
     * @param string                      $ldapEmailExpression
     */
    public function __construct(
        UzivatelRepositoryInterface $userRepository,
        EntityManager $entityManager,
        $ldap,
        $ldapNameExpression,
        $ldapEmailExpression
    ) {
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        $this->ldap = $ldap;
        $this->ldapNameExpression = $ldapNameExpression;
        $this->ldapEmailExpression = $ldapEmailExpression;
    }

    /**
     * @param InteractiveLoginEvent $e
     */
    public function onSuccesslogin(InteractiveLoginEvent $e)
    {
        if ($this->ldap !== null) {
            $authToken = $e->getAuthenticationToken();
            $login = $authToken->getUsername();
            $userToken = $authToken->getUser();

            /** @var Uzivatel $user */
            if ($authToken->getUser() instanceof LdapUser
                && ($user = $this->userRepository->findByUser($login)) !== false
            ) {
                $expressionLanguage = new ExpressionLanguage();

                $data = new \ArrayObject($this->ldap->getEntry($userToken->getDn()));
                $user->setJmeno(
                    $expressionLanguage->evaluate(
                        $this->ldapNameExpression,
                        ['data' => $data]
                    )
                );
                $user->setEmail(
                    $expressionLanguage->evaluate(
                        $this->ldapEmailExpression,
                        ['data' => $data]
                    )
                );

                $this->entityManager->flush();
            }
        }
    }
}
