<?php
/**
 * Created by PhpStorm.
 * User: T
 * Date: 24.2.2017
 * Time: 12:36
 */

namespace CustomIS\AppBundle\Routing;


use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\CacheWarmer\WarmableInterface;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RouterInterface;

class SessionRouter implements UrlGeneratorInterface, WarmableInterface, RouterInterface, RequestMatcherInterface
{
    /**
     * @var Router $router
     */
    private $router;

    /**
     * @var Session $session
     */
    private $session;

    /**
     * SessionRouter constructor.
     * @param Router $router
     * @param Session $session
     */
    public function __construct(Router $router, Session $session)
    {
        $this->router = $router;
        $this->session = $session;
    }

    /**
     * {@inheritdoc}
     */
    public function setContext(RequestContext $context)
    {
        $this->router->setContext($context);
    }

    /**
     * {@inheritdoc}
     */
    public function getContext()
    {
        return $this->router->getContext();
    }

    /**
     * {@inheritdoc}
     */
    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        $checkSessionData = $parameters['_customis_session_route'] ?? true;
        unset($parameters['_customis_session_route']);
        $sessionKey = 'customis_session_route/'.$name;
        if ($checkSessionData
            && $this->session->has($sessionKey)) {
            $parameters = array_merge($this->session->get($sessionKey), $parameters);
        }

        return $this->router->getGenerator()->generate($name, $parameters, $referenceType);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function matchRequest(Request $request)
    {
        return $this->router->matchRequest($request);
    }

    /**
     * @return null|RouteCollection
     */
    public function getRouteCollection()
    {
        return $this->router->getRouteCollection();
    }

    /**
     * @param string $pathinfo
     * @return array
     */
    public function match($pathinfo)
    {
        return $this->router->match($pathinfo);
    }

    /**
     * @param string $cacheDir
     */
    public function warmUp($cacheDir)
    {
        return $this->router->warmUp($cacheDir);
    }


}