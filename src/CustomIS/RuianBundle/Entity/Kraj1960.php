<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Entity;

use CustomIS\RuianBundle\Geometry\GeometryTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Kraj1960
 *
 * @package CustomIS\RuianBundle\Entity
 * @ORM\Entity(repositoryClass="Kraj1960Repository")
 * @ORM\Table(schema="ruian")
 */
class Kraj1960 extends AbstractBaseRuianEntity
{

}
