<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 07.01.2016
 * Time: 15:00
 */
namespace CustomIS\AppBundle\Pager;

use Knp\Component\Pager\Paginator;

interface PaginatableInterface
{
    public function setPaginator(Paginator $paginator);
}