<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 15.7.14
 * Time: 9:03
 */

namespace CustomIS\PostgresDateRangeBundle\Range;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class DateRange
 *
 * @package CustomIS\PostgresDateRangeBundle\Range
 */
class DateRange
{
    /**
     * @var \DateTime
     */
    private $start;

    /**
     * @var \DateTime
     */
    private $end;

    /**
     * @var bool
     */
    private $endExcluded = false;

    /**
     * DateRange constructor.
     *
     * @param \DateTimeInterface|null $start
     * @param \DateTimeInterface|null $end
     */
    public function __construct(\DateTimeInterface $start = null, \DateTimeInterface $end = null)
    {
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return '['
               .($this->getStart() === null
                ? ''
                : $this->getStart()->format('Y-m-d'))
               .', '
               .($this->getEnd() === null
                ? ''
                : $this->getEnd()->format('Y-m-d'))
               .($this->endExcluded === true
                ? ')'
                : ']');
    }

    /**
     * @return \DateTimeInterface
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param \DateTimeInterface $start
     */
    public function setStart(\DateTimeInterface $start = null)
    {
        $this->start = $start;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param \DateTimeInterface $end
     */
    public function setEnd(\DateTimeInterface $end = null)
    {
        $this->end = $end;
    }

    /**
     * @param ExecutionContextInterface $context
     * @param                           $payload
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->start !== null && $this->end !== null && $this->start > $this->end) {
            $context->buildViolation('Datum od nesmí být větší než datum do')
                    ->atPath('start')
                    ->addViolation()
            ;
        }
    }

    /**
     * @param bool $endExcluded
     */
    public function setEndExcluded(bool $endExcluded)
    {
        $this->endExcluded = $endExcluded;
    }

    /**
     * @return bool
     */
    public function isEndExcluded(): bool
    {
        return $this->endExcluded;
    }
}
