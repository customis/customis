<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 25.09.2016
 * Time: 14:04
 */

namespace CustomIS\AppBundle\Form\Extension;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntityTypeExtension extends AbstractTypeExtension
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $button_resolver = new OptionsResolver();
        $button_resolver->setRequired(['enabled']);
        $button_resolver->setDefaults([
            'enabled' => false,
//            'position' => 'after',
            'class' => 'btn btn-primary',
            'icon-class' => null
        ]);
//        $button_resolver->addAllowedValues('position', ['before', 'after']);
        $button_resolver->addAllowedValues('enabled', [true, false]);

        $options['button'] = $button_resolver->resolve($options['button']);

        $view->vars['button'] = $options['button'];
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('button', []);
        $resolver->setAllowedTypes('button', 'array');
    }

    public function getExtendedType()
    {
        return EntityType::class;
    }
}