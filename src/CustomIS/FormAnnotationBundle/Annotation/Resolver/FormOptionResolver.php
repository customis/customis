<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 14.11.2015
 * Time: 11:52
 */

namespace CustomIS\FormAnnotationBundle\Annotation\Resolver;


use Symfony\Component\Form\FormInterface;

abstract class FormOptionResolver extends AbstractResolver
{
    public function resolve(FormInterface $formElement, array $annotations)
    {
        $value = null;

        foreach ($annotations as $annotation)
        {
            if ($this->supports($annotation) && $annotation->getForm() === $this->getForm()->getName())
            {
                $value = $annotation->getValue();
                break;
            }
        }

        if ($value === null)
        {
            foreach ($annotations as $annotation)
            {
                if ($this->supports($annotation) && $annotation->getForm() === null)
                {
                    $value = $annotation->getValue();
                    break;
                }
            }
        }

        if ($value !== null)
        {
            $this->setData($value);
        }
    }

    abstract public function setData($value);
}