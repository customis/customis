<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 19.09.2016
 * Time: 17:00
 */

namespace CustomIS\PostgresBundle\DQL;


use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class TsRange extends FunctionNode
{
    public $od = null;
    public $do = null;
    public $type = null;

    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return 'tsrange('
        . $this->od->dispatch($sqlWalker)
        . ', '
        . $this->do->dispatch($sqlWalker)
        . ','
        . $this->type->dispatch($sqlWalker)
        . ')'
            ;
    }

    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     *
     * @return void
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->od = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->do = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->type = $parser->StringPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}