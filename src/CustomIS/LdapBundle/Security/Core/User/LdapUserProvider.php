<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 04.01.2016
 * Time: 14:39
 */

namespace CustomIS\LdapBundle\Security\Core\User;

use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Zend\Ldap\Dn;
use Zend\Ldap\Exception\LdapException;
use Zend\Ldap\Filter;
use Zend\Ldap\Ldap;

/**
 * Class LdapUserProvider
 *
 * @package CustomIS\LdapBundle\Security\Core\User
 */
class LdapUserProvider implements UserProviderInterface
{
    /**
     * @var Ldap
     */
    private $ldap;

    /**
     * @var string
     */
    private $ldapFilter;

    /**
     * @var array
     */
    private $defaultRoles = [];

    /**
     * @var array
     */
    private $roles;

    /**
     * LdapUserProvider constructor.
     *
     * @param Ldap   $ldap
     * @param string $ldapFilter
     * @param array  $defaultRoles
     * @param array  $roles
     */
    public function __construct(Ldap $ldap, $ldapFilter, array $defaultRoles, array $roles)
    {
        $this->ldap = $ldap;
        $this->ldapFilter = $ldapFilter;
        $this->defaultRoles = $defaultRoles;
        $this->roles = $roles;
    }

    /**
     * @param string $username
     *
     * @return LdapUser
     */
    public function loadUserByUsername($username)
    {
        return $this->loadUser($username);
    }

    /**
     * @param UserInterface $user
     *
     * @return LdapUser
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof LdapUser) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUser($user->getUsername());
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class === LdapUser::class;
    }

    private function loadUser($username)
    {
        try {
            $ldapFilter = str_replace('{username}', $username, $this->ldapFilter);
            $data = $this->ldap->search($ldapFilter);
            if (count($data) === 0) {
                throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username));
            } elseif (count($data) > 1) {
                throw new UsernameNotFoundException('More than one user found');
            } else {
                $userData = $data->getFirst();
                $dn = Dn::fromString($userData['dn']);

                return new LdapUser(
                    $username,
                    $dn,
                    $userData['userpassword'] ?? null,
                    array_unique(array_merge(
                        $this->defaultRoles,
                        iterator_to_array($this->loadUserRoles($username))
                    ))
                );
            }
        } catch (LdapException $e) {
            throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username), 0, $e);
        }
    }

    /**
     * @param $username
     *
     * @return \Generator|string[]
     */
    private function loadUserRoles($username)
    {
        foreach ($this->roles as $role => $filters) {
            $ldapFilter = Filter::andFilter(
                Filter::equals('sAMAccountName', $username),
                Filter::orFilter(...$filters)
            );

            if (count($this->ldap->search($ldapFilter)) > 0) {
                yield $role;
            }
        }
    }
}
