<?php

namespace CustomIS\FormAnnotationBundle\Annotation;
use Doctrine\Common\Annotations\Annotation\Enum;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 * @Attributes({
 *   @Attribute("value", type = "string"),
 *   @Attribute("before", type = "string"),
 *   @Attribute("after", type = "string"),
 *   @Attribute("form", type = "string", required = false)
 * })
 */
class Order extends Annotation
{
    /**
     * @Enum({"first", "last"})
     */
    private $value;

    private $position;

    public function __construct(array $data)
    {
        parent::__construct($data);

        if (!(isset($data['value']) XOR isset($data['before']) XOR isset($data['after'])))
        {
            throw new \InvalidArgumentException("Může být vyplněno pouze 'value' nebo 'before' nebo 'after'.");
        }

        if (isset($data['value']))
        {
            $this->position = $data['value'];
        }

        if (isset($data['before']))
        {
            $this->position = ['before' => $data['before']];
        }

        if (isset($data['after']))
        {
            $this->position = ['after' => $data['after']];
        }
    }

    public function getValue()
    {
        return $this->position;
    }
}