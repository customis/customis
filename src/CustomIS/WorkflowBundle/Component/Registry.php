<?php
/**
 * Created by PhpStorm.
 * User: to068928
 * Date: 5.12.2016
 * Time: 10:15
 */

namespace CustomIS\WorkflowBundle\Component;

use CustomIS\WorkflowBundle\Entity\WorkflowRepository;

class Registry extends \Symfony\Component\Workflow\Registry
{
    private $transitions = [];

    private $places = [];

    private $workflowRepo;

    protected function preloadTransitionsData($workflowName)
    {
        $workflow = $this->workflowRepo->getWorkflowWithTransitions($workflowName);
        if($workflow == null) {
            throw new \Exception('Workflow neexistuje');
        }
        $transitions = [];
        foreach($workflow->getTransitions() AS $tr) {
            $transitions[$tr->getName()] = $tr;
        }
        $this->transitions[$workflowName] = $transitions;
    }

    protected function preloadPlacesData($workflowName)
    {
        $workflow = $this->workflowRepo->getWorkflowWithPlaces($workflowName);
        if($workflow == null) {
            throw new \Exception('Workflow neexistuje');
        }
        $places = [];
        foreach($workflow->getPlaces() AS $place) {
            $places[$place->getName()] = $place;
        }
        $this->places[$workflowName] = $places;
    }

    public function __construct(WorkflowRepository $workflowRepo)
    {
        $this->workflowRepo = $workflowRepo;
    }

    public function getTransitionObject($workflow, $transition)
    {
        if(!isset($this->transitions[$workflow])) {
            $this->preloadTransitionsData($workflow);
        }
        if(isset($this->transitions[$workflow][$transition])) {
            return $this->transitions[$workflow][$transition];
        }
        return null;
    }

    public function getPlaceObject($workflow, $place)
    {
        if(!isset($this->places[$workflow])) {
            $this->preloadPlacesData($workflow);
        }
        return isset($this->places[$workflow][$place]) ? $this->places[$workflow][$place] : null;
    }
}