<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 14.11.2015
 * Time: 11:46
 */

namespace CustomIS\FormAnnotationBundle\Annotation\Resolver;


use CustomIS\FormAnnotationBundle\Annotation\Annotation;
use CustomIS\FormAnnotationBundle\Form\ElementBuilderInterface;
use Symfony\Component\Form\FormInterface;
use CustomIS\FormAnnotationBundle\Annotation\Options;

abstract class AbstractResolver
{
    /**
     * @var ElementBuilderInterface
     */
    private $elementBuilder;

    /**
     * LabelResolver constructor.
     * @param ElementBuilderInterface $elementBuilder
     */
    public function __construct(ElementBuilderInterface $elementBuilder)
    {
        $this->elementBuilder = $elementBuilder;
    }

    /**
     * @param Annotation[] $annotations
     */
    public function resolve(array $annotations, FormInterface $form)
    {
        $value = null;

        foreach ($annotations as $annotation)
        {
            if ($this->supports($annotation) && $annotation->getForm() === $form->getName() && $this->validate($annotation))
            {
                $value = $annotation->getValue();
                break;
            }
        }

        if ($value === null)
        {
            foreach ($annotations as $annotation)
            {
                if ($this->supports($annotation) && $annotation->getForm() === null && $this->validate($annotation))
                {
                    $value = $annotation->getValue();
                    break;
                }
            }
        }

        if ($value !== null)
        {
            $this->setData($value);
        }
    }

    /**
     * @param Annotation $annotation
     * @return boolean
     */
    abstract public function supports($annotation);

    /**
     * @param mixed $value
     * @return void
     */
    abstract public function setData($value);

    /**
     * @return ElementBuilderInterface
     */
    public function getElementBuilder()
    {
        return $this->elementBuilder;
    }

    public function validate(Annotation $annotation)
    {
        return true;
    }

}