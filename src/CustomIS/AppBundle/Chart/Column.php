<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 02.03.2016
 * Time: 15:50
 */
namespace CustomIS\AppBundle\Chart;

class Column
{
	/**
	 * @var string
	 */
	private $label;
	/**
	 * @var array
	 */
	private $values = [];

	/**
	 * Column constructor.
	 *
	 * @param string $label
	 * @param array $values
	 */
	public function __construct($label, array $values = [])
	{
		$this->label  = $label;
		$this->values = $values;
	}

	/**
	 * @param mixed $value
	 */
	public function addValue($value, $kategory = null)
	{
		if ($kategory !== null)
		{
			$this->values[$kategory] = $value;
		}
		else
		{
			$this->values[] = $value;
		}
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->label;
	}

	/**
	 * @return array
	 */
	public function getValues()
	{
		return $this->values;
	}

	public function toC3ChartColumnArray($json = false)
	{
	    if ($json === false)
        {
            return array_merge([$this->getLabel()], array_values($this->getValues()));
        }
        else
        {
            return array_merge(['label' => $this->getLabel()], $this->getValues());
        }

	}
}