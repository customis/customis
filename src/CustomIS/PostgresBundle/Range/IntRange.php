<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 15.7.14
 * Time: 9:03
 */

namespace CustomIS\PostgresBundle\Range;


use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class IntRange
{
    /**
     * @var integer
     */
    private $start;

    /**
     * @var integer
     */
    private $end;

    function __construct($start = null, $end = null)
    {
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @param integer $start
     */
    public function setStart($start = null)
    {
        $this->start = $start;
    }

    /**
     * @return integer
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param integer $end
     */
    public function setEnd($end = null)
    {
        $this->end = $end;
    }

    /**
     * @return integer
     */
    public function getEnd()
    {
        return $this->end;
    }

    function __toString()
    {
        return '['
                . ($this->getStart() === null ? '' : $this->getStart())
                .', '
                . ($this->getEnd() === null ? '' : $this->getEnd())
                . ']';
    }

    /**
     * @param ExecutionContextInterface $context
     * @param $payload
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->start !== null && $this->end !== null && $this->start > $this->end)
        {
            $context->buildViolation('Od nesmí být větší než do')
                ->atPath('start')
                ->addViolation();
        }
    }

} 