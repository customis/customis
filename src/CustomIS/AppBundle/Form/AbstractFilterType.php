<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 27.09.2016
 * Time: 14:44
 */

namespace CustomIS\AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractFilterType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'error_bubbling' => true,
            'csrf_protection'   => false,
            'validation_groups' => ['filtering'],
            'method' => 'get',
        ));
    }
}