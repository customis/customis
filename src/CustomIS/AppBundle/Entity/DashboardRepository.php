<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 11.01.2016
 * Time: 22:44
 */

namespace CustomIS\AppBundle\Entity;

use CustomIS\AppBundle\Graph\Graph;
use CustomIS\AppBundle\Graph\Plot\Bar;
use CustomIS\AppBundle\Graph\Plot\Line;
use CustomIS\AppBundle\Graph\Plot\Pie;
use CustomIS\AppBundle\Graph\Value;
use Doctrine\DBAL\Connection;
use Sonata\IntlBundle\Templating\Helper\DateTimeHelper;

/**
 * Class DashboardRepository
 *
 * @package CustomIS\AppBundle\Entity
 */
class DashboardRepository
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var DateTimeHelper
     */
    private $dateTimeHelper;

    /**
     * DashboardRepository constructor.
     *
     * @param Connection     $connection
     * @param DateTimeHelper $dateTimeHelper
     */
    public function __construct(Connection $connection, DateTimeHelper $dateTimeHelper)
    {
        $this->connection = $connection;
        $this->dateTimeHelper = $dateTimeHelper;
    }

    /**
     * @param int $pocetLet
     *
     * @return Graph
     */
    public function getSkodneUdalostiVLetech(int $pocetLet): Graph
    {
        $data = $this->connection->fetchAll('
            WITH t AS (
                            SELECT	*
                            FROM (
                        SELECT	EXTRACT(YEAR FROM s.datum_vzniku) AS rok,
                            ts.id AS typ_stavu_id,
                            row_number() OVER (PARTITION BY ssu.cislo_skodne_udalosti ORDER BY ssu.datum_vytvoreni DESC)
                        FROM evidence_skod.stav_skodne_udalosti ssu
                            JOIN evidence_skod.typ_stavu ts ON ssu.typ_stavu = ts.id
                            JOIN evidence_skod.skodna_udalost s ON ssu.cislo_skodne_udalosti = s.id
                    ) sub WHERE row_number = 1
                        )
            SELECT	roky AS rok,
                ts.popis AS stav,
                lower(unaccent(ts.popis)) AS stav_slug,
                COUNT(t.*) AS pocet
            FROM generate_series(EXTRACT(YEAR FROM NOW())::integer - (:pocet_let - 1), EXTRACT(YEAR FROM NOW())::integer) AS roky
                CROSS JOIN evidence_skod.typ_stavu ts
                LEFT JOIN t ON t.rok = roky AND ts.id = t.typ_stavu_id
            GROUP BY 1, 2, ts.poradi
            ORDER BY 1, ts.poradi
        ', [
            'pocet_let' => ($pocetLet <= 0) ? 0 : $pocetLet,
        ]);

        $graph = new Graph();

        foreach ($data as $row) {
            $graph->addPlot(new Bar($row['stav']), $row['stav_slug'])->addValue(
                $row['pocet'],
                $row['rok']
            );
        }

        return $graph;
    }

    /**
     * @param int $rok
     *
     * @return Graph
     */
    public function getSkodneUdalostiVAktualnimRoce($rok): Graph
    {
        $data = $this->connection->fetchAll('
            WITH t AS (
                SELECT	EXTRACT(YEAR FROM s.datum_vzniku) AS rok,
                    ts.id AS typ_stavu_id,
                    row_number() OVER (PARTITION BY ssu.cislo_skodne_udalosti ORDER BY ssu.datum_vytvoreni DESC)
                FROM evidence_skod.stav_skodne_udalosti ssu
                    JOIN evidence_skod.typ_stavu ts ON ssu.typ_stavu = ts.id
                    JOIN evidence_skod.skodna_udalost s ON ssu.cislo_skodne_udalosti = s.id
                WHERE EXTRACT(YEAR FROM s.datum_vzniku) = :rok
            )
            SELECT	ts.popis AS stav,
                    COUNT(t.*) AS pocet
            FROM evidence_skod.typ_stavu ts
                LEFT JOIN t ON ts.id = t.typ_stavu_id AND t.row_number = 1
            GROUP BY 1, ts.poradi
            ORDER BY ts.poradi
        ', [
            'rok' => $rok,
        ]);

        $graph = new Graph();
        $pie = new Pie('Stavy');
        $pie->setLabelCallback(function (Value $value) {
            return sprintf('%s: %s', $value->getOption('stav'), $value->getValue());
        });
        $graph->addPlot($pie);

        foreach ($data as $row) {
            $pie->addValue(new Value($row['pocet'], [
                'stav' => $row['stav'],
            ]), $row['stav']);
        }

        return $graph;
    }

    /**
     * @param int $rok
     *
     * @return Graph
     */
    public function getSkodneUdalostiVAktualnimRoceDivize(int $rok): Graph
    {
        $data = $this->connection->fetchAll('
            SELECT	d.nazev,
                    COUNT(su.*) AS pocet
            FROM evidence_skod.divize d
                LEFT JOIN evidence_skod.skodna_udalost su ON su.divize_id = d.id AND EXTRACT(YEAR FROM su.datum_vzniku) = :rok
            GROUP BY 1
            ORDER BY 1
        ', [
            'rok' => $rok,
        ]);

        $graph = new Graph();
        $pie = $graph->addPlot(new Pie('Divize'));

        foreach ($data as $row) {
            $pie->addValue($row['pocet'], $row['nazev']);
        }

        return $graph;
    }

    /**
     * @param int $rok
     *
     * @return Graph
     */
    public function getSkodneUdalostiVAktualnimRoceDruhSkody(int $rok): Graph
    {
        $data = $this->connection->fetchAll('
            SELECT	d.popis,
                    COUNT(su.*) AS pocet
            FROM evidence_skod.druh_skody d
                LEFT JOIN evidence_skod.skodna_udalost su ON su.druh_skody_id = d.id AND EXTRACT(YEAR FROM su.datum_vzniku) = :rok
            GROUP BY 1
            ORDER BY 1
        ', [
            'rok' => $rok,
        ]);

        $graph = new Graph();
        $pie = $graph->addPlot(new Pie('Druhy škod'));

        foreach ($data as $row) {
            $pie->addValue($row['pocet'], $row['popis']);
        }

        return $graph;
    }

    /**
     * @param int $pocetMesicu
     *
     * @return Graph
     */
    public function getSkodneUdalostiDiviziOPoslednich12Mesicu(int $pocetMesicu): Graph
    {
        $data = $this->connection->fetchAll("
            WITH t AS (
                SELECT date_trunc('month', gs)::date AS datum
                FROM generate_series( NOW() - CAST(:pocet_mesicu || ' month' AS interval), NOW(), '1 day'::interval) gs
                GROUP BY 1
            )
            SELECT	t.datum,
                    d.nazev AS divize,
                    COUNT(su.*) AS pocet
            FROM t
                CROSS JOIN evidence_skod.divize d
                LEFT JOIN evidence_skod.skodna_udalost su ON d.id = su.divize_id
                                AND date_trunc('month', su.datum_vzniku)::date = t.datum
            GROUP BY 1, 2
            ORDER BY 1, 2
        ", [
            'pocet_mesicu' => $pocetMesicu,
        ]);

        $callback = function (Value $value) {
            return sprintf('%s: %s', $value->getOption('divize'), $value->getValue());
        };

        $graph = new Graph();
        foreach ($data as $row) {
            $graph->addPlot(new Line($row['divize']), $row['divize'])->addValue(
                new Value($row['pocet'], [
                    'divize' => $row['divize'],
                ]),
                $this->dateTimeHelper->format($row['datum'], 'MMM Y')
            )
                  ->setLabelCallback($callback);
        }

        return $graph;
    }

    /**
     * @return array
     */
    public function getRokySkod(): array
    {
        return array_map('current', $this->connection->fetchAll('
            SELECT	EXTRACT(YEAR FROM datum_vzniku) AS rok
            FROM evidence_skod.skodna_udalost
            GROUP BY 1
            ORDER BY 1
        '));
    }
}
