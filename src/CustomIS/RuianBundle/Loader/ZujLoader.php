<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Loader;

use CustomIS\RuianBundle\Entity\Momc;
use CustomIS\RuianBundle\Entity\Obec;
use CustomIS\RuianBundle\Entity\Zuj\Momc as ZujMomc;
use CustomIS\RuianBundle\Entity\Zuj\Obec as ZujObec;
use CustomIS\RuianBundle\Exception\UnsupportedOperationException;

/**
 * Class ZujLoader
 *
 * @package CustomIS\RuianBundle\Loader
 */
class ZujLoader extends AbstractRuianLoader
{
    /**
     * @param \SimpleXMLElement $element
     * @param string $xmlFile
     */
    public function load(\SimpleXMLElement $element, string $xmlFile)
    {
        $ns = $element->getNamespaces(true);

        /** @var ZujMomc|ZujObec $zuj */
        $zuj = null;
        if ($element->getName() === 'Momc') {
            $mci = $element->children($ns['mci']);
            $momc = $this->getEntityManager()->getRepository(Momc::class)->findOneByCode($mci->{'Kod'});
            if (($zuj = $this->getEntityManager()->getRepository(ZujMomc::class)->findOneByCode($mci->{'Kod'})) !== null) {
                $zuj->setMomc($momc);
            } else {
                $zuj = new ZujMomc((string) $mci->{'Kod'}, $momc);
                $this->getEntityManager()->persist($zuj);
            }
        }

        if ($element->getName() === 'Obec') {
            $obi = $element->children($ns['obi']);
            $obec = $this->getEntityManager()->getRepository(Obec::class)->findOneByCode($obi->{'Kod'});
            if (($zuj = $this->getEntityManager()->getRepository(ZujObec::class)->findOneByCode($obi->{'Kod'})) !== null) {
                $zuj->setObec($obec);
            } else {
                $zuj = new ZujObec((string) $obi->{'Kod'}, $obec);
                $this->getEntityManager()->persist($zuj);
            }
        }
        $this->getEntityManager()->flush();
    }

    /**
     * @return array
     */
    public function getXmlNodeName(): array
    {
        return ['vf:Momc', 'vf:Obec'];
    }

    /**
     * @param float|null $symplifyBorderRation
     */
    public function setSymplifyBorderRation($symplifyBorderRation)
    {
        throw new UnsupportedOperationException('Cannot symplify ZUJ borders.');
    }
}
