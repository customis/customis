<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 11.01.2016
 * Time: 23:08
 */
namespace CustomIS\AppBundle\Chart;

class Bar
{
    /**
     * @var Column[]
     */
    private $columns = [];

    /**
     * @param Column $column
     */
    public function addColumn(Column $column)
    {
        $this->columns[] = $column;
    }

    /**
     * @return Column[]
     */
    public function getColumns()
    {
        return $this->columns;
    }

    public function toC3ChartColumnsArray($json = false)
    {
        if (count($this->getColumns()) == 0)
        {
            return [];
        }

        $return = [];
        $columns = $this->getColumns();
        if ($json === false)
        {
            if (count($columns[0]->getValues()) === 1 && isset($columns[0]->getValues()[0]))
            {
                $return[] = array_merge(['category'], array_map(function (Column $column) {
                    return $column->getLabel();
                }, $columns));
                $return[] = array_merge(['Suma'], array_map(function (Column $column) {
                    return $column->getValues()[0];
                }, $columns));
            }
            else
            {
                $return = [
                    array_merge(['x'], array_keys($columns[0]->getValues()))
                ];

                foreach ($columns as $column)
                {
                    $return[] = $column->toC3ChartColumnArray();
                }
            }
        }
        else
        {
            $return = array_map(function (Column $column) {
                return $column->toC3ChartColumnArray(true);
            }, $columns);
        }

        return $return;

    }

}
