<?php

namespace CustomIS\FormAnnotationBundle\Annotation;


/**
 * @Annotation
 * @Target({"PROPERTY"})
 * @Attributes({
 *   @Attribute("value", type = "string", required = true)
 * })
 */
class Form
{
    private $value;

    public function __construct(array $data)
    {
        $this->value = $data['value'];
    }
    
    public function getValue()
    {
        return $this->value;
    }
}