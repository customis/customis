<?php
/**
 * Created by PhpStorm.
 * User: T
 * Date: 24.2.2017
 * Time: 14:17
 */

namespace CustomIS\AppBundle\EventListener;


use CustomIS\AppBundle\Annotation\SessionRoute;
use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Util\ClassUtils;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class SessionRouteListener
{
    /**
     * @var Reader $reader
     */
    private $reader;

    /**
     * @var SessionInterface $session
     */
    private $session;

    /**
     * SessionRouteListener constructor.
     * @param Reader $reader
     * @param SessionInterface $session
     */
    public function __construct(Reader $reader, SessionInterface $session)
    {
        $this->reader = $reader;
        $this->session = $session;
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        if (!is_array($controller = $event->getController())) {
            return;
        }

        list($object, $method) = $controller;

        $className = ClassUtils::getClass($object);

        $reflectionClass = new \ReflectionClass($className);
        $reflectionMethod = $reflectionClass->getMethod($method);

        $annotation = $this->reader->getMethodAnnotation($reflectionMethod, SessionRoute::class);

        if ($annotation !== null) {
            $query = $event->getRequest()->query->all();
            $sessionKey = 'customis_session_route/'.$event->getRequest()->get('_route');
            $this->session->set($sessionKey, $query);
        }
    }
}