<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CustomIS\LdapBundle\Security\Core\Authentication\Provider;

use CustomIS\LdapBundle\Security\Core\User\LdapUser;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Provider\UserAuthenticationProvider;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AuthenticationServiceException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Zend\Ldap\Exception\LdapException;
use Zend\Ldap\Ldap;

/**
 * LdapBindAuthenticationProvider authenticates a user against an LDAP server.
 *
 * The only way to check user credentials is to try to connect the user with its
 * credentials to the ldap.
 *
 * @author Charles Sarrazin <charles@sarraz.in>
 */
class LdapBindAuthenticationProvider extends UserAuthenticationProvider
{
    /**
     * @var UserProviderInterface
     */
    private $userProvider;

    /**
     * @var Ldap
     */
    private $ldap;

    /**
     * @var bool
     */
    private $allowAuthenticationWithoutBind;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Constructor.
     *
     * @param UserProviderInterface $userProvider
     * @param UserCheckerInterface  $userChecker
     * @param string                $providerKey
     * @param Ldap                  $ldap
     * @param LoggerInterface       $logger
     * @param bool                  $hideUserNotFoundExceptions
     * @param bool                  $allowAuthenticationWithoutBind
     */
    public function __construct(
        UserProviderInterface $userProvider,
        UserCheckerInterface $userChecker,
        $providerKey,
        Ldap $ldap,
        LoggerInterface $logger,
        $hideUserNotFoundExceptions = true,
        $allowAuthenticationWithoutBind = false
    ) {
        parent::__construct($userChecker, $providerKey, $hideUserNotFoundExceptions);

        $this->userProvider = $userProvider;
        $this->ldap = $ldap;
        $this->allowAuthenticationWithoutBind = $allowAuthenticationWithoutBind;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    protected function retrieveUser($username, UsernamePasswordToken $token)
    {
        if ('NONE_PROVIDED' === $username) {
            throw new UsernameNotFoundException('Username can not be null');
        }

        return $this->userProvider->loadUserByUsername($username);
    }

    /**
     * {@inheritdoc}
     */
    protected function checkAuthentication(UserInterface $user, UsernamePasswordToken $token)
    {
        if (!$user instanceof LdapUser) {
            throw new AuthenticationServiceException('retrieveUser() must return a LdapUser.');
        }

        if (!$this->allowAuthenticationWithoutBind) {
            $password = $token->getCredentials();

            try {
                $this->ldap->bind($user->getDn()->toString(), $password);
            } catch (LdapException $e) {
                throw new BadCredentialsException('The presented password is invalid.');
            }
        } else {
            $this->logger->alert('Successfull LDAP login without binding to LDAP server. Use only in development!');
        }
    }
}
