<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Okres
 *
 * @package CustomIS\RuianBundle\Entity
 * @ORM\Entity(repositoryClass="OkresRepository")
 * @ORM\Table(schema="ruian")
 */
class Okres extends AbstractBaseRuianEntity
{
    /**
     * @var Kraj1960
     * @ORM\ManyToOne(targetEntity="CustomIS\RuianBundle\Entity\Kraj1960")
     * @ORM\JoinColumn(nullable=false)
     */
    private $kraj1960;

    /**
     * @var Vusc
     * @ORM\ManyToOne(targetEntity="CustomIS\RuianBundle\Entity\Vusc", inversedBy="okresy")
     * @ORM\JoinColumn(nullable=false)
     */
    private $vusc;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $czso_code;

    /**
     * @var array|ArrayCollection
     * @ORM\OneToMany(targetEntity="CustomIS\RuianBundle\Entity\Obec", mappedBy="okres")
     */
    private $obce;

    /**
     * Pou constructor.
     *
     * @param Kraj1960 $kraj
     * @param Vusc     $vusc
     * @param int      $code
     * @param string   $name
     */
    public function __construct(Kraj1960 $kraj, Vusc $vusc, $code, $name)
    {
        $this->kraj1960 = $kraj;
        $this->vusc = $vusc;
        parent::__construct($code, $name);
    }

    /**
     * @return Kraj1960
     */
    public function getKraj1960(): Kraj1960
    {
        return $this->kraj1960;
    }

    /**
     * @param Kraj1960 $kraj1960
     */
    public function setKraj1960(Kraj1960 $kraj1960)
    {
        $this->kraj1960 = $kraj1960;
    }

    /**
     * @return Vusc
     */
    public function getVusc(): Vusc
    {
        return $this->vusc;
    }

    /**
     * @param Vusc $vusc
     */
    public function setVusc(Vusc $vusc)
    {
        $this->vusc = $vusc;
    }

    /**
     * @return string|null
     */
    public function getCzsoCode()
    {
        return $this->czso_code;
    }

    /**
     * @param string|null $czso_code
     */
    public function setCzsoCode($czso_code)
    {
        $this->czso_code = $czso_code;
    }

    /**
     * @return array
     */
    public function getZsj(): array
    {
        return array_reduce(iterator_to_array($this->getObce()), function (array $carry, Obec $obec) {
            return array_merge($carry, $obec->getZsj());
        }, []);
    }

    /**
     * @return Obec[]|ArrayCollection
     */
    public function getObce()
    {
        return $this->obce;
    }
}
