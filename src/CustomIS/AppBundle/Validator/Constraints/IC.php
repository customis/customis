<?php

namespace CustomIS\AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class IC
 *
 * @package CustomIS\AppBundle\Validator\Constraints
 */
class IC extends Constraint
{
    const INVALID_IC_ERROR = '248a76eb-4428-4b61-a2d2-167cce773cde';

    protected static $errorNames = [
        self::INVALID_IC_ERROR => 'INVALID_IC_ERROR',
    ];

    public $message = 'IČ nemá správný formát';
}
