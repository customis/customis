<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 17.03.2016
 * Time: 23:36
 */

namespace CustomIS\AppBundle\Aop\Aspect;

use CustomIS\AppBundle\Annotation\Menu;
use Go\Aop\Aspect;
use Go\Aop\Intercept\MethodInvocation;
use Go\Lang\Annotation\Before;
use Knp\Menu\Iterator\RecursiveItemIterator;
use Knp\Menu\MenuItem;
use Knp\Menu\Provider\MenuProviderInterface;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\ExpressionLanguage\SyntaxError;
use Symfony\Component\HttpFoundation\RequestStack;

class MenuAspect implements Aspect
{
    /**
     * @var MenuProviderInterface
     */
    private $menuProvider;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * MenuAspect constructor.
     * @param MenuProviderInterface $menuProvider
     * @param RequestStack $requestStack
     * @internal param RequestStack $request
     */
    public function __construct(MenuProviderInterface $menuProvider, RequestStack $requestStack)
    {
        $this->menuProvider = $menuProvider;
        $this->requestStack = $requestStack;
    }


    /**
     * @Before("@execution(CustomIS\AppBundle\Annotation\Menu)")
     * @param MethodInvocation $invocation
     */
    public function beforeMethodExecution(MethodInvocation $invocation)
    {
        $expressionLanguage = new ExpressionLanguage();
        $expressionLanguage->register(
            'instanceof',
            function ($str1, $str2) {
                return sprintf('(instanceof(%1$s,%2$s))', $str1, $str2);
            },
            function ($arguments, $request, $class) {
                return $request instanceof $class;
            }
        );

        $menu = $this->menuProvider->get('main');
        $request = $this->requestStack->getMasterRequest();

        $annotations = array_values(array_filter($invocation->getMethod()->getAnnotations(), function ($annotation) {
            return $annotation instanceof Menu;
        }));
        $count = count($annotations);
        /**
         * @var integer $i
         * @var Menu $menuAnnotation
         */
        foreach ($annotations as $i => $menuAnnotation)
        {
            $iterator = new \RecursiveIteratorIterator(
                new RecursiveItemIterator($menu),
                \RecursiveIteratorIterator::SELF_FIRST
            );
            foreach ($iterator as $item)
            {
                /** @var MenuItem $item */
                foreach ($item->getExtra('routes', []) as $route)
                {
                    $params = $menuAnnotation->getParams();
                    foreach ($params as &$row)
                    {
                        try
                        {
                            $row = $expressionLanguage->evaluate($row, [
                                'request' => $request
                            ]);
                        }
                        catch (SyntaxError $e)
                        {

                        }
                        unset($row);
                    }

                    $intersect = array_intersect_assoc($route['parameters'], $params);
                    if ($menuAnnotation->getValue() === $route['route']
                        && count($intersect) === count($route['parameters'])
                    )
                    {
                        $menuItemParams = [];
                        if ($menuAnnotation->getRoute() !== null)
                        {
                            $menuItemParams['route'] = $menuAnnotation->getRoute();

                            $routeParameters = $menuAnnotation->getRouteParams();
                            foreach ($routeParameters as &$row)
                            {
                                try
                                {
                                    $row = $expressionLanguage->evaluate($row, [
                                        'request' => $request
                                    ]);
                                }
                                catch (SyntaxError $e)
                                {

                                }
                                unset($row);
                            }
                            $menuItemParams['routeParameters'] = $routeParameters;
                        }

                        $label = $menuAnnotation->getLabel();
                        try
                        {
                            $label = $expressionLanguage->evaluate($menuAnnotation->getLabel(), [
                                'request' => $request
                            ]);
                        }
                        catch (SyntaxError $e)
                        {

                        }

                        $menuItem = $item->addChild($label, $menuItemParams)
                            ->setDisplay(false)
                        ;

                        if (($i+1) === $count)
                        {
                            $menuItem->setCurrent(true);
                        }
                    }
                }
            }
        }
    }
}
