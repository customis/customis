<?php

declare(strict_types = 1);

namespace CustomIS\AppBundle\Graph;

/**
 * Class Value
 *
 * @package CustomIS\AppBundle\Graph
 */
class Value implements \JsonSerializable
{
    /**
     * @var mixed
     */
    private $value;

    /**
     * @var array
     */
    private $options = [];

    /**
     * Value constructor.
     *
     * @param mixed $value
     */
    public function __construct($value, array $options = [])
    {
        $this->value = $value;
        $this->options = $options;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function hasOption($key)
    {
        return isset($this->options[$key]);
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function getOption($key)
    {
        return $this->options[$key];
    }

    /**
     * @param string $key
     * @param mixed  $value
     */
    public function setOption($key, $value)
    {
        $this->options[$key] = $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return mixed
     */
    public function jsonSerialize()
    {
        return $this->value;
    }
}
