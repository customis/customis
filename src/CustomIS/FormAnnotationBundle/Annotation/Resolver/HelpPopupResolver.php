<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 14.11.2015
 * Time: 11:03
 */

namespace CustomIS\FormAnnotationBundle\Annotation\Resolver;

use \CustomIS\FormAnnotationBundle\Annotation\Annotation;
use \CustomIS\FormAnnotationBundle\Annotation\HelpPopup;
use \CustomIS\FormAnnotationBundle\Annotation\Label;
use \CustomIS\FormAnnotationBundle\Annotation\QueryBuilder;
use \CustomIS\FormAnnotationBundle\Form\ElementBuilderInterface;
use \CustomIS\FormAnnotationBundle\Form\QueryBuilderInterface;
use \Doctrine\ORM\EntityRepository;
use \Symfony\Component\DependencyInjection\ContainerInterface;
use \Symfony\Component\Form\Exception\UnexpectedTypeException;
use \Symfony\Component\Form\FormInterface;

class HelpPopupResolver extends AbstractResolver
{
    public function supports($annotation)
    {
        return $annotation instanceof HelpPopup;
    }

    public function setData($value)
    {
        $this->getElementBuilder()->setHelpPopup($value);
    }
}