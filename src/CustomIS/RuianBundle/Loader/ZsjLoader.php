<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Loader;

use CustomIS\RuianBundle\Entity\Obec;
use CustomIS\RuianBundle\Entity\Okres;
use CustomIS\RuianBundle\Entity\Pou;
use CustomIS\RuianBundle\Entity\Zsj;
use CustomIS\RuianBundle\Geometry\GeometryEntityFactory;

/**
 * Class ZsjLoader
 *
 * @package CustomIS\RuianBundle\Loader
 */
class ZsjLoader extends AbstractRuianLoader
{
    /**
     * @param \SimpleXMLElement $element
     * @param string $xmlFile
     */
    public function load(\SimpleXMLElement $element, string $xmlFile)
    {
        $ns = $element->getNamespaces(true);
        $zji = $element->children($ns['zji']);

        $katastralniUzemiObecMap = $this->getObecId($xmlFile, $zji);

        $kuiKod = (string) $zji->{'KatastralniUzemi'}->children($ns['kui'])->{'Kod'};
        if (isset($katastralniUzemiObecMap[$kuiKod])) {
            /** @var Obec $obec */
            if (($obec = $this->getEntityManager()->getRepository(Obec::class)->findOneByCode($katastralniUzemiObecMap[$kuiKod])) !== null
            ) {
                if (($zsj = $this->getEntityManager()->getRepository(Zsj::class)->findOneByCode($zji->{'Kod'})) !== null) {
                    /** @var Zsj $zsj */
                    $zsj->setName((string) $zji->{'Nazev'});
                    $zsj->setObec($obec);
                } else {
                    $zsj = new Zsj(
                        $obec,
                        (int) $zji->{'Kod'},
                        $zji->{'Nazev'}
                    );

                    $this->getEntityManager()->persist($zsj);
                }

                $geometry = null;
                $zsj->clearGeometry();
                foreach ($element->xpath('.//gml:Polygon[@gml:id]') as $polygon) {
                    $geometry = GeometryEntityFactory::build($polygon, $this->getSymplifyBorderRation());
                    $this->getEntityManager()->persist($geometry);
                    $zsj->addGeometry($geometry);
                }
                $this->getEntityManager()->flush();
                
                if ($geometry !== null) {
                    $this->getEntityManager()->detach($geometry);
                }
                $this->getEntityManager()->detach($zsj);
                $this->getEntityManager()->detach($obec);
            }
        } else {
            //echo "Neexistuje kod: $kuiKod\n";
        }
    }


    private function getObecId(string $xmlFile, \SimpleXMLElement $element)
    {
        static $map;

        if ($map === null) {
            $map = [];
            $xmlReader = new \XMLReader();
            $xmlReader->open($xmlFile);
            while ($xmlReader->read()) {
                if ($xmlReader->nodeType === \XMLReader::ELEMENT
                    && in_array($xmlReader->name, ['vf:KatastralniUzemi'])
                    && $xmlReader->getAttribute('gml:id') !== null
                ) {
                    $simplexml = new \SimpleXMLElement($xmlReader->readOuterXML());
                    $ns = $simplexml->getNamespaces(true);
                    $katastralniUzemiKod = (string) $simplexml->children($ns['kui'])->Kod;
                    $obecKod = (string) $simplexml->children($ns['kui'])->Obec->children($ns['obi'])->Kod;
                    $map[$katastralniUzemiKod] = $obecKod;
                }
            }
            $xmlReader->close();

        }

        return $map;
    }

    /**
     * @return array
     */
    public function getXmlNodeName(): array
    {
        return ['vf:Zsj'];
    }
}
