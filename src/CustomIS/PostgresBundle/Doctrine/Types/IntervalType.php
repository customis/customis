<?php

namespace CustomIS\PostgresBundle\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class IntervalType
 *
 * @package CustomIS\PostgresBundle\Doctrine\Types
 */
class IntervalType extends Type
{
    const INTERVAL = 'interval';

    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'interval';
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return \DateInterval|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value !== null) {
            $value = str_replace('mons', 'month', $value);

            $matches = [];
            preg_match_all('~(?<datum>\d+ [a-z]+) ?(?<cas>\d{2}:\d{2}:\d{2})?~u', $value, $matches);

            $temp = $matches['datum'] ?? [];

            $casArray = array_values(array_filter($matches['cas']));
            if (count($casArray) > 0) {
                $cas = $casArray[0];
                list($hour, $minute, $second) = array_map(function ($val) {
                    return (int)$val;
                }, explode(':', $cas));

                $temp[] = $hour.' hours';
                $temp[] = $minute.' minutes';
                $temp[] = $second.' seconds';
            }

            return \DateInterval::createFromDateString(implode(' ', $temp));
        }

        return null;
    }

    /**
     * @param \DateInterval    $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->format('%y years %m months %d days %h hours %i mins %s secs');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::INTERVAL;
    }

    /**
     * @return bool
     */
    public function canRequireSQLConversion()
    {
        return true;
    }

    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform)
    {
        return "CAST($sqlExpr AS interval)";
    }
}
