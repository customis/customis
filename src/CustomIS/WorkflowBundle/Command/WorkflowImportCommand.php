<?php
/**
 * Created by PhpStorm.
 * User: to068928
 * Date: 1.12.2016
 * Time: 14:08
 */

namespace CustomIS\WorkflowBundle\Command;

use CustomIS\WorkflowBundle\Entity\Workflow;
use CustomIS\WorkflowBundle\Entity\WorkflowPlace;
use CustomIS\WorkflowBundle\Entity\WorkflowTransition;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Exception\DatabaseObjectNotFoundException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

class WorkflowImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('customis:workflow:update')
            ->setDescription('Synchronize DB with workflows.yml config file.')
            ->addArgument(
                'workflow_config_file',
                InputArgument::OPTIONAL,
                'Workflow config file',
                '/config/workflows.yml'
            )
            ->addArgument(
                'update_mode',
                InputArgument::OPTIONAL,
                'Update mode? ',
                'update'
            )
        ;
    }

    protected function execute (InputInterface $input, OutputInterface $output)
    {
        $path = $this   ->getContainer()
                        ->get('kernel')
                        ->getRootDir()
                            . $input->getArgument('workflow_config_file');
        $truncateMode = $input->getArgument('update_mode') == 'truncate' ? true : false;
        $env = $this->getContainer()
                    ->get('kernel')
                    ->getEnvironment();
        $em = $this ->getContainer()
                    ->get('doctrine')
                    ->getEntityManager();

        $output->writeln('Načítání souboru ' . $path);
        $data = Yaml::parse(file_get_contents($path));

        $em->getConnection()->beginTransaction();
        try {
            if($truncateMode === true
                && !empty($data)
                && is_array($data)
                && $env == 'dev'
            ) {
                $query = $em->createQuery("DELETE FROM ".WorkflowTransition::class);
                $query->execute();
                $query = $em->createQuery("DELETE FROM ".WorkflowPlace::class);
                $query->execute();
                $query = $em->createQuery("DELETE FROM ".Workflow::class);
                $query->execute();
            }

            $workflowRepository = $em->getRepository(Workflow::class);
            $workflowPlaceRepository = $em->getRepository(WorkflowPlace::class);
            $workflowTransitionRepository = $em->getRepository(WorkflowTransition::class);

            foreach($data['custom_is_workflow']['workflows'] as $workflowName => $workflow) {
                $workflowEntity = $workflowRepository->findOneBy(['name' => $workflowName]);
                if(empty($workflowEntity)) {
                    $workflowEntity = new Workflow($workflowName);
                    $em->persist($workflowEntity);
                }
                else {
                    $workflowEntity->setName($workflowName);
                }
                $em->flush();
                foreach($workflow['places'] AS $placeName => $place) {
                    $placeEntity = $workflowPlaceRepository->findOneBy(
                        [
                            'name' => $placeName,
                            'workflow' => $workflowEntity
                        ]
                    );
                    $placeTitle = isset($place['title']) ? $place['title'] : $placeName;
                    $placeType = isset($place['type']) ? $place['type'] : null;
                    if(empty($placeEntity)) {
                        $placeEntity = new WorkflowPlace(
                            $workflowEntity,
                            $placeName,
                            $placeTitle,
                            $placeType
                        );
                        $em->persist($placeEntity);
                    }
                    else {
                        $placeEntity->setTitle($placeTitle)
                            ->setType($placeType);
                    }
                }
                $em->flush();

                foreach($workflow['transitions'] AS $transitionName => $transition) {
                    $transitionEntity = $workflowTransitionRepository->findOneBy(
                        [
                            'name' => $transitionName,
                            'workflow' => $workflowEntity
                        ]
                    );
                    $title = isset($transition['title']) ? $transition['title'] : $transitionName;
                    $action = isset($transition['action_name']) ? $transition['action_name'] : $title;
                    $allowedRoles = isset($transition['allowed_roles']) ? $transition['allowed_roles'] : [];
                    $type = isset($transition['type']) ? $transition['type'] : null;
                    if(empty($transitionEntity)) {
                        $transitionEntity = new WorkflowTransition(
                            $workflowEntity,
                            $transitionName,
                            $title,
                            $transition['interactive'],
                            $action,
                            $type,
                            $allowedRoles
                        );
                        $em->persist($transitionEntity);
                    }
                    else {
                        $transitionEntity->setTitle($title)
                            ->setActionName($action)
                            ->setInteractive($transition['interactive'])
                            ->setType($type)
                            ->setAllowedRoles($allowedRoles);
                    }
                    foreach ($transitionEntity->getFroms() AS $from) {
                        $transitionEntity->getFroms()->removeElement($from);
                        $from->getTransitionsFrom()->removeElement($transitionEntity);
                    }
                    $froms = is_array($transition['from']) ? $transition['from'] : [$transition['from']];
                    foreach($froms AS $from) {
                        $placeEntity = $workflowPlaceRepository->findOneBy(
                            [
                                'name' => $from,
                                'workflow' => $workflowEntity
                            ]
                        );
                        if($placeEntity == null) {
                            throw new \Exception('Entity name ' . $workflowEntity->getName() . ':' . $from . ' not found');
                        }
                        $transitionEntity->addFrom($placeEntity);
                    }

                    foreach ($transitionEntity->getTos() AS $to) {
                        $transitionEntity->getTos()->removeElement($to);
                        $to->getTransitionsTo()->removeElement($transitionEntity);
                    }
                    $tos = is_array($transition['to']) ? $transition['to'] : [$transition['to']];
                    foreach($tos AS $to) {
                        $placeEntity = $workflowPlaceRepository->findOneBy(
                            [
                                'name' => $to,
                                'workflow' => $workflowEntity
                            ]
                        );
                        $transitionEntity->addTo($placeEntity);
                    }
                }
            }
            $em->flush();

            $em->getConnection()->commit();
        }
        catch (DBALException $e)
        {
            $em->getConnection()->rollback();
            $output->writeln('Chyba při mazání dat z DB" ' . $e->getMessage());
        }
    }
}