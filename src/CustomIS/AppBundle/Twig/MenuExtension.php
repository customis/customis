<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 19.01.2016
 * Time: 15:45
 */
namespace CustomIS\AppBundle\Twig;

use Knp\Menu\ItemInterface;
use Knp\Menu\Matcher\MatcherInterface;
use Knp\Menu\Twig\Helper;

class MenuExtension extends \Twig_Extension
{
    /**
     * @var Helper
     */
    private $helper;

    /**
     * @var MatcherInterface
     */
    private $matcher;

    /**
     * MenuExtension constructor.
     * @param Helper $helper
     * @param MatcherInterface $matcher
     */
    public function __construct(Helper $helper, MatcherInterface $matcher)
    {
        $this->helper = $helper;
        $this->matcher = $matcher;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('knp_menu_get_current_item', function ($menu) {
                $rootItem = $this->helper->get($menu);
                $currentItem = $this->retrieveCurrentItem($rootItem);
                if (null === $currentItem) {
                    $currentItem = $rootItem;
                }
                return $currentItem;
            })
        ];
    }

    /**
     * @param ItemInterface $item
     *
     * @return ItemInterface|null
     */
    private function retrieveCurrentItem(ItemInterface $item)
    {
        $currentItem = null;
        if ($this->matcher->isCurrent($item)) {
            return $item;
        }
        if ($this->matcher->isAncestor($item)) {
            foreach ($item->getChildren() as $child) {
                $currentItem = $this->retrieveCurrentItem($child);
                if (null !== $currentItem) {
                    break;
                }
            }
        }
        return $currentItem;
    }

    public function getName()
    {
        return 'menu';
    }
}