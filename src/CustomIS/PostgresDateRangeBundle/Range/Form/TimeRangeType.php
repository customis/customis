<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 24.7.14
 * Time: 11:09
 */

namespace CustomIS\PostgresDateRangeBundle\Range\Form;


use CustomIS\PostgresDateRangeBundle\Range\TimeRange;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Valid;

class TimeRangeType extends AbstractType implements DataMapperInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $start_constraints = [];
        if (!$options['infinite_start'])
        {
            $start_constraints[] = new NotBlank();
        }

        $end_constraints = [];
        if (!$options['infinite_end'])
        {
            $end_constraints[] = new NotBlank();
        }

        $builder
            ->add('start', TimeType::class, [
                'attr' => [
                    'data-type' => 'start',
                    'title' => 'Od'
                ],
                'required' => !$options['infinite_start'],
                'label' => $options['start_title'],
                'constraints' => $start_constraints
            ])
            ->add('end', TimeType::class, [
                'attr' => [
                    'data-type' => 'end',
                    'title' => 'Do'
                ],
                'required' => !$options['infinite_end'],
                'label' => $options['end_title'],
                'constraints' => $end_constraints
            ])
            ->setDataMapper($this)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => TimeRange::class,
            'infinite_start' => false,
            'infinite_end' => false,
            'start_title' => 'Od',
            'end_title' => 'Do',
            'attr' => [
                'class' => 'customis-form-time-range'
            ],
            'empty_data' => null,
            'constraints' => new Valid(),
        ));

        $resolver->setAllowedValues('infinite_start', [true, false]);
        $resolver->setAllowedValues('infinite_end', [true, false]);
    }

    /**
     * @param TimeRange $data
     * @param \Symfony\Component\Form\FormInterface[] $forms
     */
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);
        if ($data !== null)
        {
            $forms['start']->setData($data->getStart() ?? $data->getStart());
            $forms['end']->setData($data->getEnd() ?? $data->getEnd());
        }
    }

    /**
     * @param \Symfony\Component\Form\FormInterface[] $forms
     * @param TimeRange $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);
        $data = new TimeRange(
            $forms['start']->getData(),
            $forms['end']->getData()
        );

    }
} 
