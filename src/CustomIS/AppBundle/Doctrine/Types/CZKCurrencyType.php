<?php
/**
 * Created by PhpStorm.
 * User: to068928
 * Date: 6.1.2017
 * Time: 13:45
 */

namespace CustomIS\AppBundle\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Money\Currencies\ISOCurrencies;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money;
use Money\Parser\DecimalMoneyParser;

/**
 * Class CZKCurrencyType
 *
 * @package CustomIS\AppBundle\Doctrine\Types
 */
class CZKCurrencyType extends Type
{
    const CZKCURRENCY = 'czkcurrency';

    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'numeric(10,2)';
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return Money
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value !== null) {
            $moneyParser = new DecimalMoneyParser(new ISOCurrencies());

            return $moneyParser->parse($value, 'CZK');
        }

        return null;
    }

    /**
     * @param Money            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        $moneyFormatter = new DecimalMoneyFormatter(new ISOCurrencies());

        return $moneyFormatter->format($value);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::CZKCURRENCY;
    }
}
