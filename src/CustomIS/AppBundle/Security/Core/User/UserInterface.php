<?php
/**
 * Created by PhpStorm.
 * User: ja068850
 * Date: 14.11.2016
 * Time: 10:22
 */

namespace CustomIS\AppBundle\Security\Core\User;


use Symfony\Component\Security\Core\Role\Role;

/**
 * Interface UserInterface
 *
 * @package CustomIS\AppBundle\Security\Core\User
 */
interface UserInterface
{
    /**
     * @param array|string[]|Role[] $roles
     *
     * @return void
     */
    public function setRoles($roles);

    /**
     * @param string|Role $role
     *
     * @return void
     */
    public function addRole($role);
}