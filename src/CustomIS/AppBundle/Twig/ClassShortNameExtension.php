<?php

namespace CustomIS\AppBundle\Twig;

class ClassShortNameExtension extends \Twig_Extension
{
    
    public function getFunctions()
    {
        return [
            'class_short_name' =>  new \Twig_SimpleFunction('class_short_name', function ($class) {
                return  (new \ReflectionClass($class))->getShortName();
            })
        ];
    }

    public function getName()
    {
        return 'class_short_name_extension';
    }
}
