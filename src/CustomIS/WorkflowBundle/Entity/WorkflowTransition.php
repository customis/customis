<?php
/**
 * Created by PhpStorm.
 * User: to068928
 * Date: 1.12.2016
 * Time: 13:22
 */

namespace CustomIS\WorkflowBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CustomIS\AppBundle\Doctrine\UuidIdTrait;

/**
 * WorkflowTransition
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class WorkflowTransition
{
    use UuidIdTrait;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    private $actionName;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $interactive;

    /**
     * @var Workflow
     * @ORM\ManyToOne(targetEntity="Workflow", inversedBy="transitions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $workflow;

    /**
     * @ORM\ManyToMany(targetEntity="WorkflowPlace", inversedBy="transitions_from")
     * @ORM\JoinTable(name="workflow_transition_from")
     */
    private $froms;

    /**
     * @ORM\ManyToMany(targetEntity="WorkflowPlace", inversedBy="transitions_to")
     * @ORM\JoinTable(name="workflow_transition_to")
     */
    private $tos;

    /**
     * @var array
     * @ORM\Column(type="jsonb", options={"default"="{}"})
     */
    private $allowedRoles =[];

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return WorkflowTransition
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return WorkflowTransition
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set interactive
     *
     * @param boolean $interactive
     *
     * @return WorkflowTransition
     */
    public function setInteractive($interactive)
    {
        $this->interactive = $interactive;

        return $this;
    }

    /**
     * Get interactive
     *
     * @return boolean
     */
    public function getInteractive()
    {
        return $this->interactive;
    }

    /**
     * Set workflow
     *
     * @param \CustomIS\WorkflowBundle\Entity\Workflow $workflow
     *
     * @return WorkflowTransition
     */
    public function setWorkflow(\CustomIS\WorkflowBundle\Entity\Workflow $workflow)
    {
        $this->workflow = $workflow;

        return $this;
    }

    /**
     * Get workflow
     *
     * @return \CustomIS\WorkflowBundle\Entity\Workflow
     */
    public function getWorkflow()
    {
        return $this->workflow;
    }

    /**
     * Set actionName
     *
     * @param string $actionName
     *
     * @return WorkflowTransition
     */
    public function setActionName($actionName)
    {
        $this->actionName = $actionName;

        return $this;
    }

    /**
     * Get actionName
     *
     * @return string
     */
    public function getActionName()
    {
        return !empty($this->actionName) ? $this->actionName : $this->getTitle();
    }
    /**
     * Constructor
     */
    public function __construct(Workflow $workflow, $name, $title, $interactive = true, $actionName = null,
                                $type = null, $allowedRoles = [])
    {
        $this->workflow = $workflow;
        $this->name = $name;
        $this->title = $title;
        $this->interactive = $interactive;
        $this->actionName = $actionName;
        $this->allowedRoles = $allowedRoles;
        $this->type = $type;
        $this->froms = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add from
     *
     * @param \CustomIS\WorkflowBundle\Entity\WorkflowPlace $from
     *
     * @return WorkflowTransition
     */
    public function addFrom(\CustomIS\WorkflowBundle\Entity\WorkflowPlace $from)
    {
        $this->froms[] = $from;

        return $this;
    }

    /**
     * Remove from
     *
     * @param \CustomIS\WorkflowBundle\Entity\WorkflowPlace $from
     */
    public function removeFrom(\CustomIS\WorkflowBundle\Entity\WorkflowPlace $from)
    {
        $this->froms->removeElement($from);
    }

    /**
     * Get froms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFroms()
    {
        return $this->froms;
    }

    /**
     * Add to
     *
     * @param \CustomIS\WorkflowBundle\Entity\WorkflowPlace $to
     *
     * @return WorkflowTransition
     */
    public function addTo(\CustomIS\WorkflowBundle\Entity\WorkflowPlace $to)
    {
        $this->tos[] = $to;

        return $this;
    }

    /**
     * Remove to
     *
     * @param \CustomIS\WorkflowBundle\Entity\WorkflowPlace $to
     */
    public function removeTo(\CustomIS\WorkflowBundle\Entity\WorkflowPlace $to)
    {
        $this->tos->removeElement($to);
    }

    /**
     * Get tos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTos()
    {
        return $this->tos;
    }

    /**
     * Set allowedRoles
     *
     * @param jsonb $allowedRoles
     *
     * @return WorkflowTransition
     */
    public function setAllowedRoles($allowedRoles)
    {
        $this->allowedRoles = $allowedRoles;

        return $this;
    }

    /**
     * Get allowedRoles
     *
     * @return jsonb
     */
    public function getAllowedRoles()
    {
        return $this->allowedRoles;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return WorkflowTransition
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
