<?php

declare(strict_types = 1);

namespace CustomIS\AppBundle\Graph;

use CustomIS\AppBundle\Graph\Plot\AbstractPlot;

class Graph implements \JsonSerializable
{
    /**
     * @var AbstractPlot[]
     */
    private $plots = [];

    /**
     * @var bool
     */
    private $bottomLegend = false;

    /**
     * @param AbstractPlot $plot
     * @param string|null  $group
     *
     * @return AbstractPlot
     */
    public function addPlot(AbstractPlot $plot, $group = null)
    {
        if ($group !== null) {
            if (!$this->hasPlot($group)) {
                $this->plots[$group] = $plot;
            }

            return $this->plots[$group];
        } else {
            $this->plots[] = $plot;

            return $plot;
        }
    }

    /**
     * @param bool $bottomLegend
     */
    public function setBottomLegend(bool $bottomLegend)
    {
        $this->bottomLegend = $bottomLegend;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $return = [];

        if ($this->bottomLegend === true) {
            $return['legend']['orientation'] = 'h';
        }

        foreach ($this->plots as $plot) {
            $return['data'][] = $plot;
        }

        return $return;
    }

    /**
     * @param string|int $name
     *
     * @return bool
     */
    public function hasPlot($name): bool
    {
        return isset($this->plots[$name]);
    }

    /**
     * @param string|int $name
     *
     * @return AbstractPlot
     */
    public function getPlot($name): AbstractPlot
    {
        return $this->plots[$name];
    }
}
