<?php

namespace CustomIS\PostgresBundle\Form;

use CustomIS\PostgresBundle\Repeat\Repeat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RepeateType
 *
 * @package CustomIS\PostgresBundle\Form
 */
class RepeateType extends AbstractType implements DataMapperInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('times', IntegerType::class, [
                'label' => 'Počet opakování',
            ])
            ->add('interval', DateIntervalType::class, [
                'label'       => 'Opakovat po (dnech)',
                'widget'      => 'integer',
                'placeholder' => 'dnech',
                'with_years'  => false,
                'with_months' => false,
            ])
            ->setDataMapper($this)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Repeat::class,
            'empty_data' => null,
        ]);
    }

    /**
     * @param Repeat          $data
     * @param FormInterface[] $forms
     */
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);
        if ($data !== null) {
            $forms['times']->setData($data->getTimes());
            $forms['interval']->setData($data->getInterval());
        }
    }

    /**
     * @param FormInterface[] $forms
     * @param Repeat          $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);
        $data = new Repeat(
            $forms['times']->getData(),
            $forms['interval']->getData()
        );
    }
}
