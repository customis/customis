<?php

namespace CustomIS\AppBundle;

use CustomIS\AppBundle\DependencyInjection\Compiler\FilterCompilerPass;
use CustomIS\AppBundle\DependencyInjection\Compiler\FlysystemHashPluginPass;
use CustomIS\AppBundle\DependencyInjection\Compiler\PaginateCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CustomISAppBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new FilterCompilerPass());
        $container->addCompilerPass(new PaginateCompilerPass());
    }
}
