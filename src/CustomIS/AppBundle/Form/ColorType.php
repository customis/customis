<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 19.01.2016
 * Time: 23:08
 */
namespace CustomIS\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ColorType extends AbstractType
{
    public function getParent()
    {
        return TextType::class;
    }
}