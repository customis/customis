<?php

declare(strict_types = 1);

namespace CustomIS\AppBundle\Response;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class RedirectRouteResponse
 *
 * @package CustomIS\AppBundle\Response
 */
class RedirectRouteResponse extends RedirectRoute
{
    /**
     * RedirectRouteResponse constructor.
     *
     * @param string $routeName
     * @param array  $parameters
     * @param int    $statusCode
     * @param array  $headers
     */
    public function __construct(string $routeName, array $parameters = [], int $statusCode = 301, array $headers = [])
    {
        parent::__construct($routeName, $parameters, new Response('', $statusCode, $headers));
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->getResponse()->getStatusCode();
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->getResponse()->headers->all();
    }
}
