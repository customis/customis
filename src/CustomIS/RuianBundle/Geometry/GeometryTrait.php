<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Geometry;

use CustomIS\RuianBundle\Entity\Geometry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class GeometryTrait
 *
 * @package CustomIS\RuianBundle
 */
trait GeometryTrait
{
    /**
     * @var Geometry[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="CustomIS\RuianBundle\Entity\Geometry", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $geometry;

    /**
     * @return Geometry[]|ArrayCollection
     */
    public function getGeometry()
    {
        return $this->geometry;
    }

    /**
     * @param Geometry[]|ArrayCollection $geometry
     */
    public function setGeometry($geometry)
    {
        $this->geometry = $geometry;
    }

    /**
     * @param Geometry $geometry
     */
    public function addGeometry(Geometry $geometry)
    {
        $this->geometry[] = $geometry;
    }

    /**
     * @param Geometry $geometry
     */
    public function removeGeometry(Geometry $geometry)
    {
        $this->geometry->removeElement($geometry);
    }

    public function clearGeometry()
    {
        $this->geometry->clear();
    }
}
