<?php

declare(strict_types = 1);

namespace CustomIS\AppBundle\EventListener;

use CustomIS\AppBundle\Entity\File;
use CustomIS\AppBundle\Entity\FileInfo;
use CustomIS\AppBundle\Entity\Picture;
use Doctrine\ORM\EntityManager;
use Oneup\UploaderBundle\Event\PostPersistEvent;
use Oneup\UploaderBundle\Uploader\File\FlysystemFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class UploadFileListener
 *
 * @package CustomIS\AppBundle\EventListener
 */
class UploadFileListener
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * UploadFileListener constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param PostPersistEvent $e
     */
    public function onUpload(PostPersistEvent $e)
    {
        /** @var FlysystemFile $flysystem */
        $flysystem = $e->getFile();
        $response = $e->getResponse();

        /** @var string $hash */
        $hash = $flysystem->hash('sha1');

        /** @var File|null $file */
        if (($file = $this->entityManager->find(File::class, $hash)) === null) {
            $file = null;

            $mimeType = $flysystem->getMimetype();
            if (strpos($mimeType, 'image') === 0) {
                /** @var int $width */
                /** @var int $height */
                list($width, $height) = getimagesizefromstring($flysystem->read());
                $file = new Picture(
                    $hash,
                    $flysystem->getMimetype(),
                    $flysystem->getSize(),
                    $width,
                    $height
                );
            } else {
                $file = new File(
                    $hash,
                    $flysystem->getMimetype(),
                    $flysystem->getSize()
                );
            }

            $this->entityManager->persist($file);
        }

        /** @var UploadedFile $requestFile */
        $requestFile = $e->getRequest()->files->get('file');
        $fileInfo = new FileInfo(
            $file,
            $requestFile->getClientOriginalName()
        );
        $this->entityManager->persist($fileInfo);
        $this->entityManager->flush();

        $response['file'] = $fileInfo;
    }
}
