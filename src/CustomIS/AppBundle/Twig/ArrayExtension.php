<?php

namespace CustomIS\AppBundle\Twig;

/**
 * Class ArrayExtension
 *
 * @package CustomIS\AppBundle\Twig
 */
class ArrayExtension extends \Twig_Extension
{
    /**
     * @return array|\Twig_SimpleFilter[]
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('unset', function (array $array, $key) {
                unset($array[$key]);

                return $array;
            }),
        ];
    }
}
