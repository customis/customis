<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 05.11.2016
 * Time: 16:40
 */

namespace CustomIS\AppBundle\Doctrine;


use Doctrine\ORM\Mapping as ORM;

trait UuidIdTrait
{
    /**
     * @var string
     * @ORM\Id()
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
}
