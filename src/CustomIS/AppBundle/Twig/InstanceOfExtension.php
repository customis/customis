<?php

namespace CustomIS\AppBundle\Twig;

class InstanceOfExtension extends \Twig_Extension
{
    
    public function getTests()
    {
        return [
            'instanceof' =>  new \Twig_SimpleTest('instanceof', function ($var, $instance) {
                return $var instanceof $instance;
            })
        ];
    }

    public function getName()
    {
        return 'instanceof_extension';
    }
}
