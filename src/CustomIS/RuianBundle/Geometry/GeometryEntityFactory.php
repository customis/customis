<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Geometry;

use CustomIS\PostgresBundle\Geometry\Point;
use CustomIS\PostgresBundle\Geometry\Polygon;
use CustomIS\RuianBundle\Entity\Geometry;
use CustomIS\RuianBundle\Geometry\Convertor\JtskToWgsPositionList;
use Location\Coordinate;
use Location\Polyline;
use Location\Processor\Polyline\SimplifyBearing;

class GeometryEntityFactory
{
    /**
     * @param \SimpleXMLElement $simpleXMLElement
     * @param float             $symplifyBorderRation
     *
     * @return Geometry|null
     */
    public static function build(\SimpleXMLElement $simpleXMLElement, float $symplifyBorderRation = null)
    {
        if ($simpleXMLElement->getName() === 'Polygon') {
            $elementsExterior = $simpleXMLElement->xpath('.//gml:exterior');
            if (count($elementsExterior) === 1) {
                $polygon = self::createPolygonFromGml($elementsExterior[0]);
                $polygon = self::simplify($polygon, $symplifyBorderRation);
                $geometry = new Geometry($polygon);

                $elementsInterior = $simpleXMLElement->xpath('.//gml:interior');
                if (count($elementsInterior) === 1) {
                    $polygon = self::createPolygonFromGml($elementsInterior[0]);
                    $polygon = self::simplify($polygon, $symplifyBorderRation);
                    $geometry->setInnerBorder($polygon);
                }

                return $geometry;
            }
        }

        return null;
    }

    private static function createPolygonFromGml(\SimpleXMLElement $simpleXMLElement)
    {
        /** @var \SimpleXMLElement $posList */
        $posList = (string)$simpleXMLElement->xpath('.//gml:posList')[0];

        return new Polygon(iterator_to_array(JtskToWgsPositionList::convert($posList)));
    }

    /**
     * @param Polygon    $polygon
     * @param float|null $symplifyRation
     *
     * @return Polygon
     */
    public static function simplify(Polygon $polygon, float $symplifyRation = null)
    {
        if ($symplifyRation === null) {
            return $polygon;
        }

        $polyline = new Polyline();
        foreach ($polygon->getPoints() as $point) {
            $polyline->addPoint(new Coordinate($point->getX(), $point->getY()));
        }

        $simplifiedPolygon = new Polygon();
        $processor = new SimplifyBearing($symplifyRation);
        /** @var Coordinate $simplifiedPoint */
        foreach ($processor->simplify($polyline)->getPoints() as $simplifiedPoint) {
            $simplifiedPolygon->addPoint(new Point($simplifiedPoint->getLat(), $simplifiedPoint->getLng()));
        }

        return $simplifiedPolygon;
    }
}
