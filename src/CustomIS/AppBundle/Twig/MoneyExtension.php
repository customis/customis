<?php

namespace CustomIS\AppBundle\Twig;

use Money\Currencies\ISOCurrencies;
use Money\Formatter\IntlMoneyFormatter;
use Money\Money;
use Symfony\Component\HttpFoundation\RequestStack;

class MoneyExtension extends \Twig_Extension
{
    /**
     * @var RequestStack
     */
    private $requestStack;


    /**
     * MoneyExtension constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('format_money', function (Money $money) {
                $request = $this->requestStack->getMasterRequest();
                $locale = $request->getLocale();

                $numberFormatter = new \NumberFormatter($locale, \NumberFormatter::CURRENCY);
                $intlMoneyFormatter = new IntlMoneyFormatter($numberFormatter, new ISOCurrencies());

                return $intlMoneyFormatter->format($money);
            }),
        ];
    }

    public function getName()
    {
        return 'money_extension';
    }
}
