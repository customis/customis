<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 14.11.2015
 * Time: 11:03
 */

namespace CustomIS\FormAnnotationBundle\Annotation\Resolver;

use CustomIS\FormAnnotationBundle\Annotation\Required;

class RequiredResolver extends AbstractResolver
{
    public function supports($annotation)
    {
        return $annotation instanceof Required;
    }

    public function setData($value)
    {
        $this->getElementBuilder()->setReqired($value);
    }


}