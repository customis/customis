<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 07.01.2016
 * Time: 15:03
 */
namespace CustomIS\AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class PaginateCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('knp_paginator'))
        {
            return;
        }

        $taggedServices = $container->findTaggedServiceIds('customis.paginate');

        foreach ($taggedServices as $id => $tags)
        {
            $container
                ->findDefinition($id)
                ->addMethodCall('setPaginator', [
                    new Reference('knp_paginator')
                ])
            ;
        }
    }
}