<?php

namespace CustomIS\LdapBundle;

use CustomIS\LdapBundle\DependencyInjection\Security\Factory\FormLoginLdapFactory;
use CustomIS\LdapBundle\DependencyInjection\Security\UserProvider\LdapFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CustomISLdapBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new FormLoginLdapFactory());
        $extension->addUserProviderFactory(new LdapFactory());
    }
}
