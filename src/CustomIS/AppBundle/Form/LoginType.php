<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 04.12.2015
 * Time: 10:54
 */

namespace CustomIS\AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_username', TextType::class, [
                'label' => 'Přihlašovací jméno'
            ])
            ->add('_password', PasswordType::class, [
                'label' => 'Heslo'
            ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }


}
