<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 23.11.2015
 * Time: 12:57
 */

namespace CustomIS\AppBundle\Menu;


use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use CustomIS\AppBundle\Event\MenuEvent;

class MainMenuBuilder
{
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * MainMenuBuilder constructor.
     * @param FactoryInterface $factory
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        FactoryInterface $factory,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->factory = $factory;
        $this->eventDispatcher = $eventDispatcher;
    }


    public function create(array $options)
    {
        /** @var ItemInterface $menu */
        static $menu;

        if ($menu === null)
        {
            $menu = $this->factory->createItem('side-menu');

            $menu->addChild('Dashboard', [
                'route' => 'homepage'
            ]);

            $event = new MenuEvent($menu);
            $this->eventDispatcher->dispatch(MenuEvent::NAME, $event);
        }

        return $menu;
    }

    public function createBreadcrumbs()
    {

    }
}
