<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 27.10.2015
 * Time: 21:19
 */

namespace CustomIS\FormAnnotationBundle\Tests\Entity;

use Doctrine\ORM\Mapping as ORM;
use \CustomIS\FormAnnotationBundle\Annotation AS Form;

/**
 * @ORM\Entity()
 */
class Test
{
    /**
     * @ORM\Column(type="string", length=255)
     * @Form\Form("test")
     * @Form\Label("Jméno")
     * @Form\Order(after="test2")
     */
    private $test;

    /**
     * @ORM\Column(type="text")
     * @Form\Form("test")
     * @Form\Type("textarea")
     * @Form\Order("first")
     */
    private $test2;

    /**
     * @return mixed
     */
    public function getTest()
    {
        return $this->test;
    }

    /**
     * @param mixed $test
     */
    public function setTest($test)
    {
        $this->test = $test;
    }

    /**
     * @return mixed
     */
    public function getTest2()
    {
        return $this->test2;
    }

    /**
     * @param mixed $test2
     */
    public function setTest2($test2)
    {
        $this->test2 = $test2;
    }

    /**
     * @return mixed
     */
    public function getTest3()
    {
        return $this->test3;
    }

    /**
     * @param mixed $test3
     */
    public function setTest3($test3)
    {
        $this->test3 = $test3;
    }


}