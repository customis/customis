<?php

namespace CustomIS\FormAnnotationBundle\Annotation;

use Symfony\Component\Form\FormBuilderInterface;

abstract class Annotation
{
    /**
     * @var string
     */
    private $form;

    /**
     * Annotation constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        if (isset($data['form']))
        {
            $this->form = $data['form'];
        }

    }


    /**
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    abstract public function getValue();
}