<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 15.05.2016
 * Time: 14:56
 */

namespace CustomIS\RequestLogBundle\EventListener;


use CustomIS\RequestLogBundle\Entity\Log;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class LogRequestListener
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    /**
     * LogRequestListener constructor.
     * @param EntityManager $entityManager
     * @param Container $container
     * @param TokenStorageInterface $tokenStorage
     * @internal param TokenStorage $tokenStorage
     */
    public function __construct(EntityManager $entityManager, TokenStorageInterface $tokenStorage,
        AuthorizationCheckerInterface $authChecker)
    {
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->authChecker = $authChecker;
    }

    public function onKernelRequest(GetResponseEvent $e)
    {
        $request = $e->getRequest();
        if($request->get('_route') !== null && $request->get('_route')[0] !== '_')
        {
            $this->entityManager->persist(new Log($request, $e->isMasterRequest(), $this->authChecker, $this->tokenStorage->getToken()));
            $this->entityManager->flush();
        }
    }
}
