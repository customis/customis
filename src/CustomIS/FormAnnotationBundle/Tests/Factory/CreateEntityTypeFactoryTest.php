<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 27.10.2015
 * Time: 21:08
 */

namespace CustomIS\FormAnnotationBundle\Tests\Factory;


use CustomIS\FormAnnotationBundle\Exception\ClassNotFoundException;
use CustomIS\FormAnnotationBundle\Factory\CreateEntityTypeFactory;
use CustomIS\FormAnnotationBundle\Form\EventListener\EntityFormSubscriber;
use CustomIS\FormAnnotationBundle\Tests\Entity\Test;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Persistence\AbstractManagerRegistry;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Tests\Common\Persistence\ManagerRegistryTest;
use Ivory\OrderedForm\Extension\OrderedExtension;
use Ivory\OrderedForm\Extension\OrderedFormExtension;
use Ivory\OrderedForm\OrderedResolvedFormTypeFactory;
use Symfony\Bridge\Doctrine\Form\DoctrineOrmExtension;
use Symfony\Bridge\Doctrine\Form\DoctrineOrmTypeGuesser;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Validator\Exception\ValidatorException;

class CreateEntityTypeFactoryTest extends TypeTestCase
{
    /**
     * @var CreateEntityTypeFactory
     */
    protected $factory;

    public function setUp()
    {
        parent::setUp();

        $reader = new AnnotationReader();

        $containerBuilder = new ContainerBuilder();

        $entityFormSubscriber = new EntityFormSubscriber($reader, $containerBuilder);

        $validator = $this->getMock('Symfony\Component\Validator\Validator\ValidatorInterface');
        $metadata = $this->getMockBuilder('Symfony\Component\Validator\Mapping\ClassMetadata')->disableOriginalConstructor()->getMock();
        $validator->expects($this->any())->method('getMetadataFor')->will($this->returnValue($metadata));

        $formFactory = Forms::createFormFactoryBuilder()
            ->setResolvedTypeFactory(new OrderedResolvedFormTypeFactory())
            ->addExtensions([
                new ValidatorExtension($validator),
            ])
            ->addTypeExtensions([
                new OrderedFormExtension(),
            ])
            ->getFormFactory();

        $this->factory = new CreateEntityTypeFactory($formFactory, $entityFormSubscriber);
    }

    /**
     * @expectedException \CustomIS\FormAnnotationBundle\Exception\ClassNotFoundException
     */
    public function testCreateTypeFromNotExistingEntity()
    {
        $this->factory->createForm('CustomIS\FormAnnotationBundle\Tests\Entity\NotExistingEntityname');
    }

    public function testFormNameAutogeneratiopn()
    {
        $form = $this->factory->createForm(Test::class);
        $this->assertEquals($form->getName(), 'customis_formannotationbundle_tests_entity_test');
    }

    public function testFormCustomName()
    {
        $form = $this->factory->createForm(Test::class, 'pokus');
        $this->assertEquals($form->getName(), 'pokus');
    }

    public function testLabelAnnotation()
    {
        $form = $this->factory->createForm(Test::class, 'test')->setData(new Test());
        $this->assertTrue($form->has('test'));
        $this->assertEquals($form->get('test')->getConfig()->getOption('label'), 'Jméno');
    }

    public function testChangeTypeAnnotationFromTextToTextarea()
    {
        $form = $this->factory->createForm(Test::class, 'test')->setData(new Test());
        $this->assertTrue($form->has('test2'));
        $this->assertEquals($form->get('test2')->getConfig()->getType()->getName(), "textarea");
    }

    public function testFormElementOrder()
    {
        $form = $this->factory->createForm(Test::class, 'test')->setData(new Test())->createView();

        $this->assertEquals(key($form->children), 'test2');
        next($form->children);
        $this->assertEquals(key($form->children), 'test');
    }

}

