<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 16.06.2016
 * Time: 10:21
 */

namespace CustomIS\AppBundle\Pager\Doctrine;


use Doctrine\ORM\NativeQuery;
use Doctrine\ORM\Query\Parameter;
use Knp\Component\Pager\Event\ItemsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class NativeQuerySubscriber implements EventSubscriberInterface
{
    /**
     * @var RequestStack
     */
    private $requestStack;


    /**
     * NativeQuerySubscriber constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function items(ItemsEvent $event)
    {
        $target = $event->target;
        if ($target instanceof NativeQuery)
        {
            $request = $this->requestStack->getMasterRequest();
            $connection = $target->getEntityManager()->getConnection();

            $order = '';
            if ($request->query->has($event->options['sortFieldParameterName']) || isset($event->options['defaultSortFieldName']))
            {
                $sortField = $request->query->get(
                    $event->options['sortFieldParameterName'],
                    $event->options['defaultSortFieldName']
                );

                $direction = $request->query->get(
                    $event->options['sortDirectionParameterName'],
                    isset($event->options['defaultSortDirection']) ? $event->options['defaultSortDirection'] : 'ASC'
                );

                $fields = explode('+', $sortField);
                array_walk($fields, function (&$value) use ($connection, $direction) {
                    $value =
                        $connection->quoteIdentifier($value)
                        . ' '
                        . (in_array(strtolower($direction), ['asc', 'desc']) ? $direction : 'ASC')
                    ;
                });

                $order = ' ORDER BY ' . implode(', ', $fields);
            }

            $sql = trim($target->getSQL());
            $sqlCount = '';
            if (preg_match('~^SELECT~i', $sql))
            {
                $sqlCount = "
                            SELECT  COUNT(*)
                            FROM (
                            " . $sql . "
                            ) AS subquery
                        ";
                $sql =  "
                            SELECT  *
                            FROM (
                            " . $sql . "
                            " . $order . "
                            ) AS subquery
                        ";

                $params = [];
                /** @var Parameter $param */
                foreach ($target->getParameters() as $param)
                {
                    $params[$param->getName()] = $param->getValue();
                }

                $event->count = $connection->fetchColumn($sqlCount, $params);
            }

            $platform = $connection->getDatabasePlatform();
            $sql = $platform->modifyLimitQuery($sql, $event->getLimit(), $event->getOffset());

            $target->setSQL($sql);
            $result = $target->getResult();
            if ($target->hasHint('customis.dto'))
            {
                $dtoClass = $target->getHint('customis.dto');
                $result = array_map(function ($value) use ($dtoClass) {
                    $reflextion = new \ReflectionClass($dtoClass);
                    return $reflextion->newInstanceArgs(is_array($value) ? $value : [$value]);
                }, $result);
            }

            $event->items = $result;

            $event->stopPropagation();
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            'knp_pager.items' => array('items', 1) /* triggers before a standard array subscriber*/
        );
    }
}
