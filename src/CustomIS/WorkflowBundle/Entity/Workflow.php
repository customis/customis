<?php
/**
 * Created by PhpStorm.
 * User: to068928
 * Date: 1.12.2016
 * Time: 13:21
 */

namespace CustomIS\WorkflowBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CustomIS\AppBundle\Doctrine\UuidIdTrait;

/**
 * Workflow
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="WorkflowRepository")
 */
class Workflow
{
    use UuidIdTrait;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    private $title;

    /**
     * @var WorkflowPlace[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="WorkflowPlace", mappedBy="workflow", cascade={"remove", "persist"})
     */
    private $places;

    /**
     * @var WorkflowPlace[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="WorkflowTransition", mappedBy="workflow", cascade={"remove", "persist"})
     */
    private $transitions;

    /**
     * Constructor
     */
    public function __construct($name)
    {
        $this->name = $name;
        $this->places = new \Doctrine\Common\Collections\ArrayCollection();
        $this->transitions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Workflow
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Workflow
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add place
     *
     * @param \CustomIS\WorkflowBundle\Entity\WorkflowPlace $place
     *
     * @return Workflow
     */
    public function addPlace(\CustomIS\WorkflowBundle\Entity\WorkflowPlace $place)
    {
        $this->places[] = $place;

        return $this;
    }

    /**
     * Remove place
     *
     * @param \CustomIS\WorkflowBundle\Entity\WorkflowPlace $place
     */
    public function removePlace(\CustomIS\WorkflowBundle\Entity\WorkflowPlace $place)
    {
        $this->places->removeElement($place);
    }

    /**
     * Get places
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * Add transition
     *
     * @param \CustomIS\WorkflowBundle\Entity\WorkflowTransition $transition
     *
     * @return Workflow
     */
    public function addTransition(\CustomIS\WorkflowBundle\Entity\WorkflowTransition $transition)
    {
        $this->transitions[] = $transition;

        return $this;
    }

    /**
     * Remove transition
     *
     * @param \CustomIS\WorkflowBundle\Entity\WorkflowTransition $transition
     */
    public function removeTransition(\CustomIS\WorkflowBundle\Entity\WorkflowTransition $transition)
    {
        $this->transitions->removeElement($transition);
    }

    /**
     * Get transitions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransitions()
    {
        return $this->transitions;
    }
}
