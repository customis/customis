<?php

declare(strict_types = 1);

namespace CustomIS\AppBundle\EventListener;

use CustomIS\AppBundle\Response\RedirectRoute;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class RedirectListener
 *
 * @package CustomIS\AppBundle\EventListener
 */
class RedirectListener
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * RedirectListener constructor.
     *
     * @param UrlGeneratorInterface $router
     */
    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        $redirect = $event->getControllerResult();

        if (!($redirect instanceof RedirectRoute)) {
            return;
        }

        $response = $redirect->getResponse();
        if (!$response) {
            $response = new Response('', 302);
        }

        $response->headers->set('Location', $this->router->generate(
            $redirect->getRouteName(),
            $redirect->getParameters()
        ));
        $event->setResponse($response);
    }
}
