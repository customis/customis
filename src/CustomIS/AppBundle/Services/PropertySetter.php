<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 24.01.2016
 * Time: 15:22
 */
namespace CustomIS\AppBundle\Services;

class PropertySetter
{
    public static function set($object, $property, $value)
    {
        $reflection = new \ReflectionObject($object);
        $property = $reflection->getProperty($property);
        $property->setAccessible(true);
        $property->setValue($object, $value);
        $property->setAccessible(false);
    }
}