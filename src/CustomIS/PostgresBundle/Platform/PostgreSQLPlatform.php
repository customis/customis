<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 15.05.2016
 * Time: 18:50
 */

namespace CustomIS\PostgresBundle\Platform;


use Doctrine\DBAL\Platforms\PostgreSQL100Platform;

class PostgreSQLPlatform extends PostgreSQL100Platform
{
    public function getDateTimeFormatString()
    {
        return 'Y-m-d H:i:s.u';
    }

    public function getDateTimeTypeDeclarationSQL(array $fieldDeclaration)
    {
        return 'TIMESTAMP WITHOUT TIME ZONE';
    }

    public function getDateTimeTzTypeDeclarationSQL(array $fieldDeclaration)
    {
        return 'TIMESTAMP WITH TIME ZONE';
    }

    protected function initializeDoctrineTypeMappings()
    {
        parent::initializeDoctrineTypeMappings();
        $this->doctrineTypeMapping['repeat'] = 'repeat';
    }
}
