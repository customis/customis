<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Loader;

use CustomIS\RuianBundle\Entity\Obec;
use CustomIS\RuianBundle\Entity\Okres;
use CustomIS\RuianBundle\Entity\Pou;
use CustomIS\RuianBundle\Geometry\GeometryEntityFactory;

/**
 * Class ObecLoader
 *
 * @package CustomIS\RuianBundle\Loader
 */
class ObecLoader extends AbstractRuianLoader
{
    /**
     * @param \SimpleXMLElement $element
     * @param string $xmlFile
     */
    public function load(\SimpleXMLElement $element, string $xmlFile)
    {
        $ns = $element->getNamespaces(true);
        $obi = $element->children($ns['obi']);

        /** @var Okres $okres */
        /** @var Pou $pou */
        if (($okres = $this->getEntityManager()->getRepository(Okres::class)->findOneByCode($obi->{'Okres'}->children($ns['oki'])->{'Kod'})) !== null
            && ($pou = $this->getEntityManager()->getRepository(Pou::class)->findOneByCode($obi->{'Pou'}->children($ns['pui'])->{'Kod'})) !== null
        ) {
            if (($obec = $this->getEntityManager()->getRepository(Obec::class)->findOneByCode($obi->{'Kod'})) !== null) {
                /** @var Obec $obec */
                $obec->setName((string) $obi->{'Nazev'});
                $obec->setOkres($okres);
                $obec->setPou($pou);
            } else {
                $obec = new Obec(
                    $okres,
                    $pou,
                    (int) $obi->{'Kod'},
                    $obi->{'Nazev'}
                );

                $this->getEntityManager()->persist($obec);
            }

            $obec->clearGeometry();
            $geometry = null;
            foreach ($element->xpath('.//gml:Polygon[@gml:id]') as $polygon) {
                $geometry = GeometryEntityFactory::build($polygon, $this->getSymplifyBorderRation());
                $this->getEntityManager()->persist($geometry);
                $obec->addGeometry($geometry);
            }
            $this->getEntityManager()->flush();
            $this->getEntityManager()->detach($obec);
            $this->getEntityManager()->detach($okres);
            $this->getEntityManager()->detach($pou);
            if ($geometry !== null) {
                $this->getEntityManager()->detach($geometry);
            }
        }
    }

    /**
     * @return array
     */
    public function getXmlNodeName(): array
    {
        return ['vf:Obec'];
    }
}
