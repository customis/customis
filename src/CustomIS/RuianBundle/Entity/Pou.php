<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Entity;

use CustomIS\RuianBundle\Geometry\GeometryTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Pou
 *
 * @package CustomIS\RuianBundle\Entity
 * @ORM\Entity(repositoryClass="PouRepository")
 * @ORM\Table(schema="ruian")
 */
class Pou extends AbstractBaseRuianEntity
{
    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $czso_code;

    /**
     * Pou constructor.
     *
     * @param int    $code
     * @param string $name
     */
    public function __construct($code, $name)
    {
        parent::__construct($code, $name);
    }

    /**
     * @return string|null
     */
    public function getCzsoCode()
    {
        return $this->czso_code;
    }

    /**
     * @param string|null $czso_code
     */
    public function setCzsoCode($czso_code)
    {
        $this->czso_code = $czso_code;
    }
}
