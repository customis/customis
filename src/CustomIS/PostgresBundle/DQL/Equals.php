<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 12.06.2016
 * Time: 20:35
 */

namespace CustomIS\PostgresBundle\DQL;


use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class Equals extends FunctionNode
{
    private $expr1;
    private $expr2;

    public function getSql(SqlWalker $sqlWalker)
    {
        return $this->expr1->dispatch($sqlWalker) . ' = ' . $this->expr2->dispatch($sqlWalker);
    }

    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->expr1 = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_COMMA);
        $this->expr2 = $parser->StringExpression();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

}