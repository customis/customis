<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CustomIS\LdapBundle\DependencyInjection\Security\Factory;

use CustomIS\LdapBundle\Security\Core\Authentication\Provider\LdapBindAuthenticationProvider;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\FormLoginFactory;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * FormLoginLdapFactory creates services for form login ldap authentication.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 * @author Charles Sarrazin <charles@sarraz.in>
 */
class FormLoginLdapFactory extends FormLoginFactory
{
    /**
     * @param NodeDefinition $node
     */
    public function addConfiguration(NodeDefinition $node)
    {
        parent::addConfiguration($node);

        $node
            ->children()
            ->scalarNode('service')->end()
            ->end();
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return 'customis-form-login-ldap';
    }

    protected function createAuthProvider(ContainerBuilder $container, $id, $config, $userProviderId)
    {
        $definition = $container
            ->setDefinition(LdapBindAuthenticationProvider::class, new Definition(LdapBindAuthenticationProvider::class))
            ->setAutowired(true)
            ->setAutoconfigured(true)
            ->setArgument('$userProvider', new Reference($userProviderId))
            ->setArgument('$userChecker', new Reference('security.user_checker.'.$id))
            ->setArgument('$providerKey', $id)
        ;

        if ($container->hasParameter('ldap_allow_authenticate_without_bind')) {
            $definition->setArgument('$allowAuthenticationWithoutBind', $container->getParameter('ldap_allow_authenticate_without_bind'));
        }

        return LdapBindAuthenticationProvider::class;
    }
}
