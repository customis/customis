<?php

namespace CustomIS\RequestLogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Role\SwitchUserRole;

/**
 * Log
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Log
{
    /**
     * @var string
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", length=1)
     */
    private $datum;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @var string
     * @ORM\Column(type="string", length=8000)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $method;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $ajax;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $request;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $query;

    /**
     * @var array
     *
     * @ORM\Column(type="json_array")
     */
    private $session;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $cookie;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $files;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $header;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $scheme;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $masterRequest;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $switched_user;

    /**
     * Log constructor.
     */
    public function __construct(Request $request, $masterRequest, AuthorizationChecker $authChecker,
        TokenInterface $userToken = null)
    {
        $request_data = $request->request->all();
        if (isset($request_data['_password'])) {
            $request_data['_password'] = '*****';
        }
        $impersonatingUser = null;
        if ($userToken != null && $authChecker->isGranted('ROLE_PREVIOUS_ADMIN')) {
            foreach ($userToken->getRoles() AS $role) {
                if ($role instanceof SwitchUserRole) {
                    $impersonatingUser = $role->getSource()->getUser()->getUsername();
                    break;
                }
            }
        }
        $microtime = str_replace(',', '.', (string)microtime(true));
        $this->datum = \DateTimeImmutable::createFromFormat('U.u', $microtime);
        $this->username = ($userToken === null || $userToken instanceof AnonymousToken) ? null : $userToken->getUsername();
        $this->url = $request->getRequestUri();
        $this->method = $request->getMethod();
        $this->ajax = $request->isXmlHttpRequest();
        $this->request = $request_data;
        $this->query = $request->query->all();
        $this->session = $request->hasSession() ? $request->getSession()->all() : [];
        $this->cookie = $request->cookies->all();
        $this->files = $request->files->all();
        $this->header = $request->headers->all();
        $this->scheme = $request->getScheme();
        $this->masterRequest = $masterRequest;
        $this->ip = $request->getClientIp();
        $this->switched_user = $impersonatingUser;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get datum
     *
     * @return \DateTime
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get method
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Get ajax
     *
     * @return boolean
     */
    public function getAjax()
    {
        return $this->ajax;
    }

    /**
     * Get session
     *
     * @return array
     */
    public function getSession()
    {
        return $this->session;
    }
}

