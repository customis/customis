<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 18.11.2015
 * Time: 11:00
 */
namespace CustomIS\FormAnnotationBundle\Form;

interface ElementBuilderInterface
{
    /**
     * @return Element
     */
    public function getElement();

    public function setLabel($label);

    public function setOrder($order);
    
    public function setHelpText($helptext);
    
    public function setHelpPopup($help);

    public function setOptions($options);

    public function setType($type);

    public function setReqired($data);

    public function setQueryBuilder(callable $query_builder);
}