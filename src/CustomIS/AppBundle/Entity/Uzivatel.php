<?php

namespace CustomIS\AppBundle\Entity;

use CustomIS\AppBundle\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Uzivatel
 *
 * @ORM\MappedSuperclass()
 */
class Uzivatel implements UserInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=4000, unique=true)
     */
    protected $login;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default"=true})
     */
    protected $povolen = true;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    protected $jmeno;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    protected $email;

    /**
     * @var string[]|Role[]
     */
    protected $roles = [];

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return Uzivatel
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return boolean
     */
    public function isPovolen()
    {
        return $this->povolen;
    }

    /**
     * @param boolean $povolen
     */
    public function setPovolen($povolen)
    {
        $this->povolen = $povolen;
    }

    /**
     * @return string
     */
    public function getJmeno()
    {
        return $this->jmeno;
    }

    /**
     * @param string $jmeno
     */
    public function setJmeno($jmeno)
    {
        $this->jmeno = $jmeno;
    }

    /**
     * @return string
     */
    public function getCeleJmeno()
    {
        return $this->getJmeno() !== null
            ? $this->getJmeno()
            : $this->getLogin()
        ;
    }

    /**
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param array|string[]|Role[] $roles
     *
     * @return void
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    /**
     * @param string|Role $role
     *
     * @return void
     */
    public function addRole($role)
    {
        if (!in_array($role, $this->roles)) {
            $this->roles[] = $role;
        }
    }
}

