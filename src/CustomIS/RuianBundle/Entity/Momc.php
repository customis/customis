<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Entity;

use CustomIS\RuianBundle\Geometry\GeometryTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Momc
 *
 * @package CustomIS\RuianBundle\Entity
 * @ORM\Entity(repositoryClass="MomcRepository")
 * @ORM\Table(schema="ruian")
 */
class Momc extends AbstractBaseRuianEntity
{
    /**
     * @var Obec
     * @ORM\ManyToOne(targetEntity="CustomIS\RuianBundle\Entity\Obec", inversedBy="momc")
     * @ORM\JoinColumn(nullable=false)
     */
    private $obec;

    /**
     * @var Zsj[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="CustomIS\RuianBundle\Entity\Zsj", inversedBy="momc", cascade={"persist", "remove"})
     * @ORM\JoinTable(schema="ruian")
     */
    private $zsj;

    /**
     * Pou constructor.
     *
     * @param Obec   $obec
     * @param int    $code
     * @param string $name
     */
    public function __construct(Obec $obec, $code, $name)
    {
        $this->obec = $obec;
        $this->zsj = new ArrayCollection();
        parent::__construct($code, $name);
    }

    /**
     * @return Obec
     */
    public function getObec(): Obec
    {
        return $this->obec;
    }

    /**
     * @param Obec $obec
     */
    public function setObec(Obec $obec)
    {
        $this->obec = $obec;
    }

    /**
     * @param Zsj $zsj
     */
    public function addZsj(Zsj $zsj)
    {
        $this->zsj[] = $zsj;
    }

    /**
     * @param Zsj $zsj
     */
    public function removeZsj(Zsj $zsj)
    {
        $this->zsj->removeElement($zsj);
    }

    /**
     * @return Zsj[]|ArrayCollection
     */
    public function getZsj()
    {
        return array_map(function (Zsj $zsj) {
            return $zsj->getCode();
        }, iterator_to_array($this->zsj));
    }
}
