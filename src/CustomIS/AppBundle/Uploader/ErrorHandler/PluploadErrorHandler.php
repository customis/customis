<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 29.01.2016
 * Time: 12:01
 */
namespace CustomIS\AppBundle\Uploader\ErrorHandler;

use Exception;
use Oneup\UploaderBundle\Uploader\ErrorHandler\ErrorHandlerInterface;
use Oneup\UploaderBundle\Uploader\Response\AbstractResponse;

class PluploadErrorHandler implements ErrorHandlerInterface
{
    public function addException(AbstractResponse $response, Exception $exception)
    {
        $message = $exception->getMessage();
        $response['error'] = $message;
    }
}