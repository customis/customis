<?php

declare(strict_types = 1);

namespace CustomIS\AppBundle\Response;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class RedirectRoute
 *
 * @package CustomIS\AppBundle\Response
 */
class RedirectRoute
{
    /**
     * @var string
     */
    private $routeName;

    /**
     * @var array
     */
    private $parameters;

    /**
     * @var Response|null
     */
    private $response;

    /**
     * RedirectRoute constructor.
     *
     * @param string        $routeName
     * @param array         $parameters
     * @param Response|null $response
     */
    public function __construct(string $routeName, array $parameters = [], Response $response = null)
    {
        $this->routeName = $routeName;
        $this->parameters = $parameters;
        $this->response = $response;
    }

    /**
     * @return string
     */
    public function getRouteName(): string
    {
        return $this->routeName;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @return null|Response
     */
    public function getResponse(): Response
    {
        return $this->response;
    }
}
