<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Loader;

use Doctrine\ORM\EntityManager;

/**
 * Class AbstractRuianLoader
 *
 * @package CustomIS\RuianBundle\Loader
 */
abstract class AbstractRuianLoader
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var float|null
     */
    private $symplifyBorderRation;

    /**
     * AbstractRuianLoader constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param \SimpleXMLElement $element
     * @param string $xmlFile
     *
     * @return
     */
    abstract public function load(\SimpleXMLElement $element, string $xmlFile);

    /**
     * @return string[]
     */
    abstract public function getXmlNodeName(): array;

    /**
     * @return EntityManager
     */
    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    /**
     * @return float|null
     */
    public function getSymplifyBorderRation()
    {
        return $this->symplifyBorderRation;
    }

    /**
     * @param float|null $symplifyBorderRation
     */
    public function setSymplifyBorderRation($symplifyBorderRation)
    {
        $this->symplifyBorderRation = $symplifyBorderRation;
    }
}
