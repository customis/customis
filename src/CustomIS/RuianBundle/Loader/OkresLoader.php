<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Loader;

use CustomIS\RuianBundle\Entity\Kraj1960;
use CustomIS\RuianBundle\Entity\Okres;
use CustomIS\RuianBundle\Entity\Vusc;
use CustomIS\RuianBundle\Geometry\GeometryEntityFactory;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * Class OkresLoader
 *
 * @package CustomIS\RuianBundle\Loader
 */
class OkresLoader extends AbstractRuianLoader
{
    const CZSO_URL = 'http://apl.czso.cz/iSMS/cisexp.jsp?kodcis=101&format=0';

    /**
     * @var Client
     */
    private $client;

    /**
     * PouLoader constructor.
     *
     * @param EntityManager $entityManager
     * @param Client        $client
     */
    public function __construct(EntityManager $entityManager, Client $client)
    {
        parent::__construct($entityManager);
        $this->client = $client;
    }

    /**
     * @param \SimpleXMLElement $element
     * @param string $xmlFile
     */
    public function load(\SimpleXMLElement $element, string $xmlFile)
    {
        $ns = $element->getNamespaces(true);
        $oki = $element->children($ns['oki']);

        /** @var Kraj1960 $kraj */
        /** @var Vusc $vusc */
        if (($kraj = $this->getEntityManager()->getRepository(Kraj1960::class)->findOneByCode($oki->{'Kraj'}->children($ns['kri'])->{'Kod'})) !== null
            && ($vusc = $this->getEntityManager()->getRepository(Vusc::class)->findOneByCode($oki->{'Vusc'}->children($ns['vci'])->{'Kod'})) !== null
        ) {
            $promise = $this->client->getAsync(self::CZSO_URL);

            if (($okres = $this->getEntityManager()->getRepository(Okres::class)->findOneByCode($oki->{'Kod'})) !== null) {
                /** @var Okres $okres */
                $okres->setName((string) $oki->{'Nazev'});
                $okres->setKraj1960($kraj);
                $okres->setVusc($vusc);
            } else {
                $okres = new Okres(
                    $kraj,
                    $vusc,
                    $oki->{'Kod'},
                    $oki->{'Nazev'}
                );

                $this->getEntityManager()->persist($okres);
            }
            $promise->then(function (ResponseInterface $res) use ($okres, $oki) {
                $okres->setCzsoCode($this->getCzsoCode((string) $oki->{'Kod'}, $res->getBody()->getContents()));
            });

            $okres->clearGeometry();
            foreach ($element->xpath('.//gml:Polygon[@gml:id]') as $polygon) {
                $geometry = GeometryEntityFactory::build($polygon, $this->getSymplifyBorderRation());
                $this->getEntityManager()->persist($geometry);
                $okres->addGeometry($geometry);
            }

            $promise->wait();
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return array
     */
    public function getXmlNodeName(): array
    {
        return ['vf:Okres'];
    }

    private function getCzsoCode(string $kod, string $content)
    {
        $simplexml = simplexml_load_string($content);
        $search = $simplexml->xpath("//*[@akronym='KOD_RUIAN' and text()='$kod']/../..");
        if (count($search) === 1) {
            return (string) $search[0]->CHODNOTA;
        }

        return null;
    }
}
