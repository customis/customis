<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 12.06.2016
 * Time: 20:35
 */

namespace CustomIS\PostgresBundle\DQL;


use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class RangeOverlap extends FunctionNode
{
    public $range1 = null;
    public $range2 = null;

    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return '('
            . $this->range1->dispatch($sqlWalker)
            . ' && '
            . $this->range2->dispatch($sqlWalker)
            . ')'
        ;
    }

    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     *
     * @return void
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->range1 = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->range2 = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

}