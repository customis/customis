<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 18.11.2015
 * Time: 10:59
 */

namespace CustomIS\FormAnnotationBundle\Form;


class ElementBuilder implements ElementBuilderInterface
{

    private $element;

    /**
     * ElementBuilder constructor.
     */
    public function __construct()
    {
        $this->element = new Element();
    }

    public function setName($name)
    {
        $this->element->setName($name);
    }

    public function setLabel($label)
    {
        $this->element->setOption('label', $label);
    }

    public function setOrder($order)
    {
        $this->element->setOption('position', $order);
    }
    
    public function setHelpText($helptext)
    {
        $this->element->setOption('help_text', $helptext);
    }
    
    public function setHelpPopup($help)
    {
        $this->element->setOption('help_popup', $help);
    }

    public function setOptions($options)
    {
        $this->element->setOptions(array_merge($this->element->getOptions(), $options));
    }

    public function setType($type)
    {
        $this->element->setType($type);
    }

    public function setQueryBuilder(callable $query_builder)
    {
        $this->element->setOption('query_builder', $query_builder);
    }

    public function getElement()
    {
        return $this->element;
    }

    public function setReqired($data)
    {
        $this->element->setOption('required', $data);
    }
}