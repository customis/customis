<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 06.08.2016
 * Time: 16:52
 */

namespace CustomIS\AppBundle\Form;


use Money\Currency;
use Money\Money;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MoneyType extends \Symfony\Component\Form\Extension\Core\Type\MoneyType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addViewTransformer(new CallbackTransformer(
                function ($value) use ($options)
                {
                    if ($value !== null)
                    {
                        return $value->getAmount();
                    }

                    return null;
                },
                function ($value) use ($options)
                {
                    return new Money($value, new Currency($options['currency']));
                }
            ))
        ;

        parent::buildForm($builder, $options);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('divisor', 100);
    }

}