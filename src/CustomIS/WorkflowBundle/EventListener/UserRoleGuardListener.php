<?php
/**
 * Created by PhpStorm.
 * User: to068928
 * Date: 6.12.2016
 * Time: 9:53
 */

namespace CustomIS\WorkflowBundle\EventListener;

use CustomIS\WorkflowBundle\Component\Registry;
use O2\AppBundle\Exception\UnsupportedTransitionException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Workflow\Event\GuardEvent;
use Symfony\Component\Workflow\Workflow;

class UserRoleGuardListener implements EventSubscriberInterface
{
    protected $workflowRegistry;

    protected $authorizationChecker;

    public function __construct(Registry $workflowRegistry, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->workflowRegistry = $workflowRegistry;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function userRoleBaseGuard(GuardEvent $event)
    {
        $object = $event->getSubject();
        $transition = $event->getTransition();
        $workflow = $this->workflowRegistry->get($object);
        if(!($workflow instanceof Workflow)) {
            $event->setBlocked(true);
            return;
        }
        $workflowName = $workflow->getName();
        $transitionEntity = $this->workflowRegistry->getTransitionObject($workflowName, $transition->getName());
        if ($transitionEntity === null) {
            throw new UnsupportedTransitionException(
                sprintf('Transition %s doesnt exists in the DB. Run customis:workflow:update', $transition->getName())
            );
        }
        if(empty($transitionEntity->getAllowedRoles())) {
            return;
        }
        foreach($transitionEntity->getAllowedRoles() AS $role) {
            if($this->authorizationChecker->isGranted($role)) {
                return;
            }
        }

        $event->setBlocked(true);
    }

    public static function getSubscribedEvents()
    {
        return array(
            'workflow.guard' => array('userRoleBaseGuard'),
        );
    }
}