<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 14.11.2015
 * Time: 12:14
 */

namespace CustomIS\FormAnnotationBundle\Annotation\Resolver;


use CustomIS\FormAnnotationBundle\Annotation\Annotation;
use CustomIS\FormAnnotationBundle\Annotation\Options;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormInterface;

class OptionsResolver extends AbstractResolver
{
    public function supports($annotation)
    {
        return $annotation instanceof Options;
    }

    public function setData($value)
    {
        $this->getElementBuilder()->setOptions($value);
    }


    public function validate(Annotation $annotation)
    {
        return is_array($annotation->getValue());
    }

}