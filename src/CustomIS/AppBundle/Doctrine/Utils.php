<?php

declare(strict_types = 1);

namespace CustomIS\AppBundle\Doctrine;

use Doctrine\ORM\EntityManager;

class Utils
{
    /**
     * @param EntityManager $entityManager
     * @param string        $class
     *
     * @return string
     */
    public static function tableName(EntityManager $entityManager, $class)
    {
        $metadata = $entityManager->getClassMetadata($class);
        $tableName = $metadata->getTableName();
        if (($schemaName = $metadata->getSchemaName()) !== null) {
            $tableName = $schemaName.'.'.$tableName;
        }

        return $tableName;
    }

    /**
     * @param EntityManager $entityManager
     * @param string        $class
     * @param string        $property
     *
     * @param null|string   $tableAlias
     *
     * @return string
     */
    public static function columnName(EntityManager $entityManager, $class, $property, $tableAlias = null)
    {
        $metadata = $entityManager->getClassMetadata($class);
        $tableName = ($tableAlias ?? $metadata->getTableName()).'.';

        return $tableName.$metadata->getColumnName($property);
    }
}
