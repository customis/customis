<?php

namespace CustomIS\AppBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DateTimeTypeExtension
 *
 * @package CustomIS\AppBundle\Form\Extension
 */
class DateTimeTypeExtension extends AbstractTypeExtension
{
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $attr = $view->vars['attr'];
        $class = isset($attr['class'])
            ? $attr['class']
            : '';
        $attr['class'] = trim($class.' form_datetime');
        $view->vars['attr'] = $attr;

        $view['date']->vars['showDatepickerOnFocus'] = $options['show_datepicker_on_focus'];
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('show_datepicker_on_focus', true);
    }

    /**
     * @return mixed
     */
    public function getExtendedType()
    {
        return DateTimeType::class;
    }
}
