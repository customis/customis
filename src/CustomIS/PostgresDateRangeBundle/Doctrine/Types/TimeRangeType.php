<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 15.8.14
 * Time: 14:05
 */

namespace CustomIS\PostgresDateRangeBundle\Doctrine\Types;


use CustomIS\PostgresDateRangeBundle\Range\DateRange;
use CustomIS\PostgresDateRangeBundle\Range\TimeRange;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class TimeRangeType extends Type
{
    const DATE_RANGE = 'timerange'; // modify to match your type name

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'timerange';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $matches = [];
        if (!preg_match('~^(?P<lower_bracket>[\[(])(?P<lower_bound>[0-9:]*?),(?P<upper_bound>[0-9:]*?)(?P<upper_bracket>[\])])$~', $value, $matches))
        {
            throw new \Exception();
        }

        $lower_bound = null;
        if (!empty($matches['lower_bound']))
        {
            $lower_bound = new \DateTime($matches['lower_bound']);
        }

        $upper_bound = null;
        if (!empty($matches['upper_bound']))
        {
            $upper_bound = new \DateTime($matches['upper_bound']);
        }

        return new TimeRange($lower_bound, $upper_bound);
    }

    /**
     * @param DateRange $value
     * @param AbstractPlatform $platform
     * @return mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!empty($value->getEnd()))
        {
            $end = $value->getEnd()->format('H:i:s');
        }
        else
        {
            $end = '';
        }

        return "[" . $value->getStart()->format('H:i:s') . "'," . $end . "]";
    }

    public function getName()
    {
        return self::DATE_RANGE;
    }

    public function canRequireSQLConversion()
    {
        return true;
    }

    public function convertToPHPValueSQL($sqlExpr, $platform)
    {
        return "'[' || COALESCE(lower(" . $sqlExpr . ")::varchar, '') || ',' || COALESCE(upper(" . $sqlExpr . ")::varchar,'') ||']'";
    }

    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform)
    {
        return $sqlExpr;
    }

    public function getMappedDatabaseTypes(AbstractPlatform $platform)
    {
        return ['timerange'];
    }


} 