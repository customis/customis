<?php
/**
 * Created by PhpStorm.
 * User: to068928
 * Date: 2.1.2017
 * Time: 13:46
 */

namespace CustomIS\WorkflowBundle\Validator;

use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

interface WorkflowValidatorAuthCheckerInterface
{
    /**
     * @param AuthorizationCheckerInterface $authorizationChecker
     *
     * @return $this
     */
    public function setAuthorizationChecker(AuthorizationCheckerInterface $authorizationChecker);

}