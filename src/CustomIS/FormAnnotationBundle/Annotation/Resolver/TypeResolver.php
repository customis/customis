<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 14.11.2015
 * Time: 12:14
 */

namespace CustomIS\FormAnnotationBundle\Annotation\Resolver;


use CustomIS\FormAnnotationBundle\Annotation\Annotation;
use CustomIS\FormAnnotationBundle\Annotation\Type;
use Symfony\Component\Form\FormInterface;

class TypeResolver extends AbstractResolver
{
    public function supports($annotation)
    {
        return $annotation instanceof Type;
    }

    public function setData($value)
    {
        $this->getElementBuilder()->setType($value);
    }
}