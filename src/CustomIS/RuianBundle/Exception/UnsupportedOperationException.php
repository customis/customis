<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Exception;

class UnsupportedOperationException extends \RuntimeException
{
}
