<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 08.06.2016
 * Time: 9:25
 */

namespace CustomIS\AppBundle\Event;

use CustomIS\AppBundle\Entity\Uzivatel;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class LdapUserRoleResolverEvent
 *
 * @package CustomIS\AppBundle\Event
 */
class LdapUserRoleResolverEvent extends UserRoleResolverEvent
{
}
