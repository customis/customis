<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 07.01.2016
 * Time: 14:58
 */
namespace CustomIS\AppBundle\Filter;

use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdater;

interface FilterableInterface
{
    public function setFilterBuilder(FilterBuilderUpdater $filterBuilderUpdater);
}