<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Loader;

use CustomIS\RuianBundle\Entity\Vusc;
use CustomIS\RuianBundle\Geometry\GeometryEntityFactory;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * Class VuscLoader
 *
 * @package CustomIS\RuianBundle\Loader
 */
class VuscLoader extends AbstractRuianLoader
{
    const CZSO_URL = 'http://apl.czso.cz/iSMS/cisexp.jsp?kodcis=100&format=0';

    /**
     * @var Client
     */
    private $client;

    /**
     * VuscLoader constructor.
     *
     * @param EntityManager $entityManager
     * @param Client        $client
     */
    public function __construct(EntityManager $entityManager, Client $client)
    {
        parent::__construct($entityManager);
        $this->client = $client;
    }

    /**
     * @param \SimpleXMLElement $element
     * @param string $xmlFile
     */
    public function load(\SimpleXMLElement $element, string $xmlFile)
    {
        $ns = $element->getNamespaces(true);
        $vci = $element->children($ns['vci']);

        $promise = $this->client->getAsync(self::CZSO_URL);

        if (($vusc = $this->getEntityManager()->getRepository(Vusc::class)->findOneByCode($vci->{'Kod'})) !== null) {
            /** @var Vusc $vusc */
            $vusc->setName((string) $vci->{'Nazev'});
        } else {
            $vusc = new Vusc(
                (int) $vci->{'Kod'},
                $vci->{'Nazev'}
            );

            $this->getEntityManager()->persist($vusc);
        }

        $promise->then(function (ResponseInterface $res) use ($vusc, $vci) {
            $vusc->setCzsoCode($this->getCzsoCode((string) $vci->{'Kod'}, $res->getBody()->getContents()));
        });

        $vusc->clearGeometry();
        foreach ($element->xpath('.//gml:Polygon[@gml:id]') as $polygon) {
            $geometry = GeometryEntityFactory::build($polygon, $this->getSymplifyBorderRation());
            $this->getEntityManager()->persist($geometry);
            $vusc->addGeometry($geometry);
        }

        $promise->wait();
        $this->getEntityManager()->flush();
    }

    /**
     * @return array
     */
    public function getXmlNodeName(): array
    {
        return ['vf:Vusc'];
    }

    private function getCzsoCode(string $kod, string $content)
    {
        $simplexml = simplexml_load_string($content);
        $search = $simplexml->xpath("//*[@akronym='KOD_RUIAN' and text()='$kod']/../..");
        if (count($search) === 1) {
            return (string) $search[0]->CHODNOTA;
        }

        return null;
    }
}
