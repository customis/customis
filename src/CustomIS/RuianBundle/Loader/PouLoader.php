<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Loader;

use CustomIS\RuianBundle\Entity\Pou;
use CustomIS\RuianBundle\Geometry\GeometryEntityFactory;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class PouLoader extends AbstractRuianLoader
{
    const CZSO_URL = 'http://apl.czso.cz/iSMS/cisexp.jsp?kodcis=61&format=0';

    /**
     * @var Client
     */
    private $client;

    /**
     * PouLoader constructor.
     *
     * @param EntityManager $entityManager
     * @param Client        $client
     */
    public function __construct(EntityManager $entityManager, Client $client)
    {
        parent::__construct($entityManager);
        $this->client = $client;
    }

    /**
     * @param \SimpleXMLElement $element
     * @param string $xmlFile
     */
    public function load(\SimpleXMLElement $element, string $xmlFile)
    {
        $ns = $element->getNamespaces(true);
        $pui = $element->children($ns['pui']);

        $promise = $this->client->getAsync(self::CZSO_URL);
        if (($pou = $this->getEntityManager()->getRepository(Pou::class)->findOneByCode($pui->{'Kod'})) !== null) {
            /** @var Pou $pou */
            $pou->setName((string) $pui->{'Nazev'});
        } else {
            $pou = new Pou(
                $pui->{'Kod'},
                $pui->{'Nazev'}
            );

            $this->getEntityManager()->persist($pou);
        }
        $promise->then(function (ResponseInterface $res) use ($pui, $pou) {
            $pou->setCzsoCode($this->getCzsoCode((string) $pui->{'Kod'}, $res->getBody()->getContents()));
        });

        $pou->clearGeometry();
        foreach ($element->xpath('.//gml:Polygon[@gml:id]') as $polygon) {
            $geometry = GeometryEntityFactory::build($polygon, $this->getSymplifyBorderRation());
            $this->getEntityManager()->persist($geometry);
            $pou->addGeometry($geometry);
        }

        $promise->wait();
        $this->getEntityManager()->flush();
    }

    /**
     * @return array
     */
    public function getXmlNodeName(): array
    {
        return ['vf:Pou'];
    }

    private function getCzsoCode(string $kod, string $content)
    {
        $simplexml = simplexml_load_string($content);
        $search = $simplexml->xpath("//*[@akronym='KOD_RUIAN' and text()='$kod']/../..");
        if (count($search) === 1) {
            return (string) $search[0]->CHODNOTA;
        }

        return null;
    }
}
