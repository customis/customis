<?php
/**
 * Created by PhpStorm.
 * User: ja068850
 * Date: 6.12.2016
 * Time: 8:53
 */

namespace CustomIS\AppBundle\Doctrine;

use Doctrine\ORM\Mapping as ORM;

trait SerialIdTrait
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
