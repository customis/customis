<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 04.01.2016
 * Time: 14:40
 */

namespace CustomIS\LdapBundle\Security\Core\User;

use CustomIS\AppBundle\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Zend\Ldap\Dn;

/**
 * Class LdapUser
 *
 * @package CustomIS\LdapBundle\Security\Core\User
 */
class LdapUser implements AdvancedUserInterface, UserInterface
{
    private $username;
    private $password;
    private $enabled;
    private $accountNonExpired;
    private $credentialsNonExpired;
    private $accountNonLocked;
    private $roles;

    /**
     * @var Dn
     */
    private $dn;

    /**
     * LdapUser constructor.
     *
     * @param string $username
     * @param Dn     $dn
     * @param string $password
     * @param array  $roles
     * @param bool   $enabled
     * @param bool   $userNonExpired
     * @param bool   $credentialsNonExpired
     * @param bool   $userNonLocked
     */
    public function __construct(
        $username,
        Dn $dn,
        $password,
        array $roles = [],
        $enabled = true,
        $userNonExpired = true,
        $credentialsNonExpired = true,
        $userNonLocked = true
    ) {
        if ('' === $username || null === $username) {
            throw new \InvalidArgumentException('The username cannot be empty.');
        }

        if ('' === $dn || null === $dn) {
            throw new \InvalidArgumentException('The username cannot be empty.');
        }

        $this->username = $username;
        $this->password = $password;
        $this->enabled = $enabled;
        $this->accountNonExpired = $userNonExpired;
        $this->credentialsNonExpired = $credentialsNonExpired;
        $this->accountNonLocked = $userNonLocked;
        $this->roles = $roles;
        $this->dn = $dn;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getUsername();
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonExpired()
    {
        return $this->accountNonExpired;
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonLocked()
    {
        return $this->accountNonLocked;
    }

    /**
     * {@inheritdoc}
     */
    public function isCredentialsNonExpired()
    {
        return $this->credentialsNonExpired;
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
    }

    /**
     * @return Dn
     */
    public function getDn()
    {
        return $this->dn;
    }

    /**
     * @param string $role
     *
     * @return void
     */
    public function addRole($role)
    {
        $this->roles[] = $role;
    }

    /**
     * @param array $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }
}
