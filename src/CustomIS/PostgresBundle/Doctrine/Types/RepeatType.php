<?php

namespace CustomIS\PostgresBundle\Doctrine\Types;

use CustomIS\PostgresBundle\Repeat\Repeat;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class RepeatType
 *
 * @package CustomIS\PostgresBundle\Doctrine\Types
 * SQL declaration: CREATE TYPE repeat AS (times INTEGER, interval INTERVAL);
 */
class RepeatType extends Type
{
    const REPEAT = 'repeat';

    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param array                                     $fieldDeclaration The field declaration.
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform         The currently used database platform.
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return self::REPEAT;
    }

    /**
     * Gets the name of this type.
     *
     * @return string
     *
     * @todo Needed?
     */
    public function getName()
    {
        return self::REPEAT;
    }

    /**
     * @param Repeat           $value
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        $intervalTyoe = Type::getType('interval');

        return sprintf(
            "(%d, '%s')",
            $value->getTimes(),
            $intervalTyoe->convertToDatabaseValue($value->getInterval(), $platform)
        );
    }

    /**
     * @param string           $sqlExpr
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform)
    {
        return "CAST($sqlExpr AS repeat)";
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === '' || $value === null) {
            return null;
        }

        $intervalTyoe = Type::getType('interval');

        $value = trim($value, '()');
        $value = str_getcsv($value);

        return new Repeat(
            $value[0],
            $intervalTyoe->convertToPHPValue($value[1], $platform)
        );
    }
}
