<?php

namespace CustomIS\AppBundle\Controller;

use CustomIS\AppBundle\Entity\FileInfo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Class FileController
 *
 * @package CustomIS\AppBundle\Controller
 * @Route("/files")
 */
class FileController
{
    /**
     * @Route("/download/{fileinfo}")
     * @param FileInfo $fileinfo
     *
     * @return StreamedResponse
     */
    public function downloadAction(FileInfo $fileinfo)
    {
        $file = $fileinfo->getFile();
        $stream = $file->getFlysystemFile()->readStream();

        $response = new StreamedResponse(function () use ($stream) {
            rewind($stream);
            fpassthru($stream);
            fclose($stream);
        });

        $filenameAScii = (function ($filename) {
            $filename = iconv('UTF-8', 'ASCII//TRANSLIT', $filename);
            $filename = preg_replace('/[^a-zA-Z0-9.\-_()]/', ' ', $filename);
            $filename = preg_replace('/\s\s+/', ' ', $filename);

            return str_replace(' ', '_', trim($filename));
        })($fileinfo->getFileName());

        $response->headers->set('Content-Type', $file->getMimeType());
        $response->headers->set('Content-Length', $file->getFileSize());
        $response->headers->set('Content-Disposition', $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $filenameAScii
        ));
        $response->setPublic();

        return $response;
    }
}
