<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 11.08.2016
 * Time: 15:27
 */

namespace CustomIS\AppBundle\Form\Extension;


use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TimeTypeExtension extends AbstractTypeExtension
{

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $attr = $view->vars['attr'];
        $class = isset($attr['class']) ? $attr['class'] : '';
        $attr['class'] = trim($class . ' form_time');
        $view->vars['attr'] = $attr;

        $hour = $view['hour'];
        $minute = $view['minute'];

        $attr_hour = $hour->vars['attr'];
        $class_hour = isset($attr_hour['class']) ? $attr_hour['class'] : '';
        $attr_hour['class'] = trim($class_hour . ' hour');
        $hour->vars['attr'] = $attr_hour;

        $attr_minute = $minute->vars['attr'];
        $class_minute = isset($attr_minute['class']) ? $attr_minute['class'] : '';
        $attr_minute['class'] = trim($class_minute . ' minute');
        $minute->vars['attr'] = $attr_minute;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('widget', 'text');
    }


    public function getExtendedType()
    {
        return TimeType::class;
    }
}