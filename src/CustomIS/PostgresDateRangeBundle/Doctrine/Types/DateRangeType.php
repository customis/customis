<?php

namespace CustomIS\PostgresDateRangeBundle\Doctrine\Types;

use CustomIS\PostgresDateRangeBundle\Range\DateRange;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class DateRangeType
 *
 * @package CustomIS\PostgresDateRangeBundle\Doctrine\Types
 */
class DateRangeType extends Type
{
    const DATE_RANGE = 'daterange'; // modify to match your type name

    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'daterange';
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return DateRange|null
     * @throws \Exception
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        $matches = [];
        if (!preg_match('~^(?P<lower_bracket>[\[(])(?P<lower_bound>[0-9-]*?),\s*(?P<upper_bound>[0-9-]*?)(?P<upper_bracket>[\])])$~', $value, $matches)) {
            throw new \RuntimeException("Daterange value didn't match");
        }

        $lowerBound = null;
        if (!empty($matches['lower_bound'])) {
            $lowerBound = new \DateTime($matches['lower_bound']);
        }

        $upperBound = null;
        if (!empty($matches['upper_bound'])) {
            $upperBound = new \DateTime($matches['upper_bound']);
        }

        return new DateRange($lowerBound, $upperBound);
    }

    /**
     * @param DateRange        $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value !== null) {
            if (!empty($value->getEnd())) {
                $end = "'".$value->getEnd()->format('Y-m-d')."'";
            } else {
                $end = '';
            }

            return "['".$value->getStart()->format('Y-m-d')."',".$end.($value->isEndExcluded()
                    ? ')'
                    : ']');
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::DATE_RANGE;
    }

    /**
     * @return bool
     */
    public function canRequireSQLConversion()
    {
        return true;
    }

    /**
     * @param string           $sqlExpr
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToPHPValueSQL($sqlExpr, $platform)
    {
        return "
        CASE    WHEN $sqlExpr IS NOT NULL THEN '[' || COALESCE(lower_bound($sqlExpr)::varchar, '') || ',' || COALESCE(upper_bound($sqlExpr)::varchar,'') ||']'
                ELSE null
        END
        ";
    }

    /**
     * @param string           $sqlExpr
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform)
    {
        return $sqlExpr;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return array
     */
    public function getMappedDatabaseTypes(AbstractPlatform $platform)
    {
        return ['daterange'];
    }
}
