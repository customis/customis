<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Controller;

use CustomIS\RuianBundle\Entity\AbstractBaseRuianEntity;
use CustomIS\RuianBundle\Entity\Geometry;
use CustomIS\RuianBundle\Entity\Kraj1960;
use CustomIS\RuianBundle\Entity\Kraj1960Repository;
use CustomIS\RuianBundle\Entity\Momc;
use CustomIS\RuianBundle\Entity\MomcRepository;
use CustomIS\RuianBundle\Entity\Obec;
use CustomIS\RuianBundle\Entity\ObecRepository;
use CustomIS\RuianBundle\Entity\Okres;
use CustomIS\RuianBundle\Entity\OkresRepository;
use CustomIS\RuianBundle\Entity\Pou;
use CustomIS\RuianBundle\Entity\PouRepository;
use CustomIS\RuianBundle\Entity\Vusc;
use CustomIS\RuianBundle\Entity\VuscRepository;
use CustomIS\RuianBundle\Entity\ZsjRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ApiController
 *
 * @package CustomIS\RuianBundle\Controller
 * @Route("/ruian/api")
 */
class ApiController
{
    /**
     * @var PouRepository
     */
    private $pouRepository;

    /**
     * @var MomcRepository
     */
    private $momcRepository;

    /**
     * @var Kraj1960Repository
     */
    private $krajRepository;

    /**
     * @var ObecRepository
     */
    private $obecRepository;

    /**
     * @var OkresRepository
     */
    private $okresRepository;

    /**
     * @var VuscRepository
     */
    private $vuscRepository;

    /**
     * @var ZsjRepository
     */
    private $zsjRepository;

    /**
     * ApiController constructor.
     *
     * @param PouRepository      $pouRepository
     * @param MomcRepository     $momcRepository
     * @param Kraj1960Repository $krajRepository
     * @param ObecRepository     $obecRepository
     * @param OkresRepository    $okresRepository
     * @param VuscRepository     $vuscRepository
     * @param ZsjRepository      $zsjRepository
     */
    public function __construct(
        PouRepository $pouRepository,
        MomcRepository $momcRepository,
        Kraj1960Repository $krajRepository,
        ObecRepository $obecRepository,
        OkresRepository $okresRepository,
        VuscRepository $vuscRepository,
        ZsjRepository $zsjRepository
    ) {
        $this->pouRepository = $pouRepository;
        $this->momcRepository = $momcRepository;
        $this->krajRepository = $krajRepository;
        $this->obecRepository = $obecRepository;
        $this->okresRepository = $okresRepository;
        $this->vuscRepository = $vuscRepository;
        $this->zsjRepository = $zsjRepository;
    }

    /**
     * @Route("/kraje-1960", options={"expose"=true})
     * @return JsonResponse
     */
    public function kraje1960Action()
    {
        $kraje = [];
        /** @var Kraj1960 $row */
        foreach ($this->krajRepository->findAllWithGeometry() as $row) {
            if (count($row->getGeometry()) > 0) {
                $kraje[$row->getCode()]['name'] = $row->getName();
                $kraje[$row->getCode()]['polygon'] = array_map(function (Geometry $geometry) {
                    return [
                        'border' => $geometry->getBorder(),
                        'hole'   => $geometry->getInnerBorder(),
                    ];
                }, iterator_to_array($row->getGeometry()));
            }
        }

        return new JsonResponse($kraje);
    }

    /**
     * @Route("/kraje", options={"expose"=true})
     * @Route("/vusc", options={"expose"=true})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function krajeAction(Request $request)
    {
        $kraje = [];
        /** @var Vusc $row */
        foreach ($this->vuscRepository->findAllWithGeometry($request->query->get('name')) as $row) {
            if (count($row->getGeometry()) > 0) {
                $kraje[$row->getCode()]['name'] = $row->getName();
                if ($request->query->has('wkt')) {
                    $kraje[$row->getCode()]['wkt'] = 'MULTIPOLYGON ('.implode(', ', array_map(function (Geometry $value) {
                            return $value->toWktString();
                    }, iterator_to_array($row->getGeometry()))).')';
                } else {
                    $kraje[$row->getCode()]['polygon'] = array_map(function (Geometry $geometry) {
                        return [
                            'border' => $geometry->getBorder(),
                            'hole'   => $geometry->getInnerBorder(),
                        ];
                    }, iterator_to_array($row->getGeometry()));
                }
            }
        }

        return new JsonResponse($kraje);
    }

    /**
     * @Route("/obce", options={"expose"=true})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function obceAction(Request $request)
    {
        $obce = [];
        /** @var Obec $row */
        foreach ($this->obecRepository->findAllWithGeometry($request->query->get('name')) as $row) {
            if (count($row->getGeometry()) > 0) {
                $obce[$row->getCode()]['name'] = $row->getName();
                if ($request->query->has('wkt')) {
                    $obce[$row->getCode()]['wkt'] = 'MULTIPOLYGON ('.implode(', ', array_map(function (Geometry $value) {
                            return $value->toWktString();
                    }, iterator_to_array($row->getGeometry()))).')';
                } else {
                    $obce[$row->getCode()]['polygon'] = array_map(function (Geometry $geometry) {
                        return [
                            'border' => $geometry->getBorder(),
                            'hole'   => $geometry->getInnerBorder(),
                        ];
                    }, iterator_to_array($row->getGeometry()));
                }
            }
        }

        return new JsonResponse($obce);
    }

    /**
     * @Route("/okresy", options={"expose"=true})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function okresyAction(Request $request)
    {
        $okresy = [];
        /** @var Okres $row */
        foreach ($this->okresRepository->findAllWithGeometry($request->query->get('name')) as $row) {
            if (count($row->getGeometry()) > 0) {
                $okresy[$row->getCode()]['name'] = $row->getName();
                if ($request->query->has('wkt')) {
                    $okresy[$row->getCode()]['wkt'] = 'MULTIPOLYGON ('.implode(', ', array_map(function (Geometry $value) {
                            return $value->toWktString();
                    }, iterator_to_array($row->getGeometry()))).')';
                } else {
                    $okresy[$row->getCode()]['polygon'] = array_map(function (Geometry $geometry) {
                        return [
                            'border' => $geometry->getBorder(),
                            'hole'   => $geometry->getInnerBorder(),
                        ];
                    }, iterator_to_array($row->getGeometry()));
                }
            }
        }

        return new JsonResponse($okresy);
    }

    /**
     * @Route("/pou", options={"expose"=true})
     * @return JsonResponse
     */
    public function pouAction()
    {
        $pou = [];
        /** @var Pou $row */
        foreach ($this->pouRepository->findAllWithGeometry() as $row) {
            if (count($row->getGeometry()) > 0) {
                $pou[$row->getCode()]['name'] = $row->getName();
                $pou[$row->getCode()]['polygon'] = array_map(function (Geometry $geometry) {
                    return [
                        'border' => $geometry->getBorder(),
                        'hole'   => $geometry->getInnerBorder(),
                    ];
                }, iterator_to_array($row->getGeometry()));
            }
        }

        return new JsonResponse($pou);
    }

    /**
     * @Route("/momc", options={"expose"=true})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function momcAction(Request $request)
    {
        $momc = [];
        /** @var Momc $row */
        foreach ($this->momcRepository->findAllWithGeometry($request->query->get('name')) as $row) {
            if (count($row->getGeometry()) > 0) {
                $momc[$row->getCode()]['name'] = $row->getName();
                if ($request->query->has('wkt')) {
                    $momc[$row->getCode()]['wkt'] = 'MULTIPOLYGON ('.implode(', ', array_map(function (Geometry $value) {
                            return $value->toWktString();
                    }, iterator_to_array($row->getGeometry()))).')';
                } else {
                    $momc[$row->getCode()]['polygon'] = array_map(function (Geometry $geometry) {
                        return [
                            'border' => $geometry->getBorder(),
                            'hole'   => $geometry->getInnerBorder(),
                        ];
                    }, iterator_to_array($row->getGeometry()));
                }
            }
        }

        return new JsonResponse($momc);
    }

    /**
     * @Route("/zsj", options={"expose"=true})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function zsjAction(Request $request)
    {
        $zsj = [];
        /** @var Momc $row */
        foreach ($this->zsjRepository->findAllWithGeometry($request->query->get('name')) as $row) {
            if (count($row->getGeometry()) > 0) {
                $zsj[$row->getCode()]['name'] = $row->getName();
                if ($request->query->has('wkt')) {
                    $zsj[$row->getCode()]['wkt'] = 'MULTIPOLYGON ('.implode(', ', array_map(function (Geometry $value) {
                            return $value->toWktString();
                    }, iterator_to_array($row->getGeometry()))).')';
                } else {
                    $zsj[$row->getCode()]['polygon'] = array_map(function (Geometry $geometry) {
                        return [
                            'border' => $geometry->getBorder(),
                            'hole'   => $geometry->getInnerBorder(),
                        ];
                    }, iterator_to_array($row->getGeometry()));
                }
            }
        }

        return new JsonResponse($zsj);
    }

    /**
     * @param AbstractBaseRuianEntity $baseRuianEntity
     *
     * @Route("/geometry/{baseRuianEntity}", options={"expose"=true})
     *
     * @return JsonResponse
     */
    public function geometryAction(AbstractBaseRuianEntity $baseRuianEntity)
    {
        return new JsonResponse([
            'polygon' => array_map(function (Geometry $geometry) {
                return [
                    'border' => $geometry->getBorder(),
                    'hole'   => $geometry->getInnerBorder(),
                ];
            }, iterator_to_array($baseRuianEntity->getGeometry())),
        ]);
    }
}
