<?php
/**
 * Created by PhpStorm.
 * User: to068928
 * Date: 7.12.2016
 * Time: 15:07
 */

namespace CustomIS\PostgresBundle\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

/**
 * Class JsonbExistsAny
 *
 * JsonbExistsAny ::= JSONB_EA(LeftHandSide,{'RightHandSide1','RightHandSide2',...})
 *
 * Will be converted to: "jsonb_exists_any( LeftHandSide, array['RightHandSide1','RightHandSide2',...] )"
 */
class JsonbExistsAny extends FunctionNode
{
    public $leftHandSide = null;
    public $rightHandSide = [];

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->leftHandSide = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_COMMA);
        $parser->match(Lexer::T_OPEN_CURLY_BRACE);
        $this->rightHandSide[] = $parser->ScalarExpression();
        $lexer = $parser->getLexer();
        while ($lexer->isNextToken(Lexer::T_COMMA)) {
            $parser->match(Lexer::T_COMMA);

            $this->rightHandSide[] = $parser->ScalarExpression();
        }
        $parser->match(Lexer::T_CLOSE_CURLY_BRACE);
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return ' jsonb_exists_any('
            . $this->leftHandSide->dispatch($sqlWalker)
            . ',array['
                .implode(',',
                    array_map(
                        function ($rh) use ($sqlWalker) {
                            return $rh->dispatch($sqlWalker);
                        },
                        $this->rightHandSide
                    )
                )
            . '])';
    }
}