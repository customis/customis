<?php

namespace CustomIS\FormAnnotationBundle\Form\EventListener;

use \CustomIS\FormAnnotationBundle\Annotation\Form;
use \CustomIS\FormAnnotationBundle\Annotation\Resolver\AbstractResolver;
use \CustomIS\FormAnnotationBundle\Annotation\Resolver\HelpPopupResolver;
use \CustomIS\FormAnnotationBundle\Annotation\Resolver\HelpTextResolver;
use \CustomIS\FormAnnotationBundle\Annotation\Resolver\LabelResolver;
use \CustomIS\FormAnnotationBundle\Annotation\Resolver\OptionsResolver;
use \CustomIS\FormAnnotationBundle\Annotation\Resolver\OrderResolver;
use \CustomIS\FormAnnotationBundle\Annotation\Resolver\QueryBuilderResolver;
use CustomIS\FormAnnotationBundle\Annotation\Resolver\RequiredResolver;
use \CustomIS\FormAnnotationBundle\Annotation\Resolver\TypeResolver;
use \CustomIS\FormAnnotationBundle\Form\ElementBuilder;
use \Doctrine\Common\Annotations\Reader;
use \ReflectionObject;
use \Symfony\Component\DependencyInjection\ContainerInterface;
use \Symfony\Component\EventDispatcher\EventSubscriberInterface;
use \Symfony\Component\Form\FormEvent;
use \Symfony\Component\Form\FormEvents;

class EntityFormSubscriber implements EventSubscriberInterface
{
    /**
     * @var Reader
     */
    private $annotationReader;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * EntityFormSubscriber constructor.
     * @param Reader $annotationReader
     */
    public function __construct(Reader $annotationReader, ContainerInterface $container)
    {
        $this->annotationReader = $annotationReader;
        $this->container = $container;
    }


    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData'
        ];
    }

    public function preSetData(FormEvent $event)
    {
        $entity = $event->getData();
        if ($entity !== null)
        {
            $form = $event->getForm();

            $reflectionObject = new ReflectionObject($entity);
            foreach ($reflectionObject->getProperties() as $property)
            {
                $annotations = $this->annotationReader->getPropertyAnnotations($property);
                $formAnnotations = array_values(array_filter($annotations, function ($value, $key) use ($form, &$annotations) {
                    if ($value instanceof Form && $value->getValue() === $form->getName())
                    {
                        unset($annotations[$key]);
                        return true;
                    }

                    return false;
                }, ARRAY_FILTER_USE_BOTH));

                $annotations = array_values($annotations);
                if (count($formAnnotations) > 0)
                {
                    $elementBuilder = new ElementBuilder();

                    $resolvers = [
                        new OptionsResolver($elementBuilder),
                        new LabelResolver($elementBuilder),
                        new OrderResolver($elementBuilder),
                        new TypeResolver($elementBuilder),
                        new HelpTextResolver($elementBuilder),
                        new HelpPopupResolver($elementBuilder),
                        new RequiredResolver($elementBuilder),
                        new QueryBuilderResolver($elementBuilder, $this->container),
                    ];

                    $elementBuilder->setName($property->getName());

                    /** @var AbstractResolver $resolver */
                    foreach ($resolvers as $resolver)
                    {
                        $resolver->resolve($annotations, $form);
                    }

                    $form->add(
                        $elementBuilder->getElement()->getName(),
                        $elementBuilder->getElement()->getType(),
                        $elementBuilder->getElement()->getOptions()
                    );
                }
            }
        }
    }
}