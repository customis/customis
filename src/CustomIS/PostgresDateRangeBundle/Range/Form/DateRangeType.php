<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 24.7.14
 * Time: 11:09
 */

namespace CustomIS\PostgresDateRangeBundle\Range\Form;

use CustomIS\PostgresDateRangeBundle\Range\DateRange;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Valid;

/**
 * Class DateRangeType
 *
 * @package CustomIS\PostgresDateRangeBundle\Range\Form
 */
class DateRangeType extends AbstractType implements DataMapperInterface
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $startConstraints = [];
        if (!$options['infinite_start']) {
            $startConstraints[] = new NotBlank();
        }

        $endConstraints = [];
        if (!$options['infinite_end']) {
            $endConstraints[] = new NotBlank();
        }

        $builder
            ->add('start', DateType::class, array_merge([
                'attr'        => [
                    'data-type' => 'start',
                    'title'     => 'Od',
                ],
                'required'    => !$options['infinite_start'],
                'label'       => $options['start_title'],
                'constraints' => $startConstraints,
            ], $options['start_options']))
            ->add('end', DateType::class, array_merge([
                'attr'        => [
                    'data-type' => 'end',
                    'title'     => 'Do',
                ],
                'required'    => !$options['infinite_end'],
                'label'       => $options['end_title'],
                'constraints' => $endConstraints,
            ], $options['end_options']))
            ->setDataMapper($this);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'     => DateRange::class,
            'infinite_start' => false,
            'infinite_end'   => false,
            'start_title'    => 'Od',
            'end_title'      => 'Do',
            'attr'           => [
                'class' => 'customis-form-date-range',
            ],
            'empty_data'     => null,
            'constraints'    => new Valid(),
            'start_options'  => [],
            'end_options'    => [],
        ]);

        $resolver->setAllowedValues('infinite_start', [true, false]);
        $resolver->setAllowedValues('infinite_end', [true, false]);
    }

    /**
     * @param DateRange                               $data
     * @param \Symfony\Component\Form\FormInterface[] $forms
     */
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);
        if ($data !== null) {
            $forms['start']->setData($data->getStart() ?? $data->getStart());
            $forms['end']->setData($data->getEnd() ?? $data->getEnd());
        }
    }

    /**
     * @param \Symfony\Component\Form\FormInterface[] $forms
     * @param DateRange                               $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);
        if (!($forms['start']->getData() === null && $forms['end']->getData() === null)) {
            $data = new DateRange(
                $forms['start']->getData(),
                $forms['end']->getData()
            );
        }
    }
} 
