<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Entity\Zuj;

use CustomIS\RuianBundle\Entity\Zuj;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Momc
 *
 * @package CustomIS\RuianBundle\Entity\Zuj
 * @ORM\Entity()
 * @ORM\Table(name="zuj_momc", schema="ruian")
 */
class Momc extends Zuj
{
    /**
     * @var \CustomIS\RuianBundle\Entity\Momc
     * @ORM\OneToOne(targetEntity="CustomIS\RuianBundle\Entity\Momc", fetch="EAGER")
     */
    private $momc;

    public function __construct($code, \CustomIS\RuianBundle\Entity\Momc $momc)
    {
        parent::__construct($code);
        $this->momc = $momc;
    }

    /**
     * @return \CustomIS\RuianBundle\Entity\Momc
     */
    public function getMomc(): \CustomIS\RuianBundle\Entity\Momc
    {
        return $this->momc;
    }

    /**
     * @param \CustomIS\RuianBundle\Entity\Momc $momc
     */
    public function setMomc(\CustomIS\RuianBundle\Entity\Momc $momc)
    {
        $this->momc = $momc;
    }

    public function getGeometry()
    {
        return $this->getMomc()->getGeometry();
    }

    public function getName()
    {
        return $this->getMomc()->getName();
    }
}
