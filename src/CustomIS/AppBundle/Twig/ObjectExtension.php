<?php
/**
 * Created by PhpStorm.
 * User: ja068850
 * Date: 8.12.2016
 * Time: 10:23
 */

namespace CustomIS\AppBundle\Twig;

/**
 * Class ObjectExtension
 *
 * @package CustomIS\AppBundle\Twig
 */
class ObjectExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('cast_to_array', function ($object) {
                $reflection = new \ReflectionObject($object);
                foreach ($reflection->getProperties() as $property) {
                    $property->setAccessible(true);
                    $value = $property->getValue($object);
                    $property->setAccessible(false);
                    yield $property->getName() => $value;
                }
            }),
        ];
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('call_static', function ($class, $method, ...$params) {
                return $class::$method(...$params);
            }),
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'object_extension';
    }
}
