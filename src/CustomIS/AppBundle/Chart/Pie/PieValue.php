<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 02.03.2016
 * Time: 16:44
 */

namespace CustomIS\AppBundle\Chart\Pie;


class PieValue
{
    /**
     * @var string
     */
    private $label;

    /**
     * @var mixed
     */
    private $value;

    /**
     * PieValue constructor.
     * @param string $label
     * @param mixed $value
     */
    public function __construct($label, $value)
    {
        $this->label = $label;
        $this->value = $value;
    }

    public function toC3ChartColumnArray()
    {
        return [$this->label, $this->value];
    }
}