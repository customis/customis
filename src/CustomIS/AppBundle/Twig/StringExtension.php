<?php

namespace CustomIS\AppBundle\Twig;

/**
 * Class StringExtension
 *
 * @package CustomIS\AppBundle\Twig
 */
class StringExtension extends \Twig_Extension
{
    /**
     * @return \Twig_SimpleTest[]
     */
    public function getTests()
    {
        return [
            new \Twig_SimpleTest('string', function ($value) {
                return is_string($value);
            }),
        ];
    }
}
