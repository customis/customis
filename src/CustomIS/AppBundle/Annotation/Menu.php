<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 24.11.2015
 * Time: 15:55
 */

namespace CustomIS\AppBundle\Annotation;
use Doctrine\Common\Annotations\Annotation\Attribute;
use Doctrine\Common\Annotations\Annotation\Attributes;

/**
 * @Annotation
 * @Target({"METHOD"})
 * @Attributes({
 *   @Attribute("value", type = "string", required = true),
 *   @Attribute("label", type = "string", required = true),
 *   @Attribute("params", type = "array"),
 *   @Attribute("route", type = "string"),
 *   @Attribute("routeParams", type="array")
 * })
 */
class Menu
{
    /**
     * @var string
     */
    private $value;

    /**
     * @var string
     */
    private $label;

    /**
     * @var array
     */
    private $params = [];

    /**
     * @var string
     */
    private $route;

    /**
     * @var array
     */
    private $routeParams = [];

    /**
     * Menu constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->value = $data['value'];
        $this->label = $data['label'];

        if (isset($data['params']))
        {
            $this->params = $data['params'];
        }

        if (isset($data['route']))
        {
            $this->route = $data['route'];
        }

        if (isset($data['routeParams']))
        {
            $this->routeParams = $data['routeParams'];
        }
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @return array
     */
    public function getRouteParams()
    {
        return $this->routeParams;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }
}