<?php
/**
 * Created by PhpStorm.
 * User: T
 * Date: 24.2.2017
 * Time: 13:32
 */

namespace CustomIS\AppBundle\Annotation;

use Doctrine\Common\Annotations\Annotation;
use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class SessionRoute
 * @package CustomIS\AppBundle\Annotation
 *
 * @Annotation
 * @Target("METHOD")
 */
class SessionRoute
{
}