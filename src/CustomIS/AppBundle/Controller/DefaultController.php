<?php

namespace CustomIS\AppBundle\Controller;

use CustomIS\AppBundle\Form\LoginType;
use CustomIS\ControllerUtilsBundle\ActionParam\FormRequestInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Route("/", service="customis.appbundle.controller.default")
 */
class DefaultController
{
    /**
     * @var AuthenticationUtils
     */
    private $authenticationUtils;

    /**
     * DefaultController constructor.
     *
     * @param AuthenticationUtils $authenticationUtils
     */
    public function __construct(AuthenticationUtils $authenticationUtils)
    {
        $this->authenticationUtils = $authenticationUtils;
    }

    public function loginAction(FormRequestInterface $formRequest, FlashBagInterface $flash)
    {
        $error = $this->authenticationUtils->getLastAuthenticationError();
        $lastUsername = $this->authenticationUtils->getLastUsername();

        $formRequest->handle(LoginType::class, null, ['attr' => ['id' => 'login-form']]);
        $formRequest->getForm()->get('_username')->setData($lastUsername);

        if ($error instanceof AuthenticationException) {
            $flash->add('error', $error->getMessage());
        }

        return [
            'form'     => $formRequest->createFormView(),
            'fullPage' => true,
        ];
    }
}
