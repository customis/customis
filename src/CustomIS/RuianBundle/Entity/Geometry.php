<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Entity;

use CustomIS\AppBundle\Doctrine\UuidIdTrait;
use CustomIS\PostgresBundle\Geometry\Polygon;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Geometry
 *
 * @package CustomIS\RuianBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(schema="ruian")
 */
class Geometry
{
    use UuidIdTrait;

    /**
     * @var Polygon
     * @ORM\Column(type="polygon")
     */
    private $border;

    /**
     * @var Polygon|null
     * @ORM\Column(type="polygon", nullable=true)
     */
    private $innerBorder;

    /**
     * Geometry constructor.
     *
     * @param Polygon $border
     */
    public function __construct(Polygon $border)
    {
        $this->border = $border;
    }

    /**
     * @return Polygon
     */
    public function getBorder(): Polygon
    {
        return $this->border;
    }

    /**
     * @param Polygon $border
     */
    public function setBorder(Polygon $border)
    {
        $this->border = $border;
    }

    /**
     * @return Polygon|null
     */
    public function getInnerBorder()
    {
        return $this->innerBorder;
    }

    /**
     * @param Polygon|null $innerBorder
     */
    public function setInnerBorder($innerBorder)
    {
        $this->innerBorder = $innerBorder;
    }

    /**
     * @return string
     */
    public function toWktString()
    {
        $return = '('.implode(', ', array_map(function ($value) {
                return sprintf('%s %s', $value[0], $value[1]);
        }, $this->getBorder()->toArray(true))).')';

        if ($this->getInnerBorder() !== null) {
            $return .= ', '.'('.implode(', ', array_map(function ($value) {
                    return sprintf('%s %s', $value[0], $value[1]);
            }, $this->getBorder()->toArray(true))).')';
        }

        return '('.$return.')';
    }
}
