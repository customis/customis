<?php

declare(strict_types = 1);

namespace CustomIS\AppBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TextTypeExtension
 *
 * @package CustomIS\AppBundle\Form\Extension
 */
class TextTypeExtension extends AbstractTypeExtension
{
    /**
     * @param FormView      $view
     * @param FormInterface $form
     * @param array         $options
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $attr = $view->vars['attr'];
        $autocomplete = $options['autocomplete'];
        if ($autocomplete instanceof \Closure) {
            $autocomplete = $autocomplete();
        }

        if ($autocomplete !== null) {
            $attr['autocomplete'] = 'off';
            $attr['data-autocomplete'] = json_encode($autocomplete);
        }

        $view->vars['attr'] = $attr;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('autocomplete', null);
        $resolver->setAllowedTypes('autocomplete', ['null', 'array', \Closure::class]);
    }

    /**
     * @return mixed
     */
    public function getExtendedType()
    {
        return TextType::class;
    }
}
