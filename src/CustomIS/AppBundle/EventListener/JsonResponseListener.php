<?php

namespace CustomIS\AppBundle\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

/**
 * Class JsonResponseListener
 *
 * @package CustomIS\AppBundle\EventListener
 */
class JsonResponseListener
{
    /**
     * @param FilterResponseEvent $e
     */
    public function onKernelResponse(FilterResponseEvent $e)
    {
        $response = $e->getResponse();
        $request = $e->getRequest();

        if ($response instanceof JsonResponse) {
            $data = json_decode($response->getContent(), true);

            /** @var FlashBag $flashBag */
            $flashBag = $request->getSession()->getFlashBag();
            if (count($flashBag->peekAll()) > 0) {
                $data['@flash'] = $flashBag->all();
            }

            $e->setResponse(new JsonResponse($data));
        }
    }
}
