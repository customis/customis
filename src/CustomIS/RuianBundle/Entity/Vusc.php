<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Vusc
 *
 * @package CustomIS\RuianBundle\Entity
 * @ORM\Entity(repositoryClass="VuscRepository")
 * @ORM\Table(schema="ruian")
 */
class Vusc extends AbstractBaseRuianEntity
{
    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $czsoCode;

    /**
     * @var Okres[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="CustomIS\RuianBundle\Entity\Okres", mappedBy="vusc")
     */
    private $okresy;

    /**
     * @return string|null
     */
    public function getCzsoCode()
    {
        return $this->czsoCode;
    }

    /**
     * @param string|null $czsoCode
     */
    public function setCzsoCode($czsoCode)
    {
        $this->czsoCode = $czsoCode;
    }

    /**
     * @return array
     */
    public function getZsj(): array
    {
        return array_reduce(iterator_to_array($this->getOkresy()), function (array $carry, Okres $okres) {
            return array_merge($carry, $okres->getZsj());
        }, []);
    }

    /**
     * @return Okres[]|ArrayCollection
     */
    public function getOkresy()
    {
        return $this->okresy;
    }
}
