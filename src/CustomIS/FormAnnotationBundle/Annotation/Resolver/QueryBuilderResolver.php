<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 14.11.2015
 * Time: 11:03
 */

namespace CustomIS\FormAnnotationBundle\Annotation\Resolver;

use CustomIS\FormAnnotationBundle\Annotation\Annotation;
use CustomIS\FormAnnotationBundle\Annotation\Label;
use CustomIS\FormAnnotationBundle\Annotation\QueryBuilder;
use CustomIS\FormAnnotationBundle\Form\ElementBuilderInterface;
use CustomIS\FormAnnotationBundle\Form\QueryBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\FormInterface;

class QueryBuilderResolver extends AbstractResolver
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ElementBuilderInterface $elementBuilder, ContainerInterface $container)
    {
        parent::__construct($elementBuilder);
        $this->container = $container;
    }


    public function supports($annotation)
    {
        return $annotation instanceof QueryBuilder;
    }

    public function setData($value)
    {
        $query_builder = null;
        if ($this->container->has($value))
        {
            $query_builder = $this->container->get($value);
        }
        else
        {
            $query_builder = new $value();
        }

        if (!($query_builder instanceof QueryBuilderInterface))
        {
            throw new UnexpectedTypeException($query_builder, QueryBuilderInterface::class);
        }

        $this->getElementBuilder()->setQueryBuilder(function (EntityRepository $er) use ($query_builder) {
            return $query_builder->getQueryBuilder($er);
        });
    }
}