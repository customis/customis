<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 15.8.14
 * Time: 14:05
 */

namespace CustomIS\PostgresBundle\Doctrine\Types;


use CustomIS\PostgresBundle\Range\IntRange;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class IntRangeType extends Type
{
    const INT_RANGE = 'intrange'; // modify to match your type name

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'int4range';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $matches = [];
        if (!preg_match('~^(?P<lower_bracket>[\[(])(?P<lower_bound>[0-9]*?),(?P<upper_bound>[0-9]*?)(?P<upper_bracket>[\])])$~', $value, $matches))
        {
            throw new \Exception();
        }

        $lower_bound = null;
        if (!empty($matches['lower_bound']))
        {
            $lower_bound = $matches['lower_bound'];
        }

        $upper_bound = null;
        if (!empty($matches['upper_bound']))
        {
            $upper_bound = $matches['upper_bound'];
        }

        return new IntRange($lower_bound, $upper_bound);
    }

    /**
     * @param IntRange $value
     * @param AbstractPlatform $platform
     * @return mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!empty($value->getEnd()))
        {
            $end = $value->getEnd();
        }
        else
        {
            $end = '';
        }

        return "[" . $value->getStart() . "," . $end . "]";
    }

    public function getName()
    {
        return self::INT_RANGE;
    }

    public function canRequireSQLConversion()
    {
        return true;
    }

    public function convertToPHPValueSQL($sqlExpr, $platform)
    {
        return "'[' || COALESCE(lower(" . $sqlExpr . ")::varchar, '') || ',' || COALESCE(upper_bound(" . $sqlExpr . ")::varchar,'') ||']'";
    }

    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform)
    {
        return $sqlExpr;
    }

    public function getMappedDatabaseTypes(AbstractPlatform $platform)
    {
        return ['intrange', 'int4range'];
    }


} 