<?php

namespace CustomIS\PostgresDateRangeBundle\Range\Form;

use CustomIS\PostgresDateRangeBundle\Range\DateRange;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class TimestampRangeType
 *
 * @package CustomIS\PostgresDateRangeBundle\Range\Form
 */
class TimestampRangeType extends AbstractType implements DataMapperInterface
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $start_constraints = [];
        if (!$options['infinite_start']) {
            $start_constraints[] = new NotBlank();
        }

        $end_constraints = [];
        if (!$options['infinite_end']) {
            $end_constraints[] = new NotBlank();
        }

        $builder
            ->add('start', DateTimeType::class, [
                'attr'                     => [
                    'data-type' => 'start',
                    'title'     => 'Od',
                ],
                'required'                 => !$options['infinite_start'],
                'label'                    => $options['start_title'],
                'constraints'              => $start_constraints,
                'show_datepicker_on_focus' => $options['show_datepicker_on_focus'],
            ])
            ->add('end', DateTimeType::class, [
                'attr'                     => [
                    'data-type' => 'end',
                    'title'     => 'Do',
                ],
                'required'                 => !$options['infinite_end'],
                'label'                    => $options['end_title'],
                'constraints'              => $end_constraints,
                'show_datepicker_on_focus' => $options['show_datepicker_on_focus'],
            ])
            ->setDataMapper($this)
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'               => DateRange::class,
            'infinite_start'           => false,
            'infinite_end'             => false,
            'start_title'              => 'Od',
            'end_title'                => 'Do',
            'attr'                     => [
                'class' => 'customis-form-date-range',
            ],
            'empty_data'               => null,
            'show_datepicker_on_focus' => true,
            'constraints' => new Valid(),
        ]);

        $resolver->setAllowedValues('infinite_start', [true, false]);
        $resolver->setAllowedValues('infinite_end', [true, false]);
    }

    /**
     * @param DateRange                               $data
     * @param \Symfony\Component\Form\FormInterface[] $forms
     */
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);
        if ($data !== null) {
            $forms['start']->setData($data->getStart() ?? $data->getStart());
            $forms['end']->setData($data->getEnd() ?? $data->getEnd());
        }
    }

    /**
     * @param \Symfony\Component\Form\FormInterface[] $forms
     * @param DateRange                               $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);
        $data = new DateRange(
            $forms['start']->getData(),
            $forms['end']->getData()
        );
    }
}
