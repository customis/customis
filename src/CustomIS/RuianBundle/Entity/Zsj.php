<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Entity;

use CustomIS\RuianBundle\Geometry\GeometryTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Zsj
 *
 * @package CustomIS\RuianBundle\Entity
 * @ORM\Entity(repositoryClass="ZsjRepository")
 * @ORM\Table(schema="ruian")
 */
class Zsj extends AbstractBaseRuianEntity
{
    /**
     * @var Okres
     * @ORM\ManyToOne(targetEntity="CustomIS\RuianBundle\Entity\Obec", inversedBy="zsj")
     * @ORM\JoinColumn(nullable=false)
     */
    private $obec;

    /**
     * @var Momc[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="CustomIS\RuianBundle\Entity\Momc", mappedBy="zsj")
     */
    private $momc;

    /**
     * Pou constructor.
     *
     * @param Okres  $okres
     * @param Pou    $pou
     * @param int    $code
     * @param string $name
     */
    public function __construct(Obec $obec, $code, $name)
    {
        $this->obec = $obec;
        $this->momc = new ArrayCollection();
        parent::__construct($code, $name);
    }

    /**
     * @return Obec
     */
    public function getObec(): Obec
    {
        return $this->obec;
    }

    /**
     * @param Obec $obec
     */
    public function setObec(Obec $obec)
    {
        $this->obec = $obec;
    }
}
