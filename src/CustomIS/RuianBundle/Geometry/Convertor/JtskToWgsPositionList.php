<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Geometry\Convertor;

use CustomIS\PostgresBundle\Geometry\Point;

/**
 * Class JtskToWgsPositionList
 *
 * @package CustomIS\RuianBundle\Geometry\Convertor
 */
class JtskToWgsPositionList
{
    /**
     * @param string $positionList
     * @param int    $height
     *
     * @return \Generator|Point[]
     */
    public static function convert(string $positionList, $height = 200)
    {
        $temp = explode(' ', trim($positionList));
        $coordinates = [];

        $count = count($temp);
        for ($i = 0, $j = 0; $i < $count; $i += 2, $j++) {
            $coordinates[$j] = [
                $temp[$i],
                $temp[$i + 1],
            ];
        }

        foreach ($coordinates as $coordinate) {
            $wgsCoordinatesArray = self::jtsk_to_wgs($coordinate[0], $coordinate[1], $height);
            yield new Point($wgsCoordinatesArray['lat'], $wgsCoordinatesArray['lon']);
        }
    }

    private static function jtsk_to_wgs($X, $Y, $H = 200)
    {
        if ($X < 0 && $Y < 0) {
            $X = -$X;
            $Y = -$Y;
        }

        if ($Y > $X) {
            $q = $X;
            $X = $Y;
            $Y = $q;
        }

        unset($q);

        $coord = [
            'wgs84_latitude'  => "",
            'wgs84_longitude' => "",
            'lat'             => 0,
            'lon'             => 0,
            'vyska'           => 0,
        ];

        /* P�epo�et vstup�ch �daj� - vychazi z nejakeho skriptu, ktery jsem nasel na Internetu - nejsem autorem prepoctu. */

        /*Vypocet zemepisnych souradnic z rovinnych souradnic*/
        $a = 6377397.15508;
        $e = 0.081696831215303;
        $n = 0.97992470462083;
        $konst_u_ro = 12310230.12797036;
        $sinUQ = 0.863499969506341;
        $cosUQ = 0.504348889819882;
        $sinVQ = 0.420215144586493;
        $cosVQ = 0.907424504992097;
        $alfa = 1.000597498371542;
        $k = 1.003419163966575;

        $ro = sqrt($X * $X + $Y * $Y);
        $epsilon = 2 * atan($Y / ($ro + $X));
        $D = $epsilon / $n;
        $S = 2 * atan(exp(1 / $n * log($konst_u_ro / $ro))) - M_PI / 2;
        $sinS = sin($S);
        $cosS = cos($S);
        $sinU = $sinUQ * $sinS - $cosUQ * $cosS * cos($D);
        $cosU = sqrt(1 - $sinU * $sinU);
        $sinDV = sin($D) * $cosS / $cosU;
        $cosDV = sqrt(1 - $sinDV * $sinDV);
        $sinV = $sinVQ * $cosDV - $cosVQ * $sinDV;
        $cosV = $cosVQ * $cosDV + $sinVQ * $sinDV;
        $Ljtsk = 2 * atan($sinV / (1 + $cosV)) / $alfa;
        $t = exp(2 / $alfa * log((1 + $sinU) / $cosU / $k));
        $pom = ($t - 1) / ($t + 1);

        do {
            $sinB = $pom;
            $pom = $t * exp($e * log((1 + $e * $sinB) / (1 - $e * $sinB)));
            $pom = ($pom - 1) / ($pom + 1);
        } while (abs($pom - $sinB) > 1e-15);

        $Bjtsk = atan($pom / sqrt(1 - $pom * $pom));

        /* Pravo�hl� sou�adnice ve S-JTSK */
        $a = 6377397.15508;
        $f_1 = 299.152812853;
        $e2 = 1 - (1 - 1 / $f_1) * (1 - 1 / $f_1);
        $ro = $a / sqrt(1 - $e2 * sin($Bjtsk) * sin($Bjtsk));
        $x = ($ro + $H) * cos($Bjtsk) * cos($Ljtsk);
        $y = ($ro + $H) * cos($Bjtsk) * sin($Ljtsk);
        $z = ((1 - $e2) * $ro + $H) * sin($Bjtsk);

        /* Pravo�hl� sou�adnice v WGS-84*/
        $dx = 570.69;
        $dy = 85.69;
        $dz = 462.84;
        $wz = -5.2611 / 3600 * M_PI / 180;
        $wy = -1.58676 / 3600 * M_PI / 180;
        $wx = -4.99821 / 3600 * M_PI / 180;
        $m = 3.543e-6;
        $xn = $dx + (1 + $m) * ($x + $wz * $y - $wy * $z);
        $yn = $dy + (1 + $m) * (-$wz * $x + $y + $wx * $z);
        $zn = $dz + (1 + $m) * ($wy * $x - $wx * $y + $z);

        /* Geodetick� sou�adnice v syst�mu WGS-84*/
        $a = 6378137.0;
        $f_1 = 298.257223563;
        $a_b = $f_1 / ($f_1 - 1);
        $p = sqrt($xn * $xn + $yn * $yn);
        $e2 = 1 - (1 - 1 / $f_1) * (1 - 1 / $f_1);
        $theta = atan($zn * $a_b / $p);
        $st = sin($theta);
        $ct = cos($theta);
        $t = ($zn + $e2 * $a_b * $a * $st * $st * $st) / ($p - $e2 * $a * $ct * $ct * $ct);
        $B = atan($t);
        $L = 2 * atan($yn / ($p + $xn));
        $H = sqrt(1 + $t * $t) * ($p - $a / sqrt(1 + (1 - $e2) * $t * $t));

        /* Form�t v�stupn�ch hodnot */

        $B = $B / M_PI * 180;

        $coord['lat'] = $B;
        $latitude = "N";
        if ($B < 0) {
            $B = -$B;
            $latitude = "S";
        };

        $st_sirky = floor($B);
        $B = ($B - $st_sirky) * 60;
        $min_sirky = floor($B);
        $B = ($B - $min_sirky) * 60;
        $vt_sirky = round($B * 1000) / 1000;
        $latitude = $st_sirky."�".$min_sirky."'".$vt_sirky.$latitude;
        $coord['wgs84_latitude'] = $latitude;

        $L = $L / M_PI * 180;
        $coord['lon'] = $L;
        $longitude = "E";
        if ($L < 0) {
            $L = -$L;
            $longitude = "W";
        };

        $st_delky = floor($L);
        $L = ($L - $st_delky) * 60;
        $min_delky = floor($L);
        $L = ($L - $min_delky) * 60;
        $vt_delky = round($L * 1000) / 1000;
        $longitude = $st_delky."�".$min_delky."'".$vt_delky.$longitude;
        $coord['wgs84_longitude'] = $longitude;

        $coord['vyska'] = round(($H) * 100) / 100;

        return $coord;
    }
}
