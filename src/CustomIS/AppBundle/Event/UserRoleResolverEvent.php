<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 08.06.2016
 * Time: 9:25
 */

namespace CustomIS\AppBundle\Event;

use CustomIS\AppBundle\Entity\Uzivatel;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserRoleResolverEvent
 *
 * @package CustomIS\AppBundle\Event
 */
class UserRoleResolverEvent extends Event
{
    const NAME = 'customis.user_role_dispatcher';

    /**
     * @var Uzivatel
     */
    private $uzivatel;
    /**
     * @var array
     */
    private $roles;

    /**
     * @var UserInterface
     */
    private $userToken;

    /**
     * UserRoleResolverEvent constructor.
     *
     * @param Uzivatel      $uzivatel
     * @param UserInterface $userInterfaceToken
     */
    public function __construct(Uzivatel $uzivatel, UserInterface $userInterfaceToken)
    {
        $this->uzivatel = $uzivatel;
        $this->userToken = $userInterfaceToken;
        $this->roles = $userInterfaceToken->getRoles();
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param string $role
     */
    public function addRole($role)
    {
        if (!in_array($role, $this->roles)) {
            $this->roles[] = $role;
        }
    }

    /**
     * @return Uzivatel
     */
    public function getUzivatel()
    {
        return $this->uzivatel;
    }

    /**
     * @return UserInterface
     */
    public function getUserToken()
    {
        return $this->userToken;
    }
}
