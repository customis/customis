<?php
/**
 * Created by PhpStorm.
 * User: to068928
 * Date: 1.12.2016
 * Time: 13:21
 */

namespace CustomIS\WorkflowBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CustomIS\AppBundle\Doctrine\UuidIdTrait;

/**
 * WorkflowPlace
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class WorkflowPlace
{
    use UuidIdTrait;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var Workflow
     * @ORM\ManyToOne(targetEntity="Workflow", inversedBy="places")
     * @ORM\JoinColumn(nullable=false)
     */
    private $workflow;

    /**
     * @ORM\ManyToMany(targetEntity="WorkflowTransition", mappedBy="froms")
     */
    private $transitions_from;

    /**
     * @ORM\ManyToMany(targetEntity="WorkflowTransition", mappedBy="tos")
     */
    private $transitions_to;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return WorkflowPlace
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return WorkflowPlace
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set workflow
     *
     * @param \CustomIS\WorkflowBundle\Entity\Workflow $workflow
     *
     * @return WorkflowPlace
     */
    public function setWorkflow(\CustomIS\WorkflowBundle\Entity\Workflow $workflow)
    {
        $this->workflow = $workflow;

        return $this;
    }

    /**
     * Get workflow
     *
     * @return \CustomIS\WorkflowBundle\Entity\Workflow
     */
    public function getWorkflow()
    {
        return $this->workflow;
    }
    /**
     * Constructor
     */
    public function __construct(Workflow $workflow, $name, $title, $type = null)
    {
        $this->workflow = $workflow;
        $this->name = $name;
        $this->title = $title;
        $this->transitions_from = new \Doctrine\Common\Collections\ArrayCollection();
        $this->transitions_to = new \Doctrine\Common\Collections\ArrayCollection();
        $this->type = $type;
    }

    /**
     * Add transitionsFrom
     *
     * @param \CustomIS\WorkflowBundle\Entity\WorkflowTransition $transitionsFrom
     *
     * @return WorkflowPlace
     */
    public function addTransitionsFrom(\CustomIS\WorkflowBundle\Entity\WorkflowTransition $transitionsFrom)
    {
        $this->transitions_from[] = $transitionsFrom;

        return $this;
    }

    /**
     * Remove transitionsFrom
     *
     * @param \CustomIS\WorkflowBundle\Entity\WorkflowTransition $transitionsFrom
     */
    public function removeTransitionsFrom(\CustomIS\WorkflowBundle\Entity\WorkflowTransition $transitionsFrom)
    {
        $this->transitions_from->removeElement($transitionsFrom);
    }

    /**
     * Get transitionsFrom
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransitionsFrom()
    {
        return $this->transitions_from;
    }

    /**
     * Add transitionsTo
     *
     * @param \CustomIS\WorkflowBundle\Entity\WorkflowTransition $transitionsTo
     *
     * @return WorkflowPlace
     */
    public function addTransitionsTo(\CustomIS\WorkflowBundle\Entity\WorkflowTransition $transitionsTo)
    {
        $this->transitions_to[] = $transitionsTo;

        return $this;
    }

    /**
     * Remove transitionsTo
     *
     * @param \CustomIS\WorkflowBundle\Entity\WorkflowTransition $transitionsTo
     */
    public function removeTransitionsTo(\CustomIS\WorkflowBundle\Entity\WorkflowTransition $transitionsTo)
    {
        $this->transitions_to->removeElement($transitionsTo);
    }

    /**
     * Get transitionsTo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransitionsTo()
    {
        return $this->transitions_to;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return WorkflowPlace
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
