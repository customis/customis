<?php

namespace CustomIS\FormAnnotationBundle\Annotation;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 * @Attributes({
 *   @Attribute("value", type = "string", required = true),
 *   @Attribute("form", type = "string", required = false)
 * })
 */
class Type extends Annotation
{
    private $value;

    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->value = $data['value'];
    }

    public function getValue()
    {
        return $this->value;
    }
}