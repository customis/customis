<?php

namespace CustomIS\AppBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Extension\GlobalsInterface;

/**
 * Class GlobalVarsExtension
 *
 * @package CustomIS\AppBundle\Twig
 */
class GlobalVarsExtension extends \Twig_Extension implements GlobalsInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * GlobalVarsExtension constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getGlobals()
    {
        return [
            'flashMessagesMap' => $this->container->getParameter('flashMessagesMap'),
        ];
    }
}
