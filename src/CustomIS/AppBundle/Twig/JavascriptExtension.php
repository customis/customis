<?php
/**
 * Created by PhpStorm.
 * User: ja068850
 * Date: 8.12.2016
 * Time: 10:23
 */

namespace CustomIS\AppBundle\Twig;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class JavascriptExtension
 *
 * @package CustomIS\AppBundle\Twig
 */
class JavascriptExtension extends \Twig_Extension
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var string
     */
    private $rootDir;

    /**
     * JavascriptExtension constructor.
     *
     * @param RequestStack $requestStack
     * @param string       $rootDir
     */
    public function __construct(RequestStack $requestStack, string $rootDir)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->rootDir = $rootDir;
    }

    /**
     * @return array
     * <script defer src="{{ asset('assets/' ~ app.request.attributes.get('_controller')|replace({'\\': '/', '::': '/'}) ~ '.js') }}"></script>
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('javascript_module', function ($basePath, $module = false) {
                $controller = $this->request->attributes->get('_controller');
                $path = $basePath.str_replace(['\\', '::'], '/', $controller).'.js';
                if (!file_exists($this->rootDir.'/../web'.$path)) {
                    $path = $basePath.'global.js';
                }

                if ($module === true) {
                    return str_replace(['\\', '::'], '/', $controller);
                }

                return sprintf('<script defer src="%s"></script>', $path);
            }, ['is_safe' => ['html']]),
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'javascript_extension';
    }
}
