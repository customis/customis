<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Loader;

use CustomIS\RuianBundle\Entity\Momc;
use CustomIS\RuianBundle\Entity\Obec;
use CustomIS\RuianBundle\Geometry\GeometryEntityFactory;

/**
 * Class MomcLoader
 *
 * @package CustomIS\RuianBundle\Loader
 */
class MomcLoader extends AbstractRuianLoader
{
    /**
     * @param \SimpleXMLElement $element
     * @param string $xmlFile
     */
    public function load(\SimpleXMLElement $element, string $xmlFile)
    {
        $ns = $element->getNamespaces(true);
        $mci = $element->children($ns['mci']);

        /** @var Obec $obec */
        if (($obec = $this->getEntityManager()->getRepository(Obec::class)->findOneByCode($mci->{'Obec'}->children($ns['obi'])->{'Kod'}))
            !== null
        ) {
            if (($momc = $this->getEntityManager()->getRepository(Momc::class)->findOneByCode($mci->{'Kod'})) !== null) {
                /** @var Momc $momc */
                $momc->setName((string) $mci->{'Nazev'});
                $momc->setObec($obec);
            } else {
                $momc = new Momc(
                    $obec,
                    (int) $mci->{'Kod'},
                    $mci->{'Nazev'}
                );

                $this->getEntityManager()->persist($momc);
            }

            $momc->clearGeometry();
            foreach ($element->xpath('.//gml:Polygon[@gml:id]') as $polygon) {
                $geometry = GeometryEntityFactory::build($polygon, $this->getSymplifyBorderRation());
                $this->getEntityManager()->persist($geometry);
                $momc->addGeometry($geometry);
            }
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return array
     */
    public function getXmlNodeName(): array
    {
        return ['vf:Momc'];
    }
}
