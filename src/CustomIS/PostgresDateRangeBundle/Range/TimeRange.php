<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 15.7.14
 * Time: 9:03
 */

namespace CustomIS\PostgresDateRangeBundle\Range;


use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class TimeRange extends DateRange
{
    function __toString()
    {
        return '['
                . ($this->getStart() === null ? '' : $this->getStart()->format('H:i:s'))
                .', '
                . ($this->getEnd() === null ? '' : $this->getEnd()->format('H:i:s'))
                . ']';
    }

    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->getStart() !== null && $this->getEnd() !== null && $this->getStart() > $this->getEnd())
        {
            $context->buildViolation('Čas od nesmí být větší než čas do')
                ->atPath('start')
                ->addViolation();
        }
    }

} 