<?php

namespace CustomIS\AppBundle\EventListener;

use CustomIS\AppBundle\Entity\Uzivatel;
use CustomIS\AppBundle\Entity\UzivatelRepository;
use CustomIS\AppBundle\Entity\UzivatelRepositoryInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Class SuccessLoginListener
 *
 * @package CustomIS\AppBundle\EventListener
 */
class SuccessLoginListener
{
    /**
     * @var UzivatelRepository
     */
    private $userRepository;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * SuccessLoginListener constructor.
     *
     * @param UzivatelRepositoryInterface  $userRepository
     * @param EntityManager                $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(
        UzivatelRepositoryInterface $userRepository,
        EntityManager $entityManager,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param InteractiveLoginEvent $e
     */
    public function onSuccesslogin(InteractiveLoginEvent $e)
    {
        $authToken = $e->getAuthenticationToken();
        $login = $authToken->getUsername();

        if (!($user = $this->userRepository->findByUser($login))) {
            /** @var ClassMetadata $metadata */
            foreach ($this->entityManager->getMetadataFactory()
                                         ->getAllMetadata() as $metadata) {
                if ($metadata->customRepositoryClassName === get_class($this->userRepository)) {
                    $class = $metadata->getName();

                    /** @var Uzivatel $user */
                    $user = new $class();
                    $user->setLogin($login);
                    if (property_exists($class, 'password')) {
                        $password = $this->passwordEncoder->encodePassword($user, $authToken->getUser()->getPassword());
                        $user->setPassword($password);
                    }
                    $this->entityManager->persist($user);
                    $this->entityManager->flush();
                    break;
                }
            }
        }
    }
}
