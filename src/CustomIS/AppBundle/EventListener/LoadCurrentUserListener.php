<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 10.01.2016
 * Time: 13:33
 */
namespace CustomIS\AppBundle\EventListener;

use CustomIS\AppBundle\Entity\UzivatelRepository;
use CustomIS\AppBundle\Entity\UzivatelRepositoryInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class LoadCurrentUserListener
 *
 * @package CustomIS\AppBundle\EventListener
 */
class LoadCurrentUserListener
{
    /**
     * @var UzivatelRepository
     */
    private $uzivatelRepository;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var \Twig_Environment
     */
    private $environment;

    /**
     * LoadCurrentUserListener constructor.
     *
     * @param UzivatelRepositoryInterface $uzivatelRepository
     * @param TokenStorageInterface       $tokenStorage
     * @param \Twig_Environment           $environment
     */
    public function __construct(
        UzivatelRepositoryInterface $uzivatelRepository,
        TokenStorageInterface $tokenStorage,
        \Twig_Environment $environment
    )
    {
        $this->uzivatelRepository = $uzivatelRepository;
        $this->tokenStorage = $tokenStorage;
        $this->environment = $environment;
    }

    /**
     * @param GetResponseEvent $e
     */
    public function onKernelRequest(GetResponseEvent $e)
    {
        static $tempUzivatel;
        $token = $this->tokenStorage->getToken();
        if ($token !== null) {
            if ($token->getUser() instanceof UserInterface) {
                if ($tempUzivatel === null) {
                    $tempUzivatel = $this->uzivatelRepository->findByUser($token->getUser());
                }
            }
        }

        $e->getRequest()->attributes->set('_current_user', $tempUzivatel);
        $this->environment->addGlobal('currentUser', $tempUzivatel);
    }
}