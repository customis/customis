<?php

declare(strict_types = 1);

namespace CustomIS\AppBundle\Graph\Plot;

use CustomIS\AppBundle\Graph\Value;

/**
 * Class AbstractPlot
 *
 * @package CustomIS\AppBundle\Graph\Plot
 */
abstract class AbstractPlot implements \JsonSerializable
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $values = [];

    /**
     * @var callable
     */
    private $labelCallback;

    /**
     * AbstractPlot constructor.
     *
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
        $this->labelCallback = function (Value $value) {
            return $value->getValue();
        };
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param mixed       $value
     * @param string|null $x
     *
     * @return self
     */
    public function addValue($value, $x = null)
    {
        if ($x !== null) {
            $this->values[$x] = $value;
        } else {
            $this->values[] = $value;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $return = [
            'x'         => array_keys($this->getValues()),
            'y'         => array_values($this->getValues()),
            'type'      => $this->getType(),
            'name'      => $this->getName(),
            'hoverinfo' => 'text',
            'text'      => array_map($this->labelCallback, array_map(function ($value) {
                return $value instanceof Value
                    ? $value
                    : new Value($value);
            }, array_values($this->getValues()))),
        ];

        return $return;
    }

    /**
     * @return string
     */
    abstract public function getType(): string;

    /**
     * @param callable $labelCallback
     */
    public function setLabelCallback(callable $labelCallback)
    {
        $this->labelCallback = $labelCallback;
    }
}
