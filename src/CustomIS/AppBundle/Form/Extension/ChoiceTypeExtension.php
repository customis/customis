<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 07.01.2016
 * Time: 12:57
 */

namespace CustomIS\AppBundle\Form\Extension;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChoiceTypeExtension extends AbstractTypeExtension
{
    /**
     * @var string
     */
    private $placeholder_text;


    /**
     * EntityTypeExtension constructor.
     */
    public function __construct($placeholder_text)
    {
        $this->placeholder_text = $placeholder_text;
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if ($options['placeholder'] !== null)
        {
            $attr = $view->vars['attr'];
            $attr['data-placeholder'] = $options['placeholder'];
            $view->vars['attr'] = $attr;
            if ($options['multiple'] === true)
            {
                $view->vars['placeholder'] = null;
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('placeholder', $this->placeholder_text);
        $resolver->setNormalizer('placeholder', function (Options $options, $placeholder) {
            if ($options['required'] && ($options['expanded'] || isset($options['attr']['size']) && $options['attr']['size'] > 1)) {
                // placeholder for required radio buttons or a select with size > 1 does not make sense
                return;
            } elseif (false === $placeholder) {
                // an empty value should be added but the user decided otherwise
                return;
            } elseif ($options['expanded'] && '' === $placeholder) {
                // never use an empty label for radio buttons
                return 'None';
            }

            // empty value has been set explicitly
            return $placeholder;
        });
    }


    public function getExtendedType()
    {
        return ChoiceType::class;
    }

}