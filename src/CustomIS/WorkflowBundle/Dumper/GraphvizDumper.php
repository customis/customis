<?php

/*
 * Based on the original Dumper from Symfony Framework.
 *
 */

namespace CustomIS\WorkflowBundle\Dumper;

use CustomIS\WorkflowBundle\Entity\WorkflowPlace;
use CustomIS\WorkflowBundle\Entity\WorkflowTransition;
use Doctrine\DBAL\Exception\DatabaseObjectNotFoundException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Workflow\Definition;
use Symfony\Component\Workflow\Dumper\DumperInterface;
use Symfony\Component\Workflow\Marking;
use Symfony\Component\Workflow\Workflow;

class GraphvizDumper
{
    private static $defaultOptions = array(
        'graph' => array('ratio' => 'compress', 'rankdir' => 'TB'),
        'node' => array('fontsize' => 9, 'fontname' => 'Arial', 'color' => '#333333','style' => 'filled',
                        'fillcolor' => '#CDD5D8', 'fixedsize' => false, 'width' => 2, 'height' => 0.5),
        'edge' => array('fontsize' => 9, 'fontname' => 'Arial', 'color' => '#333333', 'arrowhead' => 'normal', 'arrowsize' => 1),
    );

    private $entityManager;

    private $workflowRepository;

    private $workflowPlaceRepository;

    private $workflowTransitionRepository;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->workflowRepository = $this->entityManager->getRepository(\CustomIS\WorkflowBundle\Entity\Workflow::class);
        $this->workflowPlaceRepository = $this->entityManager->getRepository(WorkflowPlace::class);
        $this->workflowTransitionRepository = $this->entityManager->getRepository(WorkflowTransition::class);
    }

    /**
     * {@inheritdoc}
     *
     * Dumps the workflow as a graphviz graph.
     *
     * Available options:
     *
     *  * graph: The default options for the whole graph
     *  * node: The default options for nodes (places + transitions)
     *  * edge: The default options for edges
     */
    public function dump(Workflow $workflow, Marking $marking = null, array $options = array())
    {
        $definition = $workflow->getDefinition();
        $places = $this->findPlaces($definition, $marking);
        $transitions = $this->findTransitions($definition);
        $edges = $this->findEdges($definition);

        $options = array_replace_recursive(self::$defaultOptions, $options);
        if(null === ($workflowEntity = $this->workflowRepository->findOneBy(['name' => $workflow->getName()]))){
            throw new DatabaseObjectNotFoundException('Workflow entity "' . $workflow->getName() . '" not found');
        }

        return $this->startDot($options)
            .$this->addPlaces($places, $workflowEntity)
            .$this->addTransitions($transitions, $workflowEntity)
            .$this->addEdges($edges)
            .$this->endDot();
    }

    private function findPlaces(Definition $definition, Marking $marking = null)
    {
        $places = array();

        foreach ($definition->getPlaces() as $place) {
            $attributes = array();
            if ($place === $definition->getInitialPlace()) {
                $attributes['fillcolor'] = '#F3F3F3';
            }
            if ($marking && $marking->has($place)) {
                $attributes['color'] = '#FF0000';
                $attributes['shape'] = 'doublecircle';
            }
            if (strpos($place, 'cancelled') !== false) {
                $attributes['fillcolor'] = '#E6A4A4';
                $attributes['style'] = 'filled';
            }
            $places[$place] = array(
                'attributes' => $attributes,
            );
        }

        return $places;
    }

    private function findTransitions(Definition $definition)
    {
        $transitions = array();

        foreach ($definition->getTransitions() as $transition) {
            $transitions[] = array(
                'attributes' => array('shape' => 'rect', 'regular' => false, 'fillcolor' => '#FBFAC4'),
                'name' => $transition->getName(),
            );
        }

        return $transitions;
    }

    private function addPlaces(array $places, $workflowEntity)
    {
        $code = '';

        foreach ($places as $id => $place) {
            $placeEntity = $this->workflowPlaceRepository->findOneBy(['name' => $id, 'workflow' => $workflowEntity]);
            $code .= sprintf("  place_%s [label=\"%s\", shape=circle%s];\n",
                $this->dotize($id),
                !empty($placeEntity->getTitle()) ? $placeEntity->getTitle() : str_replace('_', ' ', $id),
                $this->addAttributes($place['attributes'])
            );
        }

        return $code;
    }

    private function addTransitions(array $transitions, $workflowEntity)
    {
        $code = '';

        foreach ($transitions as $place) {
            $transitionEntity = $this->workflowTransitionRepository->findOneBy(['name' => $place['name'], 'workflow' => $workflowEntity]);
            $code .= sprintf("  transition_%s [label=\"%s\"%s];\n",
                $this->dotize($place['name']),
                !empty($transitionEntity->getTitle()) ? $transitionEntity->getTitle() : str_replace('_', ' ', $place['name']),
                $this->addAttributes($place['attributes'])
            );
        }

        return $code;
    }

    private function findEdges(Definition $definition)
    {
        $dotEdges = array();

        foreach ($definition->getTransitions() as $transition) {
            foreach ($transition->getFroms() as $from) {
                $dotEdges[] = array(
                    'from' => $from,
                    'to' => $transition->getName(),
                    'direction' => 'from',
                );
            }
            foreach ($transition->getTos() as $to) {
                $dotEdges[] = array(
                    'from' => $transition->getName(),
                    'to' => $to,
                    'direction' => 'to',
                );
            }
        }

        return $dotEdges;
    }

    private function addEdges($edges)
    {
        $code = '';

        foreach ($edges as $edge) {
            $code .= sprintf("  %s_%s -> %s_%s [style=\"solid\"];\n",
                'from' === $edge['direction'] ? 'place' : 'transition',
                $this->dotize($edge['from']),
                'from' === $edge['direction'] ? 'transition' : 'place',
                $this->dotize($edge['to'])
            );
        }

        return $code;
    }

    private function startDot(array $options)
    {
        return sprintf("digraph workflow {\n  %s\n  node [%s];\n  edge [%s];\n\n",
            $this->addOptions($options['graph']),
            $this->addOptions($options['node']),
            $this->addOptions($options['edge'])
        );
    }

    private function endDot()
    {
        return "}\n";
    }

    private function addAttributes($attributes)
    {
        $code = array();

        foreach ($attributes as $k => $v) {
            $code[] = sprintf('%s="%s"', $k, $v);
        }

        return $code ? ', '.implode(', ', $code) : '';
    }

    private function addOptions($options)
    {
        $code = array();

        foreach ($options as $k => $v) {
            $code[] = sprintf('%s="%s"', $k, $v);
        }

        return implode(' ', $code);
    }

    private function dotize($id)
    {
        return strtolower(preg_replace('/[^\w]/i', '_', $id));
    }
}
