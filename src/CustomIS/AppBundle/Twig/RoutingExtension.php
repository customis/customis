<?php
/**
 * Created by PhpStorm.
 * User: T
 * Date: 23.2.2017
 * Time: 23:35
 */

namespace CustomIS\AppBundle\Twig;

/**
 * Class RoutingExtension
 * @package CustomIS\AppBundle\Twig
 */
class RoutingExtension extends \Symfony\Bridge\Twig\Extension\RoutingExtension
{
    /**
     * @param $name
     * @param array $parameters
     * @param bool $relative
     * @param bool $checkSessionData
     * @return string
     */
    public function getPath($name, $parameters = array(), $relative = false, $checkSessionData = true)
    {
        $parameters['_customis_session_route'] = $checkSessionData;

        return parent::getPath($name, $parameters, $relative);
    }
}