<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 27.10.2015
 * Time: 21:15
 */
namespace CustomIS\FormAnnotationBundle\Factory;

use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;

interface CreateTypeFactoryInterface
{
    /**
     * @param string $entityName
     * @param string|null $formName
     * @return FormInterface
     */
    public function createForm($entityName, $formName = null);
}