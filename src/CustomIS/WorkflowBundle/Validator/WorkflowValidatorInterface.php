<?php
/**
 * Created by PhpStorm.
 * User: to068928
 * Date: 2.1.2017
 * Time: 13:46
 */

namespace CustomIS\WorkflowBundle\Validator;

use Doctrine\ORM\Mapping\Entity;

interface WorkflowValidatorInterface
{
    /**
     * @param object $object
     *
     * @return boolean
     */
    public function supports($object);

    /**
     * @param object $object
     *
     * @return boolean|WorkflowValidatorExceptionInterface
     */
    public function validate($object);
}