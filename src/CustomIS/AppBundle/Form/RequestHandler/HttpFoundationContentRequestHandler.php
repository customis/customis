<?php

declare(strict_types = 1);

namespace CustomIS\AppBundle\Form\RequestHandler;

use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationRequestHandler;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class HttpFoundationContentRequestHandler
 *
 * @package CustomIS\AppBundle\Form\RequestHandler
 */
class HttpFoundationContentRequestHandler extends HttpFoundationRequestHandler
{
    /**
     * @param FormInterface $form
     * @param Request|null  $request
     */
    public function handleRequest(FormInterface $form, $request = null): void
    {
        if (($content = $request->getContent()) !== null) {
            $jsonDecoded = json_decode($content, true);
            if ($jsonDecoded) {
                $request->request->replace(array_merge($jsonDecoded, $request->request->all()));
            }
        }

        parent::handleRequest($form, $request);
    }
}
