<?php

namespace CustomIS\FormAnnotationBundle\Component\Form;
//use \Symfony\Component\Form\FormEvents;


use CustomIS\FormAnnotationBundle\Form\EventListener\EntityFormSubscriber;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Util\Inflector as Inflector;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

abstract class AbstractAnnotationType extends AbstractType
{
    protected $container;
    
    abstract function getDataClass();
    
    public function __construct(\Symfony\Component\DependencyInjection\Container $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $reader = new AnnotationReader();
        $builder->addEventSubscriber(new EntityFormSubscriber($reader, $this->container));
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->getDataClass()
        ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return (new Inflector())
            ->tableize(str_replace('Type', '', basename(str_replace('\\', '/', get_class($this)))));
    }
}