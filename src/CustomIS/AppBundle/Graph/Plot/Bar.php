<?php

declare(strict_types = 1);

namespace CustomIS\AppBundle\Graph\Plot;

/**
 * Class Line
 *
 * @package CustomIS\AppBundle\Graph\Plot
 */
class Bar extends AbstractPlot
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'bar';
    }
}
