<?php

namespace CustomIS\AppBundle\Response;

/**
 * Class JsonRedirectResponse
 *
 * @package CustomIS\AppBundle\Response
 */
class JsonRedirectResponse
{
    /**
     * @var string
     */
    private $route;

    /**
     * @var array
     */
    private $routeParams;

    /**
     * JsonRedirectResponse constructor.
     *
     * @param string $route
     * @param array  $routeParams
     */
    public function __construct($route, $routeParams = [])
    {
        $this->route = $route;
        $this->routeParams = $routeParams;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @return array
     */
    public function getRouteParams()
    {
        return $this->routeParams;
    }
}
