<?php

namespace CustomIS\FormAnnotationBundle\Form;

use Doctrine\ORM\EntityRepository;

interface QueryBuilderInterface
{
    /**
     * @param EntityRepository $er
     * @return QueryBuilder
     */
    public function getQueryBuilder(EntityRepository $er);
}