<?php

namespace CustomIS\PostgresBundle\Doctrine\Types;

use CustomIS\PostgresBundle\Geometry\Point;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class PolygonType
 *
 * @package CustomIS\PostgresBundle\Doctrine\Types
 */
class PointType extends Type
{
    const POINT = 'point'; // modify to match your type name

    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'point';
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return Point
     * @throws \Exception
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value !== null) {
            $value = trim($value, '()');
            $pointArray = array_map('trim', explode(',', $value));

            return Point::fromArray($pointArray);
        }

        return null;
    }

    /**
     * @param Point            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value !== null) {
            return sprintf('(%f,%f)', $value->getX(), $value->getY());
        }

        return null;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::POINT;
    }

    /**
     * @param string           $sqlExpr
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform)
    {
        return $sqlExpr;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return array
     */
    public function getMappedDatabaseTypes(AbstractPlatform $platform)
    {
        return ['point'];
    }
}
