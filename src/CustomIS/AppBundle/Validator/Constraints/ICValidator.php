<?php

namespace CustomIS\AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class ICValidator
 *
 * @package CustomIS\AppBundle\Validator\Constraints
 */
class ICValidator extends ConstraintValidator
{
    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof IC) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\IC');
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_scalar($value) && !(is_object($value) && method_exists($value, '__toString'))) {
            throw new UnexpectedTypeException($value, 'string');
        }

        $value = (string) $value;

        // be liberal in what you receive
        $value = preg_replace('#\s+#', '', $value);

        $value = str_pad($value, 8, '0', STR_PAD_LEFT);
        // expected format?
        if (preg_match('#^\d{8}$#', $value)) {
            $a = 0;
            for ($i = 0; $i < 7; $i++) {
                $a += $value[$i] * (8 - $i);
            }

            $a %= 11;
            if ($a === 0) {
                $c = 1;
            } elseif ($a === 1) {
                $c = 0;
            } else {
                $c = 11 - $a;
            }

            if ((int) $value[7] === $c) {
                return;
            }
        }

        $this->context->buildViolation($constraint->message)
                      ->setCode(IC::INVALID_IC_ERROR)
                      ->addViolation()
        ;
    }
}
