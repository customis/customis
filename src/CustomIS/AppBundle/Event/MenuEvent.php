<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 06.07.2016
 * Time: 20:42
 */

namespace CustomIS\AppBundle\Event;


use Knp\Menu\ItemInterface;
use Symfony\Component\EventDispatcher\Event;

class MenuEvent extends Event
{
    const NAME = 'customis.menu';

    /**
     * @var ItemInterface
     */
    private $menu;

    /**
     * MenuEvent constructor.
     * @param ItemInterface $menu
     */
    public function __construct(ItemInterface $menu)
    {
        $this->menu = $menu;
    }

    /**
     * @return ItemInterface
     */
    public function getMenu()
    {
        return $this->menu;
    }

}