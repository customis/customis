<?php

namespace CustomIS\AppBundle\EventListener;

use CustomIS\AppBundle\Response\JsonRedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;

/**
 * Class AjaxRedirectResponseListener
 *
 * @package CustomIS\AppBundle\EventListener
 */
class AjaxRedirectResponseListener
{
    /**
     * @param GetResponseForControllerResultEvent $e
     */
    public function onKernelView(GetResponseForControllerResultEvent $e)
    {
        $controllerResult = $e->getControllerResult();
        if ($controllerResult instanceof JsonRedirectResponse) {
            $e->setResponse(new JsonResponse([
                '@redirect_route'        => $controllerResult->getRoute(),
                '@redirect_route_params' => $controllerResult->getRouteParams(),
            ]));
            $e->stopPropagation();
        }
    }
}
