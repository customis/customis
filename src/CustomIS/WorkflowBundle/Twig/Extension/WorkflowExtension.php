<?php


namespace CustomIS\WorkflowBundle\Twig\Extension;

use CustomIS\WorkflowBundle\Component\Registry;
use CustomIS\WorkflowBundle\Entity\WorkflowPlace;
use CustomIS\WorkflowBundle\Entity\WorkflowTransition;

class WorkflowExtension extends \Twig_Extension
{
    private $workflowRegistry;

    public function __construct(Registry $workflowRegistry)
    {
        $this->workflowRegistry = $workflowRegistry;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('workflow_can_directly', array($this, 'canTransitionDirectly')),
            new \Twig_SimpleFunction('workflow_transition_name', array($this, 'getTransitionName')),
            new \Twig_SimpleFunction('workflow_transition_action_name', array($this, 'getTransitionActionName')),
            new \Twig_SimpleFunction('workflow_place_name', array($this, 'getPlaceName')),
            new \Twig_SimpleFunction('workflow_place_entity', array($this, 'getPlaceEntity')),
            new \Twig_SimpleFunction('workflow_transition_entity', array($this, 'getTransitionEntity')),
            new \Twig_SimpleFunction('workflow_direct_transitions', array($this, 'getDirectlyEnabledTransitions')),
        );
    }

    public function getTransitionName($object, $transition, $name = null)
    {
        $workflowName = $this->workflowRegistry->get($object, $name)->getName();
        if( is_string($transition)) {
            $transition = $this->workflowRegistry->getTransitionObject($workflowName, $transition);
        }
        if($transition instanceof WorkflowTransition && $transition != null) {
            return $transition->getTitle();
        }
        return null;
    }

    public function getTransitionActionName($object, $transition, $name = null)
    {
        $workflowName = $this->workflowRegistry->get($object, $name)->getName();
        if( is_string($transition)) {
            $transition = $this->workflowRegistry->getTransitionObject($workflowName, $transition);
        }
        if($transition instanceof WorkflowTransition && $transition != null) {
            return $transition->getActionName();
        }
        return null;
    }

    public function getPlaceName($object, $place, $name = null)
    {
        $workflow = $this->workflowRegistry->get($object, $name);
        if( is_string($place)) {
            $place = $this->workflowRegistry->getPlaceObject($workflow->getName(), $place);
        }
        if($place instanceof WorkflowPlace && $place != null) {
            return $place->getTitle();
        }
        return null;
    }

    public function getTransitionEntity($object, $transition, $name = null)
    {
        $workflowName = $this->workflowRegistry->get($object, $name)->getName();
        if( is_string($transition)) {
            $transition = $this->workflowRegistry->getTransitionObject($workflowName, $transition);
        }
        if($transition instanceof WorkflowTransition && $transition != null) {
            return $transition;
        }
        return null;
    }

    public function getPlaceEntity($object, $place = null, $name = null)
    {
        $workflow = $this->workflowRegistry->get($object, $name);
        if($place == null) {
            $place = $workflow->getDefinition()->getInitialPlace();
        }
        if( is_string($place)) {
            $place = $this->workflowRegistry->getPlaceObject($workflow->getName(), $place);
        }
        if($place instanceof WorkflowPlace && $place != null) {
            return $place;
        }
        return null;
    }

    public function canTransitionDirectly($object, $transition, $name = null)
    {
        return $this->workflowRegistry->get($object, $name)->can($object, $transition)
                && $this->workflowRegistry->getTransitionObject() != null
                && $this->workflowRegistry->getTransitionObject()->getInteractive();
    }

    public function getDirectlyEnabledTransitions($object, $name = null)
    {
        $transitions = [];
        $workflow = $this->workflowRegistry->get($object, $name);
        foreach( $workflow->getEnabledTransitions($object) AS $transition) {
            if($this->workflowRegistry->getTransitionObject($workflow->getName(), $transition->getName()) != null
                && $this->workflowRegistry->getTransitionObject($workflow->getName(), $transition->getName())->getInteractive())
            $transitions[] = $transition;
        }
        return $transitions;
    }

    public function getName()
    {
        return 'customis_workflow';
    }
}
