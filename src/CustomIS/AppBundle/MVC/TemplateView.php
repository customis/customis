<?php

declare(strict_types = 1);

namespace CustomIS\AppBundle\MVC;

/**
 * Class TemplateView
 *
 * @package CustomIS\AppBundle\MVC
 */
class TemplateView
{
    /**
     * @var array
     */
    private $viewParams;

    /**
     * @var null|string
     */
    private $actionTemplateName;

    /**
     * @var int
     */
    private $statusCode;

    /**
     * @var array
     */
    private $headers;

    /**
     * TemplateView constructor.
     *
     * @param array       $viewParams
     * @param string|null $actionTemplateName
     * @param int         $statusCode
     * @param array       $headers
     */
    public function __construct(
        array $viewParams,
        $actionTemplateName = null,
        int $statusCode = 200,
        array $headers = []
    ) {
        $this->viewParams = $viewParams;
        $this->actionTemplateName = $actionTemplateName;
        $this->statusCode = $statusCode;
        $this->headers = $headers;
    }

    /**
     * @return array
     */
    public function getViewParams(): array
    {
        $viewParams = $this->viewParams;
        if (is_object($viewParams)) {
            $viewParams = ['view' => $viewParams];
        }
        if (!isset($viewParams['view'])) {
            $viewParams['view'] = $viewParams;
        }

        return $viewParams;
    }

    /**
     * @return string
     */
    public function getActionTemplateName()
    {
        return $this->actionTemplateName;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }
}
