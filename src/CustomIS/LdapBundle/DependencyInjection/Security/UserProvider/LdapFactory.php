<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CustomIS\LdapBundle\DependencyInjection\Security\UserProvider;

use CustomIS\LdapBundle\Security\Core\User\LdapUserProvider;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\UserProvider\UserProviderFactoryInterface;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * LdapFactory creates services for Ldap user provider.
 *
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 * @author Charles Sarrazin <charles@sarraz.in>
 */
class LdapFactory implements UserProviderFactoryInterface
{
    /**
     * @param ContainerBuilder $container
     * @param string           $id
     * @param string           $config
     */
    public function create(ContainerBuilder $container, $id, $config)
    {
        $container
            ->setDefinition($id, new Definition(LdapUserProvider::class))
            ->setAutowired(true)
            ->setAutoconfigured(true)
            ->setArgument(0, new Reference($config['service']))
            ->setArgument(1, $config['filter'])
            ->setArgument(2, $config['default_roles'])
            ->setArgument(3, $config['roles'])
        ;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return 'customis-ldap';
    }

    /**
     * @param NodeDefinition $node
     */
    public function addConfiguration(NodeDefinition $node)
    {
        $node
            ->children()
                ->scalarNode('service')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->arrayNode('default_roles')
                    ->beforeNormalization()->ifString()->then(function ($v) {
                        return preg_split('/\s*,\s*/', $v);
                    })->end()
                    ->requiresAtLeastOneElement()
                    ->prototype('scalar')->end()
                ->end()
                ->arrayNode('roles')
                    ->requiresAtLeastOneElement()
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->prototype('scalar')->end()
                    ->end()
                ->end()
                ->scalarNode('filter')
                    ->defaultValue('(sAMAccountName={username})')
                ->end()
            ->end()
        ;
    }
}
