<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 29.01.2016
 * Time: 10:08
 */
namespace CustomIS\AppBundle\Uploader\Namer;

use Oneup\UploaderBundle\Uploader\File\FileInterface;
use Oneup\UploaderBundle\Uploader\Naming\NamerInterface;

class Sha1Namer implements NamerInterface
{
    public function name(FileInterface $file)
    {
        $sha1 = sha1_file($file->getRealPath());
        return substr($sha1, 0, 2) . DIRECTORY_SEPARATOR . $sha1;
    }
}