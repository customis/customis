<?php
/**
 * Created by PhpStorm.
 * User: ja068850
 * Date: 10.11.2016
 * Time: 15:19
 */

namespace CustomIS\AppBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Interface UzivatelRepositoryInterface
 *
 * @package CustomIS\AppBundle\Entity
 */
interface UzivatelRepositoryInterface
{
    /**
     * @param UserInterface|string $user
     *
     * @return Uzivatel|null
     */
    public function findByUser($user);
}
