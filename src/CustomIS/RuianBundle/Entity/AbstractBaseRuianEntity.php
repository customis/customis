<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Entity;

use CustomIS\AppBundle\Doctrine\UuidIdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class BaseRuianEntity
 *
 * @package CustomIS\RuianBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(schema="ruian", name="base_entity")
 * @ORM\InheritanceType("JOINED")
 */
abstract class AbstractBaseRuianEntity
{
    use UuidIdTrait;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $code;

    /**
     * @var string
     * @ORM\Column(type="string", length=2048)
     */
    private $name;

    /**
     * @var Geometry[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="CustomIS\RuianBundle\Entity\Geometry", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinTable(schema="ruian", name="base_entity_geometry",
     *     joinColumns={@ORM\JoinColumn(name="base_entity_code")}
     * )
     */
    private $geometry;

    /**
     * BaseRuianEntity constructor.
     *
     * @param int    $code
     * @param string $name
     */
    public function __construct($code, $name)
    {
        $this->code = $code;
        $this->name = $name;
        $this->geometry = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return Geometry[]|ArrayCollection
     */
    public function getGeometry()
    {
        return $this->geometry;
    }

    /**
     * @param Geometry[]|ArrayCollection $geometry
     */
    public function setGeometry($geometry)
    {
        $this->geometry = $geometry;
    }

    /**
     * @param Geometry $geometry
     */
    public function addGeometry(Geometry $geometry)
    {
        $this->geometry[] = $geometry;
    }

    /**
     * @param Geometry $geometry
     */
    public function removeGeometry(Geometry $geometry)
    {
        $this->geometry->removeElement($geometry);
    }

    /**
     * @return void
     */
    public function clearGeometry()
    {
        $this->geometry->clear();
    }
}
