<?php

declare(strict_types = 1);

namespace CustomIS\AppBundle\Form;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class FormRequest
 *
 * @package O2\AppBundle\Form
 */
class FormRequest
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var FormInterface
     */
    private $form;

    /**
     * FormRequest constructor.
     *
     * @param FormFactoryInterface $formFactory
     * @param RequestStack         $requestStack
     */
    public function __construct(FormFactoryInterface $formFactory, RequestStack $requestStack)
    {
        $this->formFactory = $formFactory;
        $this->requestStack = $requestStack;
    }

    /**
     * Attempt to handle a form and return true when handled and data is valid.
     *
     * @param string|FormTypeInterface $formType
     * @param array|object             $bindData
     * @param array                    $options
     *
     * @return bool
     */
    public function handle($formType, $bindData = null, array $options = [])
    {
        $this->form = $this->formFactory->create($formType, $bindData, $options);
        $this->form->handleRequest($this->requestStack->getCurrentRequest());

        return $this->form->isSubmitted() && $this->form->isValid();
    }

    /**
     * @return mixed
     */
    public function getValidData()
    {

        return $this->form->getData();
    }

    /**
     * @return \Symfony\Component\Form\FormView
     */
    public function createFormView()
    {
        return $this->form->createView();
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return $this->form->isValid();
    }

    /**
     * @return bool
     */
    public function isBound()
    {
        return $this->form->isSubmitted();
    }
}
