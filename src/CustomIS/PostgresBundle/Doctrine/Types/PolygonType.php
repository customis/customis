<?php

namespace CustomIS\PostgresBundle\Doctrine\Types;

use CustomIS\PostgresBundle\Geometry\Polygon;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class PolygonType
 *
 * @package CustomIS\PostgresBundle\Doctrine\Types
 */
class PolygonType extends Type
{
    const POLYGON = 'polygon'; // modify to match your type name

    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'polygon';
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return Polygon
     * @throws \Exception
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value !== null) {
            $polygonArray = json_decode(str_replace(['(', ')'], ['[', ']'], $value));

            return Polygon::fromArray($polygonArray);
        }

        return null;
    }

    /**
     * @param Polygon          $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value !== null) {
            return str_replace(['[', ']'], ['(', ')'], json_encode($value->toArray()));
        }

        return null;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::POLYGON;
    }

    /**
     * @param string           $sqlExpr
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform)
    {
        return $sqlExpr;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return array
     */
    public function getMappedDatabaseTypes(AbstractPlatform $platform)
    {
        return ['polygon'];
    }
}
