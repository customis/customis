<?php

namespace CustomIS\FormAnnotationBundle\Annotation;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 * @Attributes({
 *   @Attribute("value", type = "boolean", required = true),
 *   @Attribute("form", type = "string", required = false)
 * })
 */
class Required extends Annotation
{
    private $value;
    
    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->value = $data['value'];
    }

    public function getValue()
    {
        return $this->value;
    }
}