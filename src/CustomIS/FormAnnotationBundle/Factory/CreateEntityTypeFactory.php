<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 27.10.2015
 * Time: 20:37
 */

namespace CustomIS\FormAnnotationBundle\Factory;

use CustomIS\FormAnnotationBundle\Exception\ClassNotFoundException;
use CustomIS\FormAnnotationBundle\Form\BaseType;
use CustomIS\FormAnnotationBundle\Form\EventListener\EntityFormSubscriber;
use Symfony\Component\Form\FormFactoryInterface;

class CreateEntityTypeFactory implements CreateTypeFactoryInterface
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var EntityFormSubscriber
     */
    private $entityFormSubscriber;

    /**
     * CreateEntityTypeFactory constructor.
     * @param FormFactoryInterface $formFactory
     * @param EntityFormSubscriber $entityFormSubscriber
     */
    public function __construct(FormFactoryInterface $formFactory, EntityFormSubscriber $entityFormSubscriber)
    {
        $this->formFactory = $formFactory;
        $this->entityFormSubscriber = $entityFormSubscriber;
    }


    public function createForm($entityName, $formName = null)
    {
        if (!class_exists($entityName))
        {
            throw new ClassNotFoundException('Entity class ' . $entityName . ' not found');
        }

        if ($formName === null)
        {
            $formName = str_replace('\\', '_', strtolower($entityName));
        }

//        $baseType = new BaseType($entityName, $this->entityFormSubscriber);
        return $this->formFactory->createNamed($formName, BaseType::class, null, [
            'validation_groups' => ['Default', $formName],
            'data_class' => $entityName
        ]);
    }


}