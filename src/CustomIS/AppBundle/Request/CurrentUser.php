<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 18.05.2016
 * Time: 18:01
 */

namespace CustomIS\AppBundle\Request;

use CustomIS\AppBundle\Entity\Uzivatel;
use CustomIS\AppBundle\Entity\UzivatelRepository;
use CustomIS\AppBundle\Entity\UzivatelRepositoryInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class CurrentUser
 *
 * @package CustomIS\AppBundle\Request
 */
class CurrentUser
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorageInterface;

    /**
     * @var UzivatelRepository
     */
    private $uzivatelRepository;

    /**
     * @var Uzivatel
     */
    private $user;

    /**
     * CurrentUser constructor.
     *
     * @param TokenStorageInterface                          $tokenStorageInterface
     * @param UzivatelRepository|UzivatelRepositoryInterface $uzivatelRepository
     */
    public function __construct(TokenStorageInterface $tokenStorageInterface, UzivatelRepositoryInterface $uzivatelRepository)
    {
        $this->tokenStorageInterface = $tokenStorageInterface;
        $this->uzivatelRepository = $uzivatelRepository;
    }

    /**
     * @return \CustomIS\AppBundle\Entity\Uzivatel|null
     */
    public function getUser()
    {
        if ($this->user !== null) {
            return $this->user;
        }

        $token = $this->tokenStorageInterface->getToken();
        if ($token !== null && !($token instanceof AnonymousToken)) {
            return (
            $this->user = $this->uzivatelRepository->findByUser($token->getUser())
            );
        }

        return null;
    }

    /**
     * @return null|\Symfony\Component\Security\Core\Authentication\Token\TokenInterface
     */
    public function getUserToken()
    {
        return $this->tokenStorageInterface->getToken();
    }
}
