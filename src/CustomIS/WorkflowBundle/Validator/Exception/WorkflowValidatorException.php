<?php
/**
 * Created by PhpStorm.
 * User: to068928
 * Date: 2.1.2017
 * Time: 14:39
 */

namespace CustomIS\WorkflowBundle\Validator\Exception;

class WorkflowValidatorException extends \RuntimeException
{
}