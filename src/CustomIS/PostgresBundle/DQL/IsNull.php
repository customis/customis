<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 12.06.2016
 * Time: 20:35
 */

namespace CustomIS\PostgresBundle\DQL;


use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class IsNull extends FunctionNode
{
    private $expr;

    public function getSql(SqlWalker $sqlWalker)
    {
        return $this->expr->dispatch($sqlWalker) . ' IS NULL';
    }

    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->expr = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

}