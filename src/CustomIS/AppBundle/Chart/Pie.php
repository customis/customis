<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 02.03.2016
 * Time: 16:43
 */

namespace CustomIS\AppBundle\Chart;


use CustomIS\AppBundle\Chart\Pie\PieValue;

class Pie
{
    /**
     * @var PieValue[]
     */
    private $values = [];

    public function addValue(PieValue $value)
    {
        $this->values[] = $value;
    }

    /**
     * @return Pie\PieValue[]
     */
    public function getValues()
    {
        return $this->values;
    }

    public function toC3ChartColumnsArray()
    {
        $return = [];
        foreach ($this->getValues() as $value)
        {
            $return[] = $value->toC3ChartColumnArray();
        }

        return $return;
    }

}