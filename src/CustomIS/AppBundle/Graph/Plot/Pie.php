<?php

declare(strict_types = 1);

namespace CustomIS\AppBundle\Graph\Plot;

/**
 * Class Line
 *
 * @package CustomIS\AppBundle\Graph\Plot
 */
class Pie extends AbstractPlot
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'pie';
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $return = parent::jsonSerialize();
        $return['values'] = $return['y'];
        $return['labels'] = $return['x'];
        unset($return['x'], $return['y']);

        return $return;
    }
}
