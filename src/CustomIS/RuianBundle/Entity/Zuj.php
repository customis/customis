<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Entity;
use CustomIS\RuianBundle\Geometry\GeometryTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Zuj
 *
 * @package CustomIS\RuianBundle\Entity
 * @ORM\Entity(repositoryClass="ZujRepository")
 * @ORM\Table(schema="ruian")
 * @ORM\InheritanceType("SINGLE_TABLE")
 */
abstract class Zuj
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $code;

    /**
     * Zuj constructor.
     *
     * @param int $code
     */
    public function __construct($code)
    {
        $this->code = $code;
    }

    /**
     * @return Geometry[]|ArrayCollection
     */
    abstract public function getGeometry();

    /**
     * @return string
     */
    abstract public function getName();

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }


}
