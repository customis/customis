<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Command;

use CustomIS\RuianBundle\Loader\AbstractRuianLoader;
use CustomIS\RuianBundle\Loader\Kraj1960Loader;
use CustomIS\RuianBundle\Loader\MomcLoader;
use CustomIS\RuianBundle\Loader\ObecLoader;
use CustomIS\RuianBundle\Loader\OkresLoader;
use CustomIS\RuianBundle\Loader\PouLoader;
use CustomIS\RuianBundle\Loader\VuscLoader;
use CustomIS\RuianBundle\Loader\ZsjLoader;
use CustomIS\RuianBundle\Loader\ZujLoader;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

/**
 * Class RuianLoaderConmmand
 *
 * @package CustomIS\RuianBundle\Command
 */
class RuianLoaderConmmand extends ContainerAwareCommand
{
    private $ruianTypes = [
        'pou' => PouLoader::class,
        'obec' => ObecLoader::class,
        'okres' => OkresLoader::class,
        'kraj' => Kraj1960Loader::class,
        'momc' => MomcLoader::class,
        'zuj' => ZujLoader::class,
        'vusc' => VuscLoader::class,
        'zsj' => ZsjLoader::class,
    ];

    protected function configure()
    {
        $this->setName('customis:ruian:load')
             ->addArgument('type', InputArgument::REQUIRED)
             ->addArgument('ruianFilePath', InputArgument::REQUIRED)
             ->addOption('simplify', null, InputOption::VALUE_OPTIONAL, 'Ratio for symplifing polygon borders (40 recomended)')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $typeKey = $input->getArgument('type');
        if (!array_key_exists($typeKey, $this->ruianTypes)) {
            throw new \InvalidArgumentException('Invalid value for type argument. Possible values are: '
                                                .implode(', ', array_keys($this->ruianTypes)));
        }

        /** @var AbstractRuianLoader $type */
        $type = $this->getContainer()->get($this->ruianTypes[$typeKey]);
        $type->setSymplifyBorderRation(
            $input->getOption('simplify') !== null
                ? (float)$input->getOption('simplify')
                : null
        );

        $file = $input->getArgument('ruianFilePath');
        if (!file_exists($file)) {
            throw new FileNotFoundException(null, 0, null, $file);
        }

        $xmlReader = new \XMLReader();
        if ($xmlReader->open($file)) {
            $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
//            $entityManager->transactional(function (EntityManager $entityManager) use ($xmlReader, $type, $file) {
                while ($xmlReader->read()) {
                    if ($xmlReader->nodeType === \XMLReader::ELEMENT
                        && in_array($xmlReader->name, $type->getXmlNodeName())
                        && $xmlReader->getAttribute('gml:id') !== null
                    ) {
                        $simplexml = new \SimpleXMLElement($xmlReader->readOuterXML());
                        $type->load($simplexml, $file);
                    }
                }
//            });

            $xmlReader->close();
        } else {
            throw new \RuntimeException('Unable to open XML file');
        }
    }
}
