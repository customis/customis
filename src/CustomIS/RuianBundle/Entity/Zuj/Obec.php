<?php

declare(strict_types = 1);

namespace CustomIS\RuianBundle\Entity\Zuj;

use CustomIS\RuianBundle\Entity\Zuj;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Obec
 *
 * @package CustomIS\RuianBundle\Entity\Zuj
 * @ORM\Entity()
 * @ORM\Table(name="zuj_obec", schema="ruian")
 */
class Obec extends Zuj
{
    /**
     * @var \CustomIS\RuianBundle\Entity\Obec
     * @ORM\OneToOne(targetEntity="CustomIS\RuianBundle\Entity\Obec", fetch="EAGER")
     */
    private $obec;

    public function __construct($code, \CustomIS\RuianBundle\Entity\Obec $obec)
    {
        parent::__construct($code);
        $this->obec = $obec;
    }

    /**
     * @return \CustomIS\RuianBundle\Entity\Obec
     */
    public function getObec(): \CustomIS\RuianBundle\Entity\Obec
    {
        return $this->obec;
    }

    /**
     * @param \CustomIS\RuianBundle\Entity\Obec $obec
     */
    public function setObec(\CustomIS\RuianBundle\Entity\Obec $obec)
    {
        $this->obec = $obec;
    }

    public function getGeometry()
    {
        return $this->getObec()->getGeometry();
    }

    public function getName()
    {
        return $this->getObec()->getName();
    }
}
